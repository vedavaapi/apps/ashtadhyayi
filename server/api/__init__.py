import flask_restx
from flask import Blueprint


api_blueprint_v1 = Blueprint('ashtadhyayi_api' + '_v1', __name__)

api = flask_restx.Api(
    app=api_blueprint_v1,
    version='1.0',
    title='PAIAS Ashtadhyayi Server',
    doc='/'
)

from . import paias_ns
