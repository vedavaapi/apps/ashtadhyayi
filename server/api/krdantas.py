from vedavaapi.ashtadhyayi import *

def load_krdantas():
    krdanta_forms = []
    for row in read_csvfile("krt-pratyayas.csv"):
        entry = {}
        for k, v in row.items():
            v = xliterate(v.strip(), row['inscript'], "SLP1")
            comps = k.split('.')
            if len(comps) > 1:
                if comps[0] not in entry:
                    entry[comps[0]] = {'label': k}
                entry[comps[0]][".".join(comps[1:])] = v
            else:
                entry[comps[0]] = {'pada': v, 'label': k}
        padas = [v for k, v in entry.items() \
                 if k in ['upasarga', 'dhatu', 'pratyaya'] and v['pada']]
        a().add_dhatu_props(padas)
        dhatu = [p for p in padas if 'dhatu' in p['label']]
        dhatu = dhatu[0]
        dhatu_label = f"{dhatu['pada']} | gaRa={dhatu['gaRa']} | arTa={dhatu['artha']}"
        krdanta_forms.append({ 'pdescs': padas,
                               'dhatu_label': dhatu_label,
                               'pratyaya': entry['pratyaya']['pada'],
                               'expected': transcribe(row['exp_result'].split('/'),
                                                     row['inscript'], "SLP1")})
    return krdanta_forms