import json

import flask_restx
from flask import session

from . import api
from examples.interpreter import *
from .krdantas import *

interpret_ns = api.namespace('krdantas', path='/')

admin_ns = api.namespace('admin', path='/admin')

info_ns = api.namespace('info', path='/info')

krdanta_forms = load_krdantas()
krdanta_idx = {}
for i,f in enumerate(krdanta_forms):
    if not f['expected']:
        continue
    for e in f['expected']:
        krdanta_idx[e] = i

dhatu_labels = set()
pratyayas = set()
for f in krdanta_forms:
    dhatu_labels.add(f['dhatu_label'])
    pratyayas.update([p['pada'] for p in f['pdescs'] if p['label'] == 'pratyaya'])
dhatu_labels = sorted(dhatu_labels)
pratyayas = sorted(pratyayas)
dhatu_ganas = (
    "BvAdi", "adAdi", "juhotyAdi", "divAdi",
    "svAdi", "tudAdi", "ruDAdi", "tanAdi",
    "kryAdi", "curAdi"
)
krdanta_padas = sorted(reduce(lambda x, y: x | y,
                              [set(f['expected']) for f in krdanta_forms if f['expected']]))
dhatu_anta = ("adanta", "Adanta", "iganta", "ejanta", "halanta")
dhatu_upadha = ("adupaDA", "igupaDA", "Seza")
scripts = ("SLP1", "DEVANAGARI", "ITRANS", "IAST")
cathelp = 'padi:Atmane|parasmE, gaRa:[...], iT:[sew|aniw], x_anta:[adanta|Adanta|halanta|iganta|ejanta], x_upadhA:[adupaDA|igupaDA|Seza]'


@interpret_ns.route('/join')
class Interpreter(flask_restx.Resource):

    get_parser = interpret_ns.parser()
    get_parser.add_argument('dhatu', type=str, location='args', required=False)
    get_parser.add_argument('gana', type=str, choices=dhatu_ganas, location='args', required=False)
    get_parser.add_argument('pratyaya', type=str, choices=pratyayas, location='args', required=True)
    #    get_parser.add_argument('permute', type=bool, location='args', required=True, default='false')
    get_parser.add_argument('exp_result', help='<final_form1>[/<form2>/..]', type=str, location='args', required=False, default='')
    get_parser.add_argument('inscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('outscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('logging_level', type=str, choices=('0', '1', '2'), location='args', default='0', required=False)

    @interpret_ns.expect(get_parser, validate=True)
    def get(self):
        set_req_context(ReqContext())

        args = self.get_parser.parse_args()
        pdescs = [
            {'pada': xliterate(args['dhatu'], args['inscript'], "SLP1"),
             'label': 'dhatu.pada',
             'gaRa': args['gana']},
            {'pada': args['pratyaya'],
             'label': 'pratyaya'}
        ]
        exp_result = args['exp_result'].split('/')
        loglevel(args['logging_level'])
        res = a().interpret(pdescs, inscr="SLP1",
                            result=exp_result, outscr=args['outscript'])

        return res, 200

@interpret_ns.route('/krtgen')
class Interpreter(flask_restx.Resource):

    get_parser = interpret_ns.parser()
    get_parser.add_argument('dhatu_label', type=str, location='args', required=True)
    get_parser.add_argument('pratyaya', type=str, location='args', required=True)
    #    get_parser.add_argument('permute', type=bool, location='args', required=True, default='false')
    get_parser.add_argument('exp_result', help='<final_form1>[/<form2>/..]', type=str, location='args', required=False, default='')
    get_parser.add_argument('inscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('outscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('logging_level', type=str, choices=('0', '1', '2'), location='args', default='0', required=False)

    @interpret_ns.expect(get_parser, validate=True)
    def get(self):
        set_req_context(ReqContext())

        args = self.get_parser.parse_args()
        args['dhatu_label'] = xliterate(args['dhatu_label'], from_script=args['inscript'],
                                        to_script="SLP1")
        args['pratyaya'] = xliterate(args['pratyaya'], from_script=args['inscript'],
                                        to_script="SLP1")
        exp_result = []
        if args['exp_result']:
            exp_result = xliterate(args['exp_result'], from_script=args['inscript'],
                                   to_script="SLP1")
            exp_result = exp_result.split('/')
        entry = [f for f in krdanta_forms \
                 if f['dhatu_label'] == args['dhatu_label'] and  \
                    f['pratyaya'] == args['pratyaya']]
        if entry:
            exp_result = entry[0]['expected']
            pdescs = entry[0]['pdescs']
        else:
            comps = re.split(r'\s*\|\s*', args['dhatu_label'])
            gana = comps[1].split('=')[1]
            artha = comps[2].split('=')[1]
            pdescs = [
                {'pada': comps[0],
                 'label': 'dhatu.pada',
                 'gaRa': gana},
                {'pada': args['pratyaya'],
                 'label': 'pratyaya'}
            ]
        if exp_result:
            info(f"Expected = {exp_result}")

        pprint(pdescs)
        loglevel(args['logging_level'])
        res = a().interpret(pdescs, inscr="SLP1",
                            result=exp_result, outscr=args['outscript'])
        pprint(res)
        return res, 200

@interpret_ns.route('/derive')
class Interpreter(flask_restx.Resource):
    get_parser = interpret_ns.parser()
    get_parser.add_argument('krdanta', type=str, location='args', required=True)
    get_parser.add_argument('inscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('outscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('logging_level', type=str, choices=('0', '1', '2'), location='args', default='0', required=False)

    @interpret_ns.expect(get_parser, validate=True)
    def get(self):
#        if 'req' not in session:
#            session['req'] = ReqContext()
        set_req_context(ReqContext())

        args = self.get_parser.parse_args()
        pada = xliterate(args['krdanta'], args['inscript'], "SLP1")
        print(f"deriving {pada}")
        outscr = args['outscript']
        if pada not in krdanta_idx:
            return {'status': 'failure', 'result': [] }, 404
        i = krdanta_idx[pada]
        entry = krdanta_forms[i]
        if 'mods' not in entry:
            loglevel(args['logging_level'])
            res = a().interpret(entry['pdescs'], inscr="SLP1",
                                result=entry['expected'], outscr="SLP1")
            for k,v in res.items():
                if k in ['generated', 'mods', 'success']:
                    entry[k] = v
        res = { 'padas': transcribe(entry['pdescs'], "SLP1", outscr),
                 'generated': xliterate(entry['generated'], "SLP1", outscr),
                 'expected': transcribe(entry['expected'], "SLP1", outscr),
                 'mods': transcribe(entry['mods'], "SLP1", outscr),
                 'success': entry['success'],
                 }
        return res, 200

@interpret_ns.route('/interpret')
class Interpreter(flask_restx.Resource):
    get_parser = interpret_ns.parser()
    get_parser.add_argument('inpadas', type=str, location='args', required=True)
    get_parser.add_argument('inscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('outscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('logging_level', type=str, choices=('0', '1', '2'), location='args', default='0', required=False)

    @interpret_ns.expect(get_parser, validate=True)
    def get(self):
        set_req_context(ReqContext())

        args = self.get_parser.parse_args()
        inpadas = xliterate(args['inpadas'], args['inscript'], "SLP1").split(" ")
        inscr = args['inscript']
        outscr = args['outscript']
        pdescs = []

        for p in inpadas:
            comps = p.split(",")
            v = comps.pop(0)
            v = xliterate(v.strip(), inscr, "SLP1")
            entry = { "pada": v, "label": "pada" }
            for c in comps:
                l = c.split('=')
                entry[l[0]] = l[1]
            pdescs.append(entry)
        
        result = a().interpret(pdescs, inscr=inscr, sutras=[], outscr=outscr)
        return result, 200

@admin_ns.route('/reload')
class AdminOps(flask_restx.Resource):
    def get(self):
        try:
            ashtadhyayi_init(True)
        except Exception as e:
            return { 'status': 'failure', 'message': str(e) }, 200
        return { 'status': 'success'}, 200

@admin_ns.route('/login')
class AdminOps(flask_restx.Resource):
    get_parser = admin_ns.parser()
    get_parser.add_argument('inscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('outscript', type=str, choices=scripts, location='args', default='SLP1', required=False)

    @admin_ns.expect(get_parser, validate=True)
    def get(self):
        if 'req' not in session:
            session['req'] = ReqContext()
        set_req_context(session['req'])
        args = self.get_parser.parse_args()
        return { 'status': 'success'}, 200

@info_ns.route('/dhatus')
class AdminOps(flask_restx.Resource):
    get_parser = info_ns.parser()
    get_parser.add_argument('dhatu_substring', type=str, location='args', default='', required=False)
    get_parser.add_argument('category', type=str, help=cathelp, location='args', default='', required=False)
    get_parser.add_argument('inscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('outscript', type=str, choices=scripts, location='args', default='SLP1', required=False)

    @info_ns.expect(get_parser, validate=True)
    def get(self):
        set_req_context(ReqContext())

        args = self.get_parser.parse_args()
        dhatus = []
        _filter = args['dhatu_substring']
        if _filter:
            _filter = xliterate(_filter, from_script=args['inscript'], to_script='SLP1')
        catdesc = args['category']
        if catdesc:
            catdesc = xliterate(catdesc, from_script=args['inscript'], to_script='SLP1')
            catdesc = dict([e.split(':') for e in re.split(r'\s*,\s*', catdesc)])

        for d in a().upadesha['BUvAdi']:
            if _filter and _filter not in Pada.nosvaras(d.padas(0)['pada']):
                continue

            dinfo = {}
            for l,linfo in d.labels.items():
                if 'padas' not in linfo:
                    continue
                if l == 'upadeSa':
                    continue
                dinfo[l] = linfo['padas'][0]['val']
            if catdesc:
                match = True
                for k,v in catdesc.items():
                    if not k in dinfo:
                        continue
                    if v and dinfo[k] != v:
                        match = False
                        break
                if not match:
                    continue
            dinfo['dhatu_label'] = f"{dinfo['dhatu']} | gaRa={dinfo['gaRa']} | arTa={dinfo['artha'].strip()}"
            dhatus.append(dinfo)

        result = transcribe([d['dhatu_label'] for d in dhatus], "SLP1", args['outscript'])
        return {'status': 'success', 'matches': len(result), 'result': result}, 200

@info_ns.route('/krdantas')
class AdminOps(flask_restx.Resource):
    get_parser = info_ns.parser()
    get_parser.add_argument('show', type=str,
                            choices=('dhatu_labels', 'dhatu_ganas', 'dhatu_anta', 'dhatu_upadha', 'pratyayas', 'krdanta_padas'),
                            location='args', default='', required=False)
    get_parser.add_argument('category', type=str, help=cathelp, location='args', default='', required=False)
    get_parser.add_argument('outscript', type=str, choices=scripts, location='args', default='SLP1', required=False)

    @info_ns.expect(get_parser, validate=True)
    def get(self):
        set_req_context(ReqContext())

        result = krdanta_forms
        args = self.get_parser.parse_args()
        if args['show']:
            result = eval(args['show'])
        elif args['category']:
            catdesc = args['category']
            catdesc = xliterate(catdesc, "DEVANAGARI", "SLP1")
            if catdesc and ':' in catdesc:
                catdesc = dict([e.split(':') for e in re.split(r'\s*,\s*', catdesc)])

            result = set()
            for f in krdanta_forms:
                d = f['pdescs'][0]
                if catdesc:
                    match = True
                    for k, v in catdesc.items():
                        if k not in d:
                            continue
                        if v and d[k] != v:
                            match = False
                            break
                    if not match:
                        continue
                result.add(f['dhatu_label'])
            result = sorted(result)

        result = transcribe(result, "SLP1", args['outscript'])

        return {'status': 'success', 'matches': len(result), 'result': result}, 200

@info_ns.route('/samjnas')
class AdminOps(flask_restx.Resource):
    get_parser = info_ns.parser()
    get_parser.add_argument('inscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('outscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('filter', type=str, location='args', default='', required=False)

    @info_ns.expect(get_parser, validate=True)
    def get(self):
        set_req_context(ReqContext())

        args = self.get_parser.parse_args()
        samjnas = []
        _filter = args['filter']
        if _filter:
            _filter = xliterate(_filter, from_script=args['inscript'], to_script='SLP1')
        for t,term_desc in a().samjna_defs.items():
            if _filter and _filter not in Pada.nosvaras(t):
                continue
            # if 'members' in term_desc:
            samjnas.append(t)

        samjnas = transcribe(samjnas, "SLP1", args['outscript'])
        return {'status': 'success', 'matches': len(samjnas), 'result': samjnas}, 200

@info_ns.route('/sutra/<sutranum>')
class AdminOps(flask_restx.Resource):
    get_parser = info_ns.parser()
    get_parser.add_argument('outscript', type=str, choices=scripts, location='args', default='SLP1', required=False)

    @info_ns.expect(get_parser, validate=True)
    def get(self, sutranum):
        set_req_context(ReqContext())

        args = self.get_parser.parse_args()
        sutra = a().sutra(sutranum)
        if not sutra:
            return {'status': 'success', 'result': None}, 404

        sutra = { 'sutra': sutra }
        sutra = transcribe(sutra, "SLP1", args['outscript'])
        if sutranum in a().vidhi_defs['rules']:
            sutra['rule'] = a().vidhi_defs['rules'][sutranum]
        return {'status': 'success', 'result': sutra}, 200

@info_ns.route('/members/<samjna>')
class AdminOps(flask_restx.Resource):
    get_parser = info_ns.parser()
    get_parser.add_argument('inscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('outscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('filter', type=str, location='args', default='', required=False)

    @info_ns.expect(get_parser, validate=True)
    def get(self, samjna):
        set_req_context(ReqContext())

        args = self.get_parser.parse_args()
        _filter = args['filter']
        if _filter:
            _filter = xliterate(_filter, from_script=args['inscript'], to_script='SLP1')
        res = a().eval_samjna(samjna)
        if res:
            vals= res['members']['val']
            if isinstance(vals, dict):
                vals = sorted(vals.keys())
            if _filter:
                vals = [v for v in vals if _filter in v]
            res['members']['val'] = vals
            res = transcribe(res, from_script="SLP1", to_script=args['outscript'])
            return {'status': 'success', 'result': res}, 200
        else:
            return {}, 404

@info_ns.route('/samjna/<samjna>')
class AdminOps(flask_restx.Resource):
    get_parser = info_ns.parser()
    get_parser.add_argument('inscript', type=str, choices=scripts, location='args', default='SLP1', required=False)
    get_parser.add_argument('outscript', type=str, choices=scripts, location='args', default='SLP1', required=False)

    @info_ns.expect(get_parser, validate=True)
    def get(self, samjna):
        set_req_context(ReqContext())

        args = self.get_parser.parse_args()
        samjna = xliterate(samjna, from_script=args['inscript'], to_script='SLP1')
        res = a().samjna_defs.get(samjna)
        if res:
            res = transcribe(res, from_script="SLP1", to_script=args['outscript'])
            return {'status': 'success', 'result': res}, 200
        else:
            return {}, 404