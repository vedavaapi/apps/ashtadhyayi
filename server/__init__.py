import sys
import json

import flask
from flask_cors import CORS

sys.path.append("..")
from vedavaapi.ashtadhyayi import *
from pprint import pprint, pformat
#from .lib.importer import ESImportContext


app = flask.Flask(__name__, instance_relative_config=True)
CORS(app)
app.secret_key = 'Vedavaapi Ashtadhyayi'

try:
    app.config.from_file(filename="config.json", load=json.load)
except FileNotFoundError as e:
    pass

#vv_conf = app.config.get('VV', {})
#client_creds_path = vv_conf.get('client_creds_path', '/opt/content_search/client_creds.json')
#atr_path = vv_conf.get('atr_path', '/opt/content_search/atr.json')
#try:
#    atr = json.loads(open(atr_path, 'rb').read().decode('utf-8'))
#except:
#    atr = {}

ashtadhyayi_init(rebuild=True)


from .api import api_blueprint_v1
app.register_blueprint(api_blueprint_v1, url_prefix='')