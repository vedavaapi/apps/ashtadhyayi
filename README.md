PAIAS Ashtadhyayi Programmatic Interface
----------------------------------------
The entire Ashtadhyayi is available in a Google spreadsheet at 
https://docs.google.com/spreadsheets/d/1zoc4iCd9_l6mE1dP0F8KSFquABJOMFk2ZrSkDVxq62o/edit?gid=0#gid=0

## Usage

### Execute any given sequence of morphemes

**SLP1 is used as the default transliteration scheme**

`cd examples/`

`python interpreter.py -r BU tumu~n`

**Devanagari input can be provided as follows**

`python interpreter.py -r -l DEVANAGARI भू तुमुँन्`

## Installation process

1. Download the *ashtadhyayi* repository and unzip it.
https://gitlab.com/vedavaapi/apps/ashtadhyayi/-/archive/master/ashtadhyayi-master.zip

*OR* you can also clone the repository.

`git clone https://gitlab.com/vedavaapi/apps/ashtadhyayi.git`

2. Install any IDE such as VS Code or PyCharm. For beginners, it is recommended to download **PyCharm** using the follwing link. Make sure to choose the **Community Edition** of PyCharm.

- https://www.jetbrains.com/pycharm/download/

VS Code can also be used to execute the project. Please note that the **Python** Extension needs to be installed in VS Code so that the actual **Python Interpreter** gets installed.

- https://code.visualstudio.com/download
- https://www.python.org/downloads/

3. Run the following command in the console to install the required Python dependencies.

    `pip install -r requirements.txt`

4. Open PyCharm / VS Code, Change into examples/ directory. Now run the `interpreter.py` file with the desired arguments.

## How to install and configure PyCharm

1. Download the PyCharm Community Edition from this link: https://www.jetbrains.com/pycharm/download/
2. Right click on the setup file and click on **Run as Administrator**. Then click Next.
3. In the *Choose Install Location* screen, click Next.
4. Select all the checkboxes in **Installation Options** and click Next.
5. In the Choose Start Menu Folder, click on **Install** button.
6. Now the installation process will begin. Wait for the installation to complete.
7. Select the option *I want to manually reboot later* and click Finish.

8. From the Desktop, double click on PyCharm icon to open the app.
9. Click on **Get from VCS** button.
10. Enter this link in the URL input box. https://gitlab.com/vedavaapi/apps/ashtadhyayi/
11. You will see a message *Git is not installed*. Click on Download and Install.
12. After the installation completes, click on the **Clone** button.
13. Wait until PyCharm finishes Indexing process.
14. Navigate to the **ashtadhyayi/examples** folder from left pane.
15. Open the `interpreter.py` file.
16. Click on Configure Python Interpreter -> Add New Interpreter -> Click Ok.
17. Now Python will be downloaded and installed.
18. Create a virtual environment using requirements.txt -> Click Ok.
19. Wait for Indexing to complete. The progress is shown below.
20. If you see a message called *Install Requirements*, click on that and click **Install**.
21. Wait for the installation process to complete.
22. Now click on the 3 dots situated to the right side of Play button (Green triangle). Then click on *Run with script parameters*.
23. In the script parameters input box, enter `-r -v 1 -l SLP1 ramu~ GaY` and Click Apply.
24. Now click on **Run**.

## Contribute

Please open an issue in this Gitlab repository to discuss any new feature, enhancement, or bug. https://gitlab.com/vedavaapi/apps/ashtadhyayi/-/issues