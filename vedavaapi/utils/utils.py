# -*- coding: utf-8 -*-

import locale
import sys
import json
import re
import functools
from indic_transliteration import sanscript

def utf8_decode(string):
    return string

def print_dict(mydict):
    stext = json.dumps(mydict, indent=2, ensure_ascii=False, separators=(',', ': '))
    return stext

def byteify(input):
    if isinstance(input, dict):
        return {byteify(key):byteify(value) for key,value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

class DotDict(dict):
    def __getattr__(self, name):
        return self[name]

def is_ascii(s: str) -> bool:
    try:
        s.encode('ascii')
        return True
    except:
        return False

_rmchars = [ "\uFEFF", "\u200c", "\u200d", "\u201c", "\u201d" ]
def xliterate(mystr, from_script = "DEVANAGARI", to_script = "SLP1"):
    from_script = eval("sanscript." + from_script)
    to_script = eval("sanscript." + to_script)
    l = mystr
    l = re.sub(r'[' + ''.join(_rmchars) + ']', '', l)
    l = sanscript.transliterate(l, from_script, to_script)
    return l

def xliterate_obj(myobj, from_script = "DEVANAGARI", to_script = "SLP1"):
    mystr = json.dumps(myobj, indent=4, ensure_ascii=False, separators=(',', ': '))
    return json.loads(xliterate(mystr, from_script, to_script))

_loglevel = 1
def loglevel(level):
    global _loglevel
    _loglevel = int(level)
    print("Log level set to", _loglevel)

def log(*msg, level=0, indent=0):
    global _loglevel
    if level <= _loglevel:
        m = " ".join([str(m) for m in msg])
        for l in m.split('\n'):
            print("{}: {}{}".format(level, ' ' * int(indent), l))

def debuglog(*msg, level=1, indent=0):
    log(*msg, level=level, indent=indent)

def errlog(*msg, indent=0):
    log(*msg, level=0, indent=indent)
