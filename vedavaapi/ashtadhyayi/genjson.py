#!/usr/bin/env python3

# Written by: Chaitanya S Lakkundi (chaitanya.lakkundi@gmail.com)

import json
import csv
import re
from collections import OrderedDict
from sys import argv
from .langutils import xliterate

def csv_to_dict(line, heads):
    ret = [('adhikara_from', [])]
    for head, ele in zip(heads, line):
        if (not ele.strip() or ele.strip() == 'na') and head != 'Sutra_type':
            continue
        try:
            # if int is possible, make int
            ret.append((head, int(ele)))
        except:
            ret.append((head, ele))
    return OrderedDict(ret)

def process_padaccheda(compzip):
    PadacCheda = []
    ctr = 0
    while ctr < len(compzip):
        comp = dict()
        pd_comp, sm_comp = compzip[ctr]
        comp['pada'] = pd_comp
        
        if sm_comp and pd_comp != sm_comp:
            # if sm_comp == "तस्य":
                # print(compzip)
            comp['pada_split'] = sm_comp

        if ctr+1 == len(compzip):
            # no more components
            # if the last comp is not vib/vac or a comment, it is a tiNantam
            if "/" not in comp and compzip[-1][0] != "(":
                comp["type"] = "tiNantam"
            PadacCheda.append(comp)
            break
        
        if '/' in compzip[ctr+1][0]:
            tmps = compzip[ctr+1][0].split('/')
            vib, vac = tmps[:2]
            vib, vac = int(vib), int(vac)
            if vib == 0 and vac == 0:
                ptype = 'avyaya'
            else:
                ptype = 'subanta'

            comp['type'], comp['vachana'], comp['vibhakti'] = ptype, vac, vib
            
            if len(tmps) == 3:
                if "=" in tmps[2]:
                    k, v = tmps[2].split("=")
                    try:
                        v = int(v)
                    except:
                        pass
                    comp[k] = v
                if "!" in tmps[2]:
                    comp['literal'] = 1
            ctr += 1
        else:
            ptype = 'tiNantam'
            comp['type'] = ptype

        cmt = []
        if ctr+1 < len(compzip) and compzip[ctr+1][0].startswith('('):
            brc = 0
            while ctr < len(compzip):
                ctr += 1
                cmt.append(compzip[ctr][0])
                brc += compzip[ctr][0].count('(')
                brc -= compzip[ctr][0].count(')')
                if brc == 0:
                    break
            comp['comment'] = ' '.join(cmt)[1:-1]

        if comp.get('pada', '') or comp.get('pada_split', ''):
            PadacCheda.append(comp)
        ctr += 1
    return PadacCheda

def genjson(input_csv, output_file=None):
    with open(input_csv, encoding="utf-8") as fd:
        data = fd.read()
        # rmchars handled by xliterate
        # data = re.sub('[\u200c\u200d]', '', data)
        data = xliterate(data)
        data = list(csv.reader(data.splitlines()))

    out_json = OrderedDict()
    sutra_attrs = OrderedDict((w.split(' ')[0], w) for w in data[0])
    
    out_json['sutra_attrs'] = sutra_attrs
    out_json['sutras'] = OrderedDict()

    adhikara = set()
    for line in data[1:]:
        if not line:
            continue
        sutra = csv_to_dict(line, sutra_attrs.keys())
        # print(sutra)

        # Sutra_type
        if sutra['Sutra_type'].strip() == '':
            sutra['Sutra_type'] = 'viDiH'
        sutra['Sutra_type'] = [w.strip() for w in sutra['Sutra_type'].split(';')]

        sutra['Commentary'] = 'https://ashtadhyayi.com/sutraani/' + sutra['Commentary']

        # adhikara_from
        if sutra.get('Influence', ''):
            adhikara.add(tuple([int(i) for i in sutra['Influence'].split('-')]))

        for ad_start, ad_end in adhikara:
            sno = int(sutra['Sutra_krama'])
            if ad_start < sno <= ad_end:
                sutra['adhikara_from'].append(ad_start)

        if sutra['adhikara_from']:
            sutra['adhikara_from'].sort()
        else:
            sutra.pop('adhikara_from')

        # PadacCheda
        if 'PadacCheda' in sutra:
            sutra['PadacCheda'] = re.sub('[ ]+', ' ', sutra['PadacCheda']).strip()
        
        if 'SamasacCheda' in sutra:
            sutra['SamasacCheda'] = re.sub('[ ]+', ' ', sutra['SamasacCheda']).strip()

        compzip = list(zip(sutra.get('PadacCheda','').split(' '), sutra.get('SamasacCheda','').split(' ')))
        # print(compzip)
        sutra['PadacCheda'] = process_padaccheda(compzip)
        sutra.pop('SamasacCheda')

        # Anuvrtti
        Anu = []
        for comp in sutra.get('Anuvrtti', '').split('|'):
            if not comp.strip():
                continue
            # print(comp)
            sno, padas = comp.split(':')
            # add vib vac if defined, also search if comment is defined
            # TODO: below, even if one pada contains vib, vac all padas are added as dictionary entry
            if "/" in padas or "(" in padas:
                padas = re.sub('[ ]+', ' ', padas).strip()
                compzip = list(zip(padas.split(' '), padas.split(' ')))
                padas = process_padaccheda(compzip)
            else:
                padas = [p.strip() for p in padas.split(' ') if p.strip()]
            Anu.append({'sutra': int(sno), 'padas': padas})

        if Anu:
            sutra['Anuvrtti'] = Anu

        # Pada tags
        # Format: 
        # separated by pipes |
        # ! indicates literal = 1.
        # pada _space_ val _space_ vibhakti/vachana
        # if only one word, pada == val
        Pada_tags = []
        for comp in sutra.get('Pada_tags', '').split('|'):
            # default initialisation
            out = {"literal": 0}
            if not comp.strip():
                continue
            comp = re.sub('[ ]+', ' ', comp).strip()
            # process components one by one based on space
            # comments are not processed here. extract them separately if needed
            for tok in comp.split(" "):
                
                if tok == "!":
                    out["literal"] = 1

                elif "/" in tok:
                    # the number of split components must be 2
                    vib, vac = tok.split('/')
                    vib, vac = int(vib), int(vac)
                    if vib == 0 and vac == 0:
                        ptype = 'avyaya'
                    else:
                        ptype = 'subanta'
                    out['type'], out['vachana'], out['vibhakti'] = ptype, vac, vib

                elif "=" in tok:
                    k, v = tok.split("=")
                    try:
                        v = int(v)
                    except:
                        pass
                    out[k] = v

                else:
                    if "pada" not in out.keys():
                        out["pada"], out["val"] = tok, tok
                    else:
                        # second iteration
                        try:
                            out["val"] = int(tok)
                        except:
                            out["val"] = tok
            Pada_tags.append(out)
        
        if len(Pada_tags):
            sutra['Pada_tags'] = Pada_tags

        out_json['sutras'].update({str(sutra['Sutra_krama']): sutra})
    # Output
    outdata = json.dumps(out_json, ensure_ascii=False, indent=4)
    if output_file:
        with open(output_file, "w", encoding="utf-8") as fd:
            fd.write(outdata)
    else:
        print(outdata)

if __name__ == "__main__":
    input_csv = argv[1]
    try:
        output_file = argv[2]
    except:
        output_file = None
    genjson(input_csv, output_file)
'''
Bug for type in a.json:
विधीयमान! put space before !
In Anuvrtti, replace । with |; also add | where missing
place comment after actual word
4.3.134 PadacCheda missing but SamasacCheda present
6.1.147 vib/vac repeated
adhikara correction 53001-53026
{
  'comment': 'तिङ्',
  'pada': 'अकः',
  'type': 'tiNantam'
},
81074 error
14043, 14044, 64040 bug fixed कारके 14042: साधकतमम्
11071 - why is vibhakti vachana given in anuvrtti. there could be vibhakti change during Anuvrtti
21023 fix error in anuvrtti
'''