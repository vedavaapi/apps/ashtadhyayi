import os.path
from os.path import dirname, realpath, join, exists

class DotDict(dict):
    def __getattr__(self, name):
        return self[name]

My_config = DotDict({
    "sutras_json_file" : "ashtadhyayi-sutras-slp1.json",
    "sutras_csv_file" : "ashtadhyayi-sutras.csv",
    "sutras_csv_url" : "https://docs.google.com/spreadsheets/d/e/2PACX-1vTPPPmQRwqRxpcHsA64fhQr0LTXUDmoiroDAT1mQ-NVjyCfCxkKZdvK_0xQ9qFhOuQ9GEiXqcPfJKmx/pub?gid=0&single=true&output=csv",
    "rootdir" : dirname(realpath(__file__)),
    "datadir" : join(dirname(realpath(__file__)), "data"),
    "outdir" : join(dirname(realpath(__file__)), "data/output"),
#    "outdir" : "/tmp/vedavaapi/ashtadhyayi/data"
    "ashtadhyayi": None,
    "reqctx": None,
    })

def getpath(fname):
    return join(My_config['rootdir'], fname)

def outpath(fname):
    return join(My_config['outdir'], fname)

def datapath(fname):
    return join(My_config['datadir'], fname)

def set_req_context(req):
    My_config.reqctx = req

def r():
    return My_config.reqctx

def a():
    return My_config.ashtadhyayi

def config_init(rebuild=False):
    try:
        if not exists(My_config['outdir']):
            os.makedirs(My_config['outdir'])
    except Exception as e:
        print("Abort: Couldn't create directory {}: {}".format(My_config['outdir'], e))
        exit(1)
