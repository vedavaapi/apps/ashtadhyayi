from functools import reduce
from .utils import *
from .padaseq import Pada

class Siksha_abc:
    v = {
        'kAla' : [ 'hrasva', 'dIrGa', 'pluta' ],
        'hrasva' : ['a', 'i', 'u', 'f', 'x'],
        'dIrGa' : ['A', 'I', 'U', 'F', 'e', 'E', 'o', 'O' ],
        'pluta' : ['a3', 'i3', 'u3', 'f3', 'x3', 'e3', 'E3', 'o3', 'O3'],
        'anunAsika' : ['~', 'N', 'Y', 'R', 'n', 'm'],
        'anusvAra': 'M',
        'visarga': 'H',
        'jihvAmUlIya': 'Z',
        'upaDmAnIya': 'V',
        'ku': ['k', 'K', 'g', 'G', 'N'],
        'cu': ['c', 'C', 'j', 'J', 'Y'],
        'wu': ['w', 'W', 'q', 'Q', 'R'],
        'tu': ['t', 'T', 'd', 'D', 'n'],
        'pu': ['p', 'P', 'b', 'B', 'm'],

        'antasTa': ['y', 'r', 'l', 'v'],
        'UzmARa': ['S', 'z', 's', 'h'],
        'mfdu': [
                    'g', 'G', 'N',
                    'j', 'J', 'Y',
                    'q', 'Q', 'R',
                    'd', 'D', 'n',
                    'b', 'B', 'm',
                    'y', 'r', 'l', 'v', 'h'
        ],
        'kaTora': [
           'k', 'K',
           'c', 'C',
           'w', 'W',
           't', 'T',
           'p', 'P',
           'S', 'z', 's'
        ],
        'udAtta': '/',
        'anudAtta': '\\',
        'svarita': '^',
    }

    @classmethod
    def reverse_index(cls, v_props, all=False):
        r_ind = {}

        props = v_props.keys()

        for s, vlist in v_props.items():
#            print(f"reverse_idx: {s} {vlist}")
            if vlist:
                newvlist = cls.variants(vlist)
            else:
                newvlist = []
#            print(f"reverse_idx: {newvlist}")
            for v in newvlist:
                if v not in r_ind:
                    r_ind[v] = set()
                r_ind[v].add(s)

                if not all:
                    continue

                if s.find('-') >= 0:
                    r_ind[v] |= set(s.split('-'))
                else:
                    r_ind[v] |= set([p for p in v_props.keys() if p.find(s) >= 0])
        return r_ind

    @classmethod
    def __init__(cls):
        cls.svara = reduce(lambda x, y: x + y, [cls.v[k] for k in cls.v['kAla']])
        cls.prayatna = {
            'bAhya': {
                'vivAra': cls.v['kaTora'],
                'saMvAra': cls.v['mfdu'],
                'SvAsa': cls.v['kaTora'],
                'nAda': cls.v['mfdu'],
                'GoSa': cls.v['mfdu'],
                'aGoSa': cls.v['kaTora'],
                'alpaprANa': cls.v['antasTa'] + reduce(lambda x, y: x + y,
                                                        [cls.v[varga][::2] for varga in
                                                         ['ku', 'cu', 'wu', 'tu', 'pu']]),
                'mahAprANa': cls.v['UzmARa'] + reduce(lambda x, y: x + y,
                                                       [cls.v[varga][1::2] for varga in
                                                        ['ku', 'cu', 'wu', 'tu', 'pu']]),
                'udAtta': '/',
                'anudAtta': '\\',
                'svarita': '^'
            },
            'AByantara': {
                'spfzTa': reduce(lambda x, y: x + y,
                                 [cls.v[varga]
                                  for varga in ['ku', 'cu', 'wu', 'tu', 'pu']]),
                'IzatspfzTa': cls.v['antasTa'],
                'vivfta': cls.svara + cls.v['UzmARa'],
                'saMvfta': []
            }
        }

        cls.sTAna = {
            'kaRWa': ['a'] + cls.v['ku'] + ['h'] + [cls.v['visarga']],
            'tAlu': ['i'] + cls.v['cu'] + ['y', 'S'],
            'mUrDA': ['f'] + cls.v['wu'] + ['r', 'z'],
            'danta': ['x'] + cls.v['tu'] + ['l', 's'],
            'ozTa': ['u'] + cls.v['pu'] + [cls.v['upaDmAnIya']],
            'nAsika': [cls.v['anusvAra']] + [cls.v[varga][4] for varga in ['ku', 'cu', 'wu', 'tu', 'pu']],
            'kaRWa-ozTa': ['o', 'O'],
            'kaRWa-tAlu': ['e', 'E'],
            'danta-ozTa': ['v'],
        }
        #cls.v['svara'] = reduce(lambda x, y: x + y,
        #                         [cls.v[varga] for varga in ['ku', 'cu', 'wu', 'tu', 'pu']]),

        cls.v_allsTAnas = cls.reverse_index(cls.sTAna, all=True)
        cls.v_sTAna = cls.reverse_index(cls.sTAna, all=False)
        cls.v_prayatna = cls.reverse_index({**cls.prayatna['bAhya'], **cls.prayatna['AByantara']})

    @classmethod
    def max_savarna(cls, findv, vlist, fuzzy=True):
        debuglog(f"max_savarna: findv = {findv}, vlist = {vlist}")
        if isinstance(findv, list):
            findv = [Pada.nosvaras(v) for v in findv]
        else:
            findv = [Pada.nosvaras(findv)]
        findsthana = [list(cls.v_sTAna[v])[0] for v in findv]
        debuglog(f"max_savarna: findv = {findv}, sthana = {findsthana}")
        kv = [(Pada.nosvaras(v), list(cls.v_sTAna[Pada.nosvaras(v)])[0]) for v in vlist]
        debuglog(f"max_savarna: {kv}")
        filterv = [(v, s) for v,s in kv if s in findsthana]
        if filterv:
            debuglog(f"max_savarna: exact match {filterv}")
            filterv = [v for v,s in filterv]
            if set(filterv) & set(findv):
                return findv[0]
            find_prayatna = reduce(lambda x,y: x|y,
                                   [cls.v_prayatna[v] for v in findv])
            debuglog(f"max_savarna: findv prayatna {find_prayatna}")
            filterv = [(v, list(cls.v_prayatna[v] & find_prayatna)) for v in filterv]
            filterv = sorted(filterv, key=lambda v: len(v[1]), reverse=True)
            debuglog(f"max_savarna: prayatna order {filterv}")
            return filterv[0][0]
        filterv = [(v, s) for v,s in kv if findsthana[0] in s]
        if filterv:
            debuglog(f"max_savarna: approx match {filterv}")
            return filterv[0][0]
        # TODO: Check for prayatna only when it is called from _transform function directly
        # If Antaratamya of one varna needs to be found out among a list of multiple varnas
        if fuzzy:
            if len(findv) == 1:
                find_prayatna = cls.v_prayatna[findv[0]]
                for v in vlist:
                    if find_prayatna & cls.v_prayatna[v] == find_prayatna:
                        debuglog(f"matching Antaratamya returning: {v}")
                        return v

        return None
        ret = sorted(kv, key = lambda kv: (kv[1], kv[0]), reverse=True)
        debuglog(f"max_savarna: {ret}")
        return ret[0][0]

    @classmethod
    def savarRa(cls, v1, v2=None):
        v1 = Pada.nosvaras(v1)
        debuglog(f"savarRa {v1}: {cls.v_allsTAnas[v1]}")
        v1_savarnas = reduce(lambda x,y: x+y,
                             [cls.variants(cls.sTAna[s]) for s in cls.v_allsTAnas[v1]])
        debuglog(f"savarRa + variants: {v1_savarnas}")
        common = set(v1_savarnas)
        if v2:
            v2 = Pada.nosvaras(v2)
            debuglog(f"savarRa {v2}: {cls.v_allsTAnas[v2]}")
            v2_savarnas = reduce(lambda x,y: x+y,
                                 [cls.variants(cls.sTAna[s]) for s in cls.v_allsTAnas[v2]])
            debuglog(f"savarRa + variants: {v2_savarnas}")
            common &= set(v2_savarnas)
        debuglog(f"savarRa {v1} {v2}: {common}")
        return common

        debuglog(f"savarRa: common = {common}")
        common_sTAna = cls.v_allsTAnas[v1] & cls.v_allsTAnas[v2]
        if not common_sTAna:
            return []
        return common_sTAna

        common_prayatna = cls.v_prayatna[v1] & cls.v_prayatna[v2]
        return common_sTAna | common_prayatna

    @classmethod
    def dIrGa(cls, v):
        if v in cls.v['hrasva']:
            dv = v.upper()
            return dv if dv in cls.v['dIrGa'] else None
        elif v in cls.v['dIrGa']:
            return v
        elif v in cls.v['pluta']:
            hv = v.replace('3', '')
            if hv in cls.v['hrasva']:
                return cls.dIrGa(hv)
            else:
                return hv
        return None

    @classmethod
    def hrasva(cls, v):
        if v in cls.v['hrasva']:
            return v
        elif v in cls.v['dIrGa']:
            hv = v.lower()
            return hv if hv in cls.v['hrasva'] else None
        elif v in cls.v['pluta']:
            hv = v.replace('3', '')
            return hv if hv in cls.v['hrasva'] else None
        return None

    @classmethod
    def pluta(cls, v):
        if v in cls.v['hrasva']:
            return v + '3'
        elif v in cls.v['dIrGa']:
            hv = v.lower()
            return hv+'3' if hv in cls.v['hrasva'] else v + '3'
        elif v in cls.v['pluta']:
            return v
        return None

    @classmethod
    def variants(cls, v):
        if isinstance(v, list):
            return sorted(reduce(lambda x,y: x+y, [cls.variants(c) for c in v]))

        if v not in cls.svara:
            return [v]
        x = set(v)
        hv = cls.hrasva(v)
        if hv:
            x.add(hv)
        dv = cls.dIrGa(v)
        if dv:
            x.add(dv)
        pv = cls.pluta(v)
        if pv:
            x.add(pv)
        return sorted(x)

Siksha = Siksha_abc()