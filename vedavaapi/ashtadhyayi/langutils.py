import json
import re
from indic_transliteration import sanscript


def dumper(obj):
    try:
        if isinstance(obj, set):
            return list(obj)
        elif callable(obj):
            return obj.__name__ + "()"
        res = obj.__repr__()
        return res
    except Exception as e:
        print(f"dumper: caught exception: {e}")
        ret = obj.__dict__
        print(f"dumper: {ret}")
        return ret

def utf8_decode(string):
    return string

def print_dict(mydict):
    stext = json.dumps(mydict, default=dumper, indent=2, ensure_ascii=False, separators=(',', ': '))
    return stext

def byteify(input):
    if isinstance(input, dict):
        return {byteify(key):byteify(value) for key,value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def _transcribe(input, modifier):
    if isinstance(input, tuple):
        input = list(input)
    if isinstance(input, dict):
        return dict((key, _transcribe(value, modifier)) for key,value in input.items())
    elif isinstance(input, list):
        return [_transcribe(element, modifier) for element in input]
    elif isinstance(input, str):
        inp = modifier(input)
        return inp
    else:
        return input

def transcribe(obj, from_script, to_script):
    return _transcribe(obj,
               lambda x: xliterate(x, from_script=from_script, to_script=to_script))

def is_ascii(s: str) -> bool:
    try:
        s.encode('ascii')
        return True
    except:
        return False

_rmchars = [ "\uFEFF", "\u200c", "\u200d", "\u201c", "\u201d" ]
_accents = { "\u0952" : "\\", "\u0951" : "/" }
def xliterate(mystr, from_script = "DEVANAGARI", to_script = "SLP1"):
    from_script = eval("sanscript." + from_script)
    to_script = eval("sanscript." + to_script)
    l = mystr
    l = re.sub(r'[' + ''.join(_rmchars) + ']', '', l)
    for a,aval in _accents.items():
        l = l.replace(a, aval)
        #l = re.sub(r'[' + a + ']', aval, l)
    l = sanscript.transliterate(l, from_script, to_script)
    return l

def xliterate_obj(myobj, from_script = "DEVANAGARI", to_script = "SLP1"):
    mystr = json.dumps(myobj, indent=4, ensure_ascii=False, separators=(',', ': '))
    return json.loads(xliterate(mystr, from_script, to_script))
