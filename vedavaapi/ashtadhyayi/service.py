#!/usr/bin/python
# -*- coding: utf-8 -*-

import os.path
#from .rule import *
from .padaseq import *
from .shiksha import *
from .config import *
from .subanta import *
from .rule import *
import copy
from functools import reduce
import re
import inspect
import traceback
from urllib.request import urlopen
from .genjson import genjson

AND = None
OR = None
NOT = None
SAMJNA = None
PRATYAYA = None
IT_ENDING = None

class Ashtadhyayi:

    @logged_func
    def __init__(self):
        global AND
        AND = self._and
        global OR
        OR = self._or
        global NOT
        NOT = self._not
        global SAMJNA
        SAMJNA = self.is_samjna
        global IT_ENDING

        self.sutra_ids = []
        self.sutra_id_idx = {}
        self.samjna_defs = {}
        self.samjna_notdefs = set() # -ve cache of failed samjna evaluations
        self.computed_samjna_defs = []
        self.computed_samjna_sutra_ids = set()

    def load(self, sutras_json_file, rebuild=False):
        # Initialize Subanta vibhakti analyzer
        info("Loading Ashtadhyayi sutras ..")
        Subanta.load(rebuild)
        with open(sutras_json_file, encoding='utf-8') as data_file:
            try:
                self.ashtadhyayi = json.load(data_file)
                self.sutra_ids = sorted(self.ashtadhyayi['sutras'].keys())
                for i in range(len(self.sutra_ids)):
                    self.sutra_id_idx[self.sutra_ids[i]] = i
            except ValueError as err: # catch *all* exceptions
                errlog("Error: Format error in JSON file: ", err, ": aborting.", sutras_json_file)
                raise err

        log("Loading upadesha data ..")
        self._load_upadeshas(rebuild)
        log("Inferring Praatipadikas for all padas ..")
        self._infer_praatipadikas(rebuild)
        log("Building mahavakyas for all sutras ..")
        self._build_mahavakyas(rebuild)
        log("Pre-processing Samjna definitions ..")
        self._extract_samjna_defs(rebuild)
        log("Pre-processing Vidhi sutras ..")
        self._compile_vidhi_rules(rebuild)

        if rebuild:
            log("Evaluating 'hal' samjna ..")
            res = self.eval_samjna('hal', sutra_id=11071)
            log("Eval Samjna hal returned", res)
            self.forget_samjna('ac')

            log("Preparing mAheSvara sutras for pratyahaara expansion ..")
            for i,s_desc in enumerate(self.upadesha['mAheSvara']):
                log("  Processing maheswara sutra: ", s_desc, indent=2)
                log("    tagging halantyam it: ", s_desc, indent=2)
                res = self.label_samjna(s_desc, 'it', sutra_id='13003')
                s_desc, _ = self.apply_vidhi_rule(s_desc, sutra_id='13009')
                prev_seq = PadaSequence(s_desc.prev_transforms[0]['pseq'])
                log("    after 'it' lopa: {} ({})".format(s_desc, " ".join(prev_seq.varnas())), indent=2)
                self.upadesha['mAheSvara'][i] = s_desc

            res = self.eval_samjna('ac', sutra_id=11071)
            #res['members']['val'] = sorted(xliterate_obj(res['members']['val'], "SLP1", "DEVANAGARI"))
            log("Eval Samjna ac returned", res)
            for i, s_desc in enumerate(self.upadesha['mAheSvara'][5:6]):
                s_desc = s_desc.pop()
                del s_desc.labels['it']
                res, _ = self.apply_vidhi_rule(s_desc, sutra_id='13009')
                log("    after 'it' lopa: {} ({})".format(s_desc, " ".join(res.varnas())), indent=2)
                self.upadesha['mAheSvara'][5+i] = s_desc

            # Now recompute 'hal' to remove 'a~' from its expansion
            self.forget_samjna('hal')
            res = self.eval_samjna('hal', sutra_id=11071)

        info("Done initializing Ashtadhyayi Engine.")

    def _and(self, pada_desc, conds):
        return reduce((lambda x, y: x and y),
            [self._propmatch(pada_desc, c) for c in conds])

    def _or(self, pada_desc, conds):
        return reduce((lambda x, y: x or y),
            [self._propmatch(pada_desc, c) for c in conds])


    def _not(self, pada_desc, cond):
        #print_dict(pada_desc)
        #print_dict(cond)
        return not self._propmatch(pada_desc, cond)

    def _load_upadeshas(self, rebuild=False):
        log("    Loading mAheSvara sutras ..")
        self.upadesha = { 'mAheSvara' : [], 'BUvAdi' : [] }
        self.upadesha_idx = {}
        with open(datapath("maheshvara-sutras.txt"), encoding='utf-8') as f:
            for l in f.readlines():
                s = Pada.split(l.strip(), '~')
                rest = [e for e in filter(lambda x: x != 'a', s[1:])]
                s = [s[0]] + rest
                sutra = PadaSequence(padas=["".join(s)]).add_label("padas.upadeSa",
                                                           positions=[0], value="mAheSvara")
                sutra.parts['padas'][0]['pada'] = l.strip()
                self.upadesha['mAheSvara'].append(sutra)
                self.upadesha_idx[sutra.parts['padas'][0]['pada']] = [sutra]

        hal_sutra = self.upadesha['mAheSvara'][13]
        hal_sutra.add_label('varnas.it', positions=[1])

        log("    Loading dhaatu paatha ..")
        with open(datapath("dhaatu_patha_slp1.json"), encoding='utf-8') as f:
            dhatus = []
            try:
                dhatus = json.load(f)
            except ValueError as err: # catch *all* exceptions
                errlog("Error: Format error in JSON file: ", err, ": aborting.", "dhaatu_patha_slp1.json")
                raise err

        self.upadesha['BUvAdi'] = []
        for d in dhatus:
            dhatu_nosvaras = Pada.nosvaras(d['dhatu'])
            dhatu_short = Pada.nosvaras(d['dhatu_mod'])
            p = PadaSequence(padas=[d['dhatu']])
            p.add_label("padas.upadeSa", positions=[0], value="BUvAdi")
            for k,v in d.items():
                p.add_label(f"padas.{k}", positions=[0], value=v)
            self.upadesha['BUvAdi'].append(p)
            if dhatu_nosvaras not in self.upadesha_idx:
                self.upadesha_idx[dhatu_nosvaras] = []
            self.upadesha_idx[dhatu_nosvaras].append(p)
            if dhatu_short not in self.upadesha_idx:
                self.upadesha_idx[dhatu_short] = []
            self.upadesha_idx[dhatu_short].append(p)

    def locate_in_upadesha(self, varna):
        locations = []
        self.debuglog("Checking {} in mAheSvara".format(varna))
        for i, sutra in enumerate(self.upadesha['mAheSvara']):
            if sutra.prev_transforms:
                sutra = PadaSequence(sutra.prev_transforms[0]['pseq'])
            pos = [j for j, v in enumerate(sutra.parts['varnas'])
                if varna == re.sub('[/\\\^~]*$', '', v)]
            if pos:
                locations.append({'key': 'mAheSvara', 'idx': i, 'pos': pos})
        if locations:
            self.debuglog(ppformat(locations))
        return locations

    def get_upadesha(self, location):
        sutra = self.upadesha[location['key']][location['idx']]
        if sutra.prev_transforms:
            sutra = PadaSequence(sutra.prev_transforms[0]['pseq'])
        return sutra.subset(vinds=location['pos'])

    def expand_pratyahara(self, first, last):
        gana = self.upadesha[first['key']]
        i1 = first['idx']
        i2 = last['idx']
        pos = [first['pos'][0], last['pos'][0]]
#        seq = set()
        seq = []
        self.debuglog("expand_pratyahara: ", pos)
        for i in range(i1, i2+1):
            gcomps = [v for v in gana[i].varnas()]
            self.debuglog("expand_pratyahara: ", gcomps, level=1)
            if i == i1:
#                seq |= set(gcomps[pos[0]:] if i < i2 else gcomps[pos[0]:pos[1]])
                seq.extend(gcomps[pos[0]:] if i < i2 else gcomps[pos[0]:pos[1]])
            else:
#                seq |= set(gcomps if i < i2 else gcomps[:pos[1]])
                seq.extend(gcomps if i < i2 else gcomps[:pos[1]])
        if seq:
            seq = reduce(lambda x, y: x+y,
                            [Siksha.variants(v) for v in seq])
        self.debuglog("expand: ", ppformat(seq))
        return list(seq)

    def is_samjna(self, samjna):
        match = samjna in self.samjna_defs
#        print("is_samjna {} = {}".format(samjna, match))
        return match

    def is_anunAsika(self, pseq):
        pos = []
        for i in pseq.views['varnas']:
            v = pseq.varnas(i)
            if '~' in v or v in Siksha.v['anunAsika']:
                pos.append(i)
        return pos

    def upadesha_category(self, pada):
        if pada in self.upadesha_idx:
            return [p.labels['upadeSa']['padas'][0]['val']
                    for p in self.upadesha_idx[pada]
                        if 'upadeSa' in p.labels]
        return []

    def is_upadesha(self, pseq):
        if 'upadeSa' in pseq.labels:
            debuglog(f"is_upadesha: preexisting - {pseq.labels['upadeSa']}")
            pinds = reduce(lambda x,y: x + y,
                        [linfo['pos'] for linfo in pseq.labels['upadeSa']['padas']])
            pinds = sorted(set(pinds) & set(pseq._derive_view('padas')))
            debuglog(f"is_upadesha: pinds={pinds}")
            return pseq.subset(pinds=pinds) if pinds else None
        pinds = []
        self.debuglog('In is_upadesha', pseq)
        for i,p in enumerate(pseq.padas()):
            pada = Pada.nosvaras(p['pada'])
            cat = self.upadesha_category(pada)
            if cat:
                for c in cat:
                    pinds.append(i)
                    pseq.add_label('padas.upadeSa', positions=[i], value=c)
        return pseq.subset(pinds=pinds) if pinds else None

    def is_category(self, pseq, category):
        if category in pseq.labels:
            if category == 'BUvAdi':
                debuglog(f"is_DAtu: preexisting", ppformat(pseq.labels[category]))
            return pseq.find_label(category)
        if not self.is_upadesha(pseq):
            return None
        found = False
        self.debuglog(f'In is_{category}', pseq)
        for m in pseq.labels['upadeSa']['padas']:
            if m['val'] == category:
                self.debuglog(f"is_{category}: found", m)
                pseq.add_label(f"padas.{category}", positions=m['pos'], value=True)
                return pseq.find_label(category)
        return pseq if found else None

    # This check for pratyaya was added to detect
    # when to interpret saptami vibhakti as 'arthe' instead of 'pare'
    # If in a vidhi sutra, a prathama vibhakti pada is NOT a pratyaya,
    #   the saptami padas in that sutra will be interpreted as 'arthe'
    def is_pratyaya(self, pada):
        for k, v in self.samjna_defs.items():
            if 'members' in v:
                m = v['members']
                if m['type'] == 'padas':
                    if pada in m['val']:
                        return True
        return False

    def it_ending(self, pada_desc, parms):
        if not (pada_desc['type'] == 'subanta' and
                pada_desc['vibhakti'] == 1):
            return False
        pada = pada_desc['pada']
        match = pada.endswith(parms["varna"])
        if match:
    #        print "Matched", pada, "with ", parms["varna"]
            pass
        return match

    def sutra(self, sutra_id):
        try:
            sutra = self.ashtadhyayi["sutras"][str(sutra_id)]
            return sutra
        except ValueError as err: # catch *all* exceptions
            log("Error: " + str(sutra_id) + " not found.")
            return None

    def split_sutra(self, sutra_id):
        s = self.sutra(sutra_id)
        return " ".join([p['pada'] for p in s['PadacCheda']])

    def _propmatch(self, myobj, cond = None):
        if not cond:
            return True

        ismatch = False
        if isinstance(cond, list):
            if callable(cond[0]):
                # If matching with list, condition must apply to any item
                if isinstance(myobj, list):
                    return reduce((lambda x, y: x or y),
                        [cond[0](o, cond[1]) for o in myobj])
                # If dict, condition must apply to it
                if isinstance(myobj, dict):
                    return cond[0](myobj, cond[1])
            # If cond is a list and myobj is scalar, match any list item
            ismatch = reduce((lambda x, y: x or y),
                [self._propmatch(myobj,  v) for v in cond])
        elif callable(cond):
            return cond(myobj)
        elif isinstance(cond, dict):
            if isinstance(myobj, list):
                if not myobj:
                    return False
                # cond must match any list item
                ismatch = reduce((lambda x, y: x or y),
                    [self._propmatch(o, cond) for o in myobj])
                return ismatch
            if not isinstance(myobj, dict):
                return False

            # Both query and object are dictionaries
            # Hence match the values for each of the keys.
            ismatch = True
            for k in cond.keys():
                if k not in myobj:
                    return False

                if not self._propmatch(myobj[k], cond[k]):
                    return False
        else:
#            if isinstance(cond, str):
#                cond = cond.decode('utf-8')
            if isinstance(myobj, list):
                ismatch = reduce((lambda x, y: x or y),
                    [(o == cond) for o in myobj])
            else:
                ismatch = (myobj == cond)

        if not ismatch:
            return False
        return True

    def equal_dvng(self, w1, w2):
#        ma = "म्"
#        ma = ma.decode('utf-8')
#        if not (w1.endswith(ma) or w2.endswith(ma)):
#            return w1 == w2
#        w1 = sanscript.transliterate(w1, sanscript.DEVANAGARI, sanscript.SLP1)
#        w2 = sanscript.transliterate(w2, sanscript.DEVANAGARI, sanscript.SLP1)
        w1 = re.sub(r'[mM]$', 'm', w1)
        w2 = re.sub(r'[mM]$', 'm', w2)
        return w1 == w2

    def sutras(self, match_props = None, reverse=False):
        sutra_ids = self.sutra_ids
        if reverse:
            sutra_ids = reversed(sutra_ids)
        return (snum for snum in sutra_ids if self._propmatch(self.sutra(snum), match_props))

    def _infer_praatipadikas(self, reset=False):
        self.upadesha['Agama'] = []
        check_anuvrtti = {}
        for s_id in self.sutras():
            sutra = self.sutra(s_id)
            ptags = sutra['Pada_tags'] if 'Pada_tags' in sutra else []
            for p in sutra['PadacCheda']:
#                p['pada'] = sanscript.transliterate(p['pada'],
#                                               sanscript.DEVANAGARI, sanscript.SLP1)
#                if 'pada_split' in p:
#                    p['pada_split'] = sanscript.transliterate(p['pada_split'],
#                                                   sanscript.DEVANAGARI, sanscript.SLP1)
                srchlist = []
                if 'pada_split' in p:
                    srchlist.append(p['pada_split'])
                else:
                    srchlist.append(p['pada'])
                if 'vibhakti' in p:
                    p['praatipadikam'] = Subanta.praatipadikam(p)
                    srchlist.append(p['praatipadikam'])

                for t in ptags:
                    if t['pada'] in ['overlaps', 'overrides']:
                        sutra[t['pada']] = t['val']
                    for pada in srchlist:
                        if t['pada'] in pada:
                            if 'tags' not in p:
                                p['tags'] = {}
                            p['tags'][t['pada']] = t
                if 'tags' in p:
                    debuglog("Sutra {}: {}".format(s_id, ppformat(p)))
                    pass

                if 'vibhakti' in p and p['praatipadikam']:
                    if p['vibhakti'] == 1 and p['pada'] != 'luk' and p['praatipadikam'].endswith(('w', 'k', 'm')):
                        oldp = p['pada']
                        p['pada'] = re.sub(r'([aiu])([km])$', r'\1~\2', p['pada'])
                        p['praatipadikam'] = re.sub(r'([aiu])([km])$', r'\1~\2', p['praatipadikam'])
                        self.upadesha['Agama'].append(p)
                        newp = self.pada_override(p)
                        self.upadesha_idx[newp['praatipadikam']] = \
                            [PadaSequence(padas=[newp['praatipadikam']]).add_label("padas.upadeSa",
                                                                           positions=[0], value="Agama")]
                        #self.debuglog(f"fix_pada: {oldp} -> {p['pada']}", level=1)
                        if oldp not in check_anuvrtti:
                            check_anuvrtti[oldp] = p

            if 'Anuvrtti' in sutra:
                for vrttam in sutra['Anuvrtti']:
                    for i,p in enumerate(vrttam['padas']):
                        if isinstance(p, dict):
                            continue
                        if p in check_anuvrtti:
                            vrttam['padas'][i] = check_anuvrtti[p]['pada']
                            #self.debuglog(f"fix_anuvrtti: {p} -> {vrttam['padas'][i]}", level=1)

    def pada_override(self, f_desc):
        if 'tags' not in f_desc:
            return f_desc

        new_desc = None
        if 'pada_split' in f_desc:
            components = Subanta.praatipadikam(f_desc)
            if not components:
                return None
            newcomps = []
            new_desc = copy.deepcopy(f_desc)
            new_desc['tags'] = {}
            if f_desc['pada'] in f_desc['tags']:
                tag = f_desc['tags'][f_desc['pada']]
                for k, v in tag.items():
                    if k not in ['val']:
                        new_desc[k] = v
            for c in components.split('-'):
                if c not in f_desc['tags']:
                    newcomps.append(c)
                    continue
                tag = f_desc['tags'][c]
                newc = tag['val']
                new_desc['tags'][newc] = tag.copy()
                newcomps.append(newc)
            new_desc['pada_split'] = "-".join(newcomps)
            new_desc['praatipadikam'] = "-".join(newcomps)
        else:
            if f_desc['pada'] in f_desc['tags']:
                new_desc = copy.deepcopy(f_desc)
                new_desc['tags'] = {}
                tag = f_desc['tags'][f_desc['pada']]
                for k,v in tag.items():
                    if k not in ['val']:
                        new_desc[k] = v
                new_desc['pada'] = tag['val']
                new_desc['tags'][tag['val']] = tag.copy()
                del new_desc['praatipadikam']
                if tag['val'] != '-' and tag['val'].find('-') >= 0:
                    new_desc['pada_split'] = tag['val']
                new_desc['praatipadikam'] = Subanta.praatipadikam(new_desc)
        return new_desc if new_desc else f_desc

    def extract_samjna_enums(self):
        not_terms = set()
        with open(datapath("pratyaya-notlist.txt"), encoding='utf-8') as f:
            lines = f.readlines()
            for l in lines:
                l = xliterate(l.rstrip())
                not_terms.add(l)

        for g,vals in Siksha.v.items():
            self.samjna_defs[g] = { 'members': { 'type': 'varnas', 'val': vals } }
            not_terms.add(g)
        self.samjna_defs['sup'] = \
            { 'members': { 'type': 'padas',
                           'val': self.sutra('41002')['PadacCheda'][0]['pada_split'].split('-') } }
        not_terms.add('sup')
        self.samjna_defs['tiN'] = \
            { 'members': { 'type': 'padas',
                           'val': self.sutra('34078')['PadacCheda'][0]['pada_split'].split('-') } }
        not_terms.add('tiN')

        for p in self.upadesha['BUvAdi']:
            gana = p.labels['gaRa']['padas'][0]['val']
            if gana not in self.samjna_defs:
                self.samjna_defs[gana] = \
                    { 'members': { 'type': 'padas', 'val': [] }}
                not_terms.add(gana)
            self.samjna_defs[gana]['members']['val'].append(p.padas(0)['pada'])

            iT = p.labels['iT']['padas'][0]['val']
            if iT not in self.samjna_defs:
                self.samjna_defs[iT] = \
                    { 'members': { 'type': 'padas', 'val': [] }}
                not_terms.add(iT)
            self.samjna_defs[iT]['members']['val'].append(p.padas(0)['pada'])

        for t,term_desc in self.samjna_defs.items():
            if 'defns' not in term_desc:
                continue
            for defn in term_desc['defns']:
                s = self.sutra(defn['sutra_id'])
                if 'aDikAraH' in s['Sutra_type']:
                    if 'Influence' not in s:
                        print(s)
                    sfirst, slast = s['Influence'].split('-')
                    starti = self.sutra_id_idx[sfirst] + 1
                    stopi = self.sutra_id_idx[slast] + 1
                    members = {}
                    for i in range(starti, stopi):
                        as_id = self.sutra_ids[i]
                        a_s = self.sutra(str(as_id))
                        if 'Anuvrtti' not in a_s:
                            continue
                        if t == 'kfttiN':
                            print_dict(a_s['Anuvrtti'])
                        t_exists = reduce(lambda x, y: x or y,
                                        [(t in a['praatipadikas']) for a in a_s['Anuvrtti']])
                        if not t_exists:
                            continue

                        if 'saMjYA' in a_s['Sutra_type']:
                            myterm = a_s['Term']
                            if myterm not in members:
                                members[myterm] = []
                            members[myterm].append(as_id)
                            continue

                        # Pick words in prathama-vibhakti as candidates
                        for p in a_s['PadacCheda']:
                            if 'type' not in p:
                                print_dict(p)
                                sys.exit(1)
                            p = self.pada_override(p)
                            if p['type'] == 'subanta' and p['vibhakti'] == 1:
                                if 'vAcya' in p and p['vAcya'] == 'arTa':
                                    continue

                                praatipadikam = Subanta.praatipadikam(p)
                                if not praatipadikam:
                                    self.debuglog(f"extract_samjna_enums: {ppformat(p)} no praatipadikam")
                                    continue
                                padas = praatipadikam.split('-') if 'pada_split' in p else [praatipadikam]
                                padas = [p for p in padas if p not in not_terms]
                                if not padas:
                                    continue
                                #print snum, pada.encode('utf-8')
                                #if pada.endswith(u"\u200C"):
                                #    continue
                                for p in padas:
                                    if p not in members:
                                        members[p] = []
                                    members[p].append(as_id)
                    term_desc['members'] = { 'type' : 'padas', 'val' : members }
                    self.upadesha[t] = members.keys()
                    for m in members.keys():
                        if m not in self.upadesha_idx:
                            self.upadesha_idx[m] = [PadaSequence(padas=[m]).add_label("padas.upadeSa", positions=[0], value=t)]
                        else:
                            self.debuglog("{} found more than once in upadesha".format(m), level=2)
                    self.debuglog("{} -> ({}) {}".format(t, len(self.upadesha[t]), ", ".join(self.upadesha[t])), level=2)

        # Extract sanAdi pratyaya list
        sanadi_range = set([str(s) for s in range(31005, 31032)])
        sanadi = [p for p,sutras in
                  self.samjna_defs['pratyaya']['members']['val'].items()
                    if set(sutras) & set(sanadi_range)]

        self.samjna_defs['sanAdi'] = \
            { 'members': { 'type': 'padas', 'val': sanadi } }
        not_terms.add('sanAdi')

    def compile_samjna_rules(self):
        self.computed_samjna_defs = []
        self.computed_samjna_sutra_ids = set()
        for t,term_desc in self.samjna_defs.items():
            if 'members' in term_desc:
                continue
            for defn in term_desc['defns']:
                defn['rule'] = Rule(self).compile_samjna(t, defn['defn'])
                if isinstance(defn['rule'][0], str) and defn['rule'][0] == 'GEN_SAMJNA':
                    if defn['sutra_id'] == '11069':
                        continue
                    self.computed_samjna_sutra_ids.add(defn['sutra_id'])
                    self.computed_samjna_defs.append(defn)
        for i,d in enumerate(self.computed_samjna_defs):
            # Move Sabdasya a-SabdasamJYA sutra to the end of computed_samjna sutras
            # It is a catch-all.
            if d['sutra_id'] == '11068':
                e = self.computed_samjna_defs[i]
                del self.computed_samjna_defs[i]
                self.computed_samjna_defs.append(e)
                break

#        for i,d in enumerate(self.computed_samjna_defs):
#            self.debuglog("{}: {}".format(d['sutra_id'],
#                                          ppformat(d['rule'])), level=0)

    def label_samjna(self, pseq: PadaSequence, samjna, sutra_id=None):
        if samjna not in self.samjna_defs:
            return None

        t = samjna
        term_desc = self.samjna_defs[t]
        found_match = False
        if 'members' in term_desc:
            m = term_desc['members']
#            self.debuglog("label_samjna: {} defined as members {}".format(ppformat(t), ppformat(m)), context=context)
            if m['type'] == 'padas':
                pos = []
                for i in pseq._derive_view('padas'):
                    if isinstance(m['val'], str):
                        if pseq.padas(i)['pada'].find(m['val']) >= 0:
                            pos.append(i)
                    elif pseq.padas(i)['pada'] in m['val']:
                            pos.append(i)

#                pos = [i for i in pseq._derive_view('padas')
#                            if pseq.padas(i)['pada'].find(m['val']) >= 0]
#                pos = [i for i,p in enumerate(pseq.padas()) if p['pada'] in m['val']]
                if pos:
                    pseq.add_label(".".join(['padas', t]), positions=pos, value=True)
                    found_match = True
            elif m['type'] == 'varnas':
                debuglog(f"label_samjna({samjna}): looking in ", pseq)
                pos = [i for i in pseq._derive_view('varnas')
                       if Pada.find_varna(pseq.varnas(i), m['val'])]
                if pos:
                    if 'sutra_id' in m:
                        sutra_id = m['sutra_id']
                    pseq.add_label(".".join(['varnas',t]), positions=pos, sutra_id=sutra_id)
                    found_match = True
        else:
            for d in term_desc['defns']:
                if 'rule' not in d:
                    continue
                if sutra_id and d['sutra_id'] != sutra_id:
                    continue
                if d['sutra_id'] in self.computed_samjna_sutra_ids:
                    continue
                debuglog("label_samjna({}, {}):\nChecking Samjna sutra {} {} ..".format(
                    ppformat(samjna), pseq, ppformat(d['sutra_id']),
                    ppformat(self.sutra(d['sutra_id'])['Sutra_text'])))
                res = Rule(self, d['rule']).apply(pseq)
                if res:
                    debuglog(f"res = {ppformat(res)}")
                    if samjna in res.labels:
                        for part,positions in res.labels[samjna].items():
                            for p in positions:
                                if 'val' in p:
                                    pval = p['val'] if 'val' in p else None
                                    pseq.add_label(".".join([part, samjna]),
                                                   positions=p['pos'], sutra_id=d['sutra_id'],
                                                   value=pval)
                    for part,pos in res.views.items():
                        pseq.add_label(".".join([part, samjna]), positions=pos,
                            sutra_id=d['sutra_id'])

        if samjna not in pseq.labels:
            return None
        return pseq.find_label(samjna)

    def log(self, *msg, level=1):
        log(*msg, level=level)

    def debuglog(self, *msg, level=2):
        debuglog(*msg, level=level)

    def forget_samjna(self, samjna):
        if samjna in self.samjna_notdefs:
            self.samjna_notdefs.remove(samjna)
        if samjna not in self.samjna_defs:
            return
        t = self.samjna_defs[samjna]
        if 'members' in t:
            self.debuglog("forgetting samjna {} = {}".format(samjna, t))
            del t['members']
        if not t.keys():
            del self.samjna_defs[samjna]

    # Evaluate what given samjna word should stand for.
    # samjna must be in praatipadika form
    @logged_func
    def eval_samjna(self, samjna, *, sutra_id=None):
        if samjna in self.samjna_defs:
            return self.samjna_defs[samjna]
        if samjna in self.samjna_notdefs:
            log(f"eval_samjna: skipping {samjna} as it couldn't be evaluated earlier")
            return None

        r().skip_word(samjna)
        self.debuglog("Evaluating new Samjna word", ppformat(samjna))
        if sutra_id:
            sutra_id = str(sutra_id)

        for d in self.computed_samjna_defs:
            if sutra_id and d['sutra_id'] != sutra_id:
                continue
            debuglog("eval_samjna: Checking {} against sutra {} {} ..".format(
                ppformat(samjna), ppformat(d['sutra_id']),
                ppformat(self.sutra(d['sutra_id'])['Sutra_text'])))
            samjnaObj = PadaSequence(padas=[samjna])
            samjnaObj.add_label('padas.upadeSa', positions=[0], value=True)
            res = Rule(self, d['rule']).apply(samjnaObj)
            if res:
                v = res.labels[samjna]
                val = v['padas'][0]['val'] if 'padas' in v else v['varnas'][0]['val']
                val['sutra_id'] = d['sutra_id']
                self.samjna_defs[samjna] = { 'members': val }
                self.debuglog("eval_samjna({}) = {}".format(ppformat(samjna), ppformat(self.samjna_defs[samjna]['members'])))
                return self.samjna_defs[samjna]

        # Remember that this samjna couldn't be evaluated earlier
        if not sutra_id:
            self.samjna_notdefs.add(samjna)
        return None

    # Generate a sequence from Ashtadhyayi
    # represented by given start and end aksharas. For instance, 
    #   aN means a i u
    #   sup means "su au jas aM auT Shas .."
    def gen_sequence(self, start, end):
        self.debuglog("gen_sequence")
        return self.expand_pratyahara(start, end)
        #seq = PadaSequence(padas=["".join(seq)]).add_label("padas.upadeSa",
        #                                                   positions=[0], value="mAheSvara")
        #s_desc = self.apply_vidhi_rule(seq, sutra_id='13009')

    def _extract_samjna_defs(self, rebuild):
        samjna_file = outpath("samjna.json")
        if os.path.isfile(samjna_file):
            if rebuild:
                os.remove(samjna_file)
            else:
                with open(samjna_file, encoding='utf-8') as f:
                    try:
                        self.samjna_defs = json.load(f)
                        return
                    except ValueError as err: # catch *all* exceptions
                        errlog("Error: Format error in JSON file: ", err, ": aborting.", samjna_file)

        log("Extracting samjna definitions from samjna sutras ..")
        self.samjna_defs = {}
        term_defs_str = ""
        term_enums_str = ""
        for snum in self.sutras({ 'Sutra_type' : 'saMjYA'}):
            s = self.sutra(snum)
            for t in s['Term'].split():
                # Search for samjna in sutra's padacCheda
                pdesc = Subanta.analyze({'pada' : t,
                    'vibhakti' : 1, 'vachana' : 1})
                #print_dict(pdesc)
                if pdesc:
                    praatipadikam = pdesc[0]['praatipadikam']
                if not praatipadikam:
                    log("Error: Couldn't find praatipadika for ", t)
                    exit(1)
                if praatipadikam not in self.samjna_defs:
                    self.samjna_defs[praatipadikam] = { 'pada' : t, 'defns' : [] }

                defn = []
                def_padas = []
                for p in s['mahavakya_padacCheda']:
                    if p['pada'] == t:
                        continue
                    #print_dict(p)
                    if 'vibhakti' in p:
                        found = False
                        a = Subanta.analyze(p)
                        for pada_a in a:
                            # collect the ids of sutras that define computed samjnas
                            for term_a in pdesc:
                                #print pada_a['praatipadikam'] + " == " + term_a['praatipadikam']
                                if pada_a['praatipadikam'] == term_a['praatipadikam']:
                                    found = True
                                    break
                            if found:
                                break
                        if found:
                            continue

                    if 'pada_split' in p:
                        #pada_split_slp1 = sanscript.transliterate(p['pada_split'], 
                        #    sanscript.DEVANAGARI, sanscript.SLP1)

                        split_desc = { 'pada' : p['pada_split'],
                            'vibhakti' : p['vibhakti'],
                            'vachana' : p['vachana'] }
                        plist = Subanta.analyze(split_desc)
                        if plist is None:   
                            log("Error getting praatipadika of ", p['pada_split'])
                            exit(1)
                        found = False
                        for w_p in plist:
                            w = w_p['praatipadikam']
                            found = reduce(lambda x, y: x or y,
                                [(pada == praatipadikam) 
                                    for pada in w.split('-')])
                            if found:
                                break
                        if found:
                            continue
                    #print_dict(p)
                    v_desc = p['vibhakti'] if 'vibhakti' in p else -1
                    def_padas.append("{}({})".format(p['pada'], v_desc))
                    defn.append(p)
#                defn = xliterate_obj(defn)
                self.samjna_defs[praatipadikam]['defns'].append({'sutra_id' : snum, 'defn' : defn})
                if def_padas:
                    term_defs_str += "{}: {} = {}\n".format(snum, praatipadikam, ' '.join(def_padas))

        log("Extracting enumerable samjnas ..")
        self.extract_samjna_enums()
        log("Compiling Samjna sutras for processing ..")
        self.compile_samjna_rules()

        with open(samjna_file, "w", encoding='utf-8') as f:
            stext = json.dumps(self.samjna_defs, indent=2, ensure_ascii=False, separators=(',', ': '))
            f.write(stext + "\n")

        with open(outpath("upadeshas.json"), "w", encoding='utf-8') as f:
            dump = {}
            for k,v in self.upadesha_idx.items():
                dump[k] = [p.toJSON() for p in v]

            stext = json.dumps(dump, indent=2, ensure_ascii=False, separators=(',', ': '))
            f.write(stext + "\n")

        with open(outpath("term_defs_slp1.txt"), "w", encoding='utf-8') as f:
            f.write(term_defs_str + "\n")
        with open(outpath("term_members_slp1.txt"), "w", encoding='utf-8') as f:
            for k,v in self.samjna_defs.items():
                if 'members' in v:
                    vmembers = { 'type': v['members']['type']}
                    if isinstance(v['members']['val'], dict):
                        vmembers['val'] = list(v['members']['val'].keys())
                    else:
                        vmembers['val'] = v['members']['val']
                    f.write("{} = {}\n".format(k, vmembers))

    def _compile_vidhi_rules(self, rebuild):
        vidhi_file = outpath("vidhis.json")
        if os.path.isfile(vidhi_file):
            if rebuild:
                os.remove(vidhi_file)
            else:
                with open(vidhi_file, encoding='utf-8') as f:
                    try:
                        self.vidhi_defs = json.load(f)
                        return
                    except ValueError as err:  # catch *all* exceptions
                        errlog("Error: Format error in JSON file: ", err, ": aborting.", vidhi_file)

        log("Loading the list of vidhi sutras to process ..")
        vidhi_sutra_nums = []
        vidhi_sutras_file = datapath("vidhi-sutras.txt")
        try:
            if not os.path.isfile(vidhi_sutras_file):
                vidhi_sutra_nums = self.sutras({'Sutra_type': ['viDiH', 'atideSaH']})
            else:
                with open(vidhi_sutras_file, encoding='utf-8') as f:
                    vidhi_sutra_nums = re.split(r'\s+', f.read())
#            print(f"{ppformat(vidhi_sutra_nums)}")
        except ValueError as err:  # catch *all* exceptions
            errlog(f"Error opening file {vidhi_sutras_file}: {err}: aborting")
            raise err

        log("Compiling vidhi sutras ..")
        self.vidhi_defs = {
            'snums': [],
            'snums_preprocess': ['13009', # '61009',
                                 '31025', '91002',
                                 '61064', '61065', '61071', '71058', '71001',
                                 '34067', '31081', '31079', '31078', '31077',
                                 '31073', '31069', '31068',
                                 '24075', '24072', '61010',
                    '64051', '64052', '61067',
                    '73078', '74040',
                                 #'82078'
                                 ],
            'snums_tripadi': [],
            'rules': {}}

        for snum in vidhi_sutra_nums:
            if not snum:
                continue
            s = self.sutra(snum)
            self.vidhi_defs['rules'][snum] = Vidhi_rule(self, snum).compile(s['mahavakya_padacCheda'])
            #if snum == '31025':
                #print(f"compile_vidhi 31025 mvpadaccheda: {ppformat(s['mahavakya_padacCheda'])}")
                #print(f"compile_vidhi 31025 rule: {ppformat(self.vidhi_defs['rules'][snum])}")
            for p in s['mahavakya_padacCheda']:
                # print_dict(p)
                if not ('vibhakti' in p and p['vibhakti'] in [3, 5, 6, 7]):
                    continue

                if 'pada_split' in p:
                    split_desc = {'pada': p['pada_split'],
                                  'vibhakti': p['vibhakti'],
                                  'vachana': p['vachana']}
                    pada = Subanta.praatipadikam(split_desc)
                else:
                    pada = Subanta.praatipadikam(p)

                indname = "index" if snum < "82001" else "index_tripadi"
            if snum < "82001":
                if snum not in self.vidhi_defs['snums_preprocess']:
                    self.vidhi_defs['snums'].append(snum)
            else:
                self.vidhi_defs['snums_tripadi'].append(snum)

        with open(outpath("vidhi.json"), "w", encoding='utf-8') as f:
            stext = json.dumps(self.vidhi_defs, indent=2, ensure_ascii=False, separators=(',', ': '))
            f.write(stext + "\n")

    def _build_mahavakyas(self, rebuild=False):
        for snum in self.sutras():
            sutra = self.sutra(snum)
            if rebuild or 'mahavakya_padacCheda' not in sutra:
                sutra['mahavakya_padacCheda'] = self.mahavakya_padacCheda(snum)

    def mahavakya_padacCheda(self, sutra_id):
        sutra = self.sutra(sutra_id)
        if not sutra:
            return None

        ptags = sutra['Pada_tags'] if 'Pada_tags' in sutra else []
        new_vaakya = []
        if 'Anuvrtti' in sutra:
            for vrttam in sutra['Anuvrtti']:
                prev_id = str(vrttam['sutra'])
                prev_sutra = self.sutra(prev_id)
                prev_padaccheda = prev_sutra['PadacCheda']

                praatipadikas = []
                for p in vrttam['padas']:
                    if isinstance(p, dict):
                        p['praatipadikam'] = Subanta.praatipadikam(p)
                        praatipadikas.append(p['praatipadikam'])
                        new_vaakya.append(p)
                    else:
                        for prevp in prev_padaccheda:
                            if self.equal_dvng(p, prevp['pada']):
                                for t in ptags:
                                    if t['pada'] == p:
                                        debuglog(
                                            f"mahavakya anuvrtti({prev_id}) tag_overlay: {sutra_id} tags {t}")
                                        newp = copy.deepcopy(prevp)
                                        debuglog(f"mahavakya anuvrtti({prev_id}) base: {newp}")
                                        if 'tags' not in newp:
                                            newp['tags'] = {p: t}
                                        elif p not in newp['tags']:
                                            newp['tags'][p] = t
                                        else:
                                            # Merge overriding tags into prevp's tags
                                            for k,v in t.items():
                                                if (k not in newp['tags'][p]) or v != '-':
                                                    newp['tags'][p][k] = v
                                        prevp = newp
                                new_vaakya.append(prevp)
                                praatipadikam = Subanta.praatipadikam(prevp)
                                if 'tags' in prevp:
                                    debuglog(f"mahavakya anuvrtti({prev_id}) tag_final: {sutra_id} {prevp} {praatipadikam}")
                                praatipadikas.append(Subanta.praatipadikam(prevp))
                                break
                vrttam['praatipadikas'] = praatipadikas

        new_vaakya.extend(sutra['PadacCheda'])
        new_vaakya = sorted(new_vaakya, key=lambda x: int(x['seq']) if 'seq' in x else 0)
        return new_vaakya

    @logged_func
    def apply_vidhi_rule(self, pseq: PadaSequence, *, sutra_id):
        if sutra_id in r().loglevels:
            loglevel(r().loglevels[sutra_id])
        stext = " ".join([p['pada'] for p in self.sutra(sutra_id)['PadacCheda']])
        mahavakya = " ".join([p['pada'] for p in self.sutra(sutra_id)['mahavakya_padacCheda']])
        xform = {'sutra_id': sutra_id,
                 'sutra_text': stext,
                 'sutra_mahavakya': mahavakya,
                 'input': str(pseq),
                 'output': None,
                 'trace': None,
                 'modified': 0}
        self.log("Applying Vidhi Sutra {} {} on Pada seq {}".
                     format(sutra_id, xform['sutra_text'], pseq))
                        #ppformat([p['pada'] for p in pseq.padas()])))
        if sutra_id not in self.vidhi_defs['rules']:
            raise RuntimeError(f"Error: Sutra {sutra_id} not compiled.")

        rule = self.vidhi_defs['rules'][sutra_id]
        res, modified, trace = Vidhi_rule(self, sutra_id, rule).apply(pseq)
        if res:
            log(f"Apply {sutra_id} returned modified={modified} {ppformat(res)}")
        #xform['result'] = res
        xform['output'] = str(res) if res else None
        xform['modified'] = modified
        xform['trace'] = trace
        if sutra_id in r().loglevels:
            loglevel(r().loglevels[0])
        return res, xform

    def apply_vidhi_rules_once(self, pseq: PadaSequence, *, sutras=[],
                               matchone=False, prevmatch_sutra=None):
        transforms = []
        res = pseq
        if not sutras:
            return None

        sutra_stack = list(reversed(sutras))
        prev_matches = {}
        while sutra_stack:
            snum = sutra_stack.pop()
            if snum in res.skip_sutras:
                continue
#            print(f"once: Applying {snum} {prevmatch_sutra}")
            res2, xform = self.apply_vidhi_rule(res, sutra_id=snum)
            if res2 and snum in prev_matches:
                res_orig = prev_matches[snum]['result']
                mods_orig = res_orig.prev_transforms[-1]['mods']
                mods_new = res2.prev_transforms[-1]['mods']
                debuglog(f"apply_vidhi_sutras_once: orig = {pformat(mods_orig)}\n new = {pformat(mods_new)}")
                if mods_new['part'] == mods_orig['part']:
                    if not set(mods_new['inds']) & set(mods_orig['inds']):
                        res2 = None
                elif mods_orig['part'] == 'varnas':
                    pinds_orig = [i for i, p in enumerate(res2.prev_transforms[-1]['pseq']['parts']['padas'])
                                    if (set(mods_orig['inds']) & set(range(*p['pos'])))]
                    prev_seq_new = res2.prev_transforms[-1]['pseq']
                    pinds_new = set()
                    for i in mods_new['inds']:
                        prange = prev_seq_new['parts']['padas'][i]['pos']
                        if i > 0 and prange[0] == prange[1]:
                            i = i - 1
                        pinds_new.add(i)
                    if not (set(pinds_orig) & pinds_new):
                        res2 = None
                if not res2:
                    debuglog(f"apply_vidhi_sutras_once: overriding sutra doesn't overlap")

            if not res2:
                if snum not in prev_matches:
                    continue
                # Revert back to the result of latter matched Vidhi sutra
                xform = prev_matches[snum]
                del prev_matches[snum]
                log(f"apply_vidhi_sutras_once: not matched {snum}; reverting to earlier match {xform['sutra_id']}")
                res = xform['result']
                del xform['result']
            else:
                res = res2
                if snum == prevmatch_sutra:
                    # TODO: what if we remove this sutra from sutra_stack
                    # Currently, we are just continuing if same sutra is applied twice.
                    continue
                    raise RuntimeError(f"Same sutra {snum} applied twice; aborting.")
                if xform['modified']:
                    prevmatch_sutra = None
                if snum in prev_matches:
                    # Discard the result of latter vidhi sutra
                    log(
                        f"apply_vidhi_sutras_once: matched {snum}; discarding earlier match {prev_matches[snum]['sutra_id']}")
                    del prev_matches[snum]

                # Does this sutra have an apavaada? Process that next
                s = self.sutra(snum)
                if 'overlaps' in s:
                    # Save the present match
                    log(f"apply_vidhi_rules_once: matched {snum}; saving result to be overridden by {s['overlaps']}")
                    s_apavaada = str(s['overlaps'])
                    xform['result'] = res
                    prev_matches[s_apavaada] = xform
                    sutra_stack.append(s_apavaada)
                    res = res.pop()
                    continue
                elif 'overrides' in s:
                    s_overridden = str(s['overrides']).split(",")
                    for s_ovr in s_overridden:
                        if s_ovr in sutra_stack:
                            log(f"apply_vidhi_rules_once: skipping overridden sutra {s_ovr}")
                            res.skip_sutras.add(s_ovr)

            transforms.append(xform)

            if xform['modified']:
                if matchone:
                    break
        return res, transforms

    @logged_func
    def apply_vidhi_rules(self, pseq: PadaSequence, *, sutras=[]):

        pre_sutras = self.vidhi_defs['snums_preprocess']
        if sutras:
            pre_sutras.extend(sutras)

        # Apply Pre-processing sutras first
        transforms = []
        res = pseq
        if pre_sutras:
            res2, xforms = self.apply_vidhi_rules_once(res, sutras=pre_sutras)
            if res2:
                res = res2
                transforms.extend(xforms)
        if sutras:
            return res, transforms

        # Auto-selection mode: Traverse sapaada saptaadhyayi sutras in reverse order
        prev_sutra = None
        while True:
            res2, xforms = self.apply_vidhi_rules_once(res,
                                       sutras=list(reversed(self.vidhi_defs['snums'])),
                                       matchone=True,
                                       prevmatch_sutra=prev_sutra)
            if not res2:
                log(f"apply_vidhi_sutras: didn't match any pre-tripaadi sutras in this round.")
                break
            res = res2
            if not (xforms and xforms[-1]['modified']):
                log(f"apply_vidhi_sutras: No transformation by pre-tripaadi sutras in this round.")
                break
            prev_sutra = xforms[-1]['sutra_id']
            transforms.extend([x for x in xforms if x['modified']])

        # Traverse tripaadi sutras in increasing order of sutra #
        log(f"apply_vidhi_sutras: Trying tripaadi sutras ...")
        res2, xforms = self.apply_vidhi_rules_once(res,
                                   sutras=self.vidhi_defs['snums_tripadi'])
        if res2:
            res = res2
            transforms.extend([x for x in xforms if x['modified']])

        return res, transforms

    def mahavakya(self, sutra_id):
        s = self.sutra(sutra_id)
        return s['mahavakya_padacCheda']

    def add_dhatu_props(self, pdescs):
        # Transform dhatu pada to its svara version in the right gaNa
        # from dhaatu paatha
        for p in pdescs:
            dhatu_nosvara = Pada.nosvaras(p['pada'])
            if isinstance(p, dict) and 'dhatu' in p['label'] and \
                    dhatu_nosvara in self.upadesha_idx:
                found = False
                for d in self.upadesha_idx[dhatu_nosvara]:
                    if 'gaRa' in d.labels:
                        if 'gaRa' not in p:
                            p['gaRa'] = d.labels['gaRa']['padas'][0]['val']
                        if p['gaRa'] == d.labels['gaRa']['padas'][0]['val']:
                            p['pada'] = d.padas(0)['pada']
                            for l, linfo in d.labels.items():
                                if 'padas' not in linfo:
                                    continue
                                if l == 'upadeSa':
                                    continue
                                p[l] = linfo['padas'][0]['val']
                            found = True
                            break

    @logged_func
    def interpret(self, pdescs, sutras=[], result=[], inscr="SLP1", outscr="SLP1"):
        script(outscr)
        transforms = []
        exp_result = [Pada.nosvaras(xliterate(x, inscr, "SLP1")) for x in result]
        pdescs = transcribe(pdescs, inscr, "SLP1")
        log(f"interpret: padas={pdescs} exp_result={exp_result} sutras={sutras}")
        try:
            # Transform dhatu pada to its svara version in the right gaNa
            # from dhaatu paatha
            self.add_dhatu_props(pdescs)

            res = PadaSequence(padas=pdescs)
            res2, transforms = self.apply_vidhi_rules(res, sutras=sutras)
            if res2:
                res = res2
            else:
                log(f"Error: No vidhi sutras matched.")
            if res:
                output = Pada.nosvaras("".join(res.varnas()))
                info("Finally, {} = {}".format(ppformat([p['pada'] for p in pdescs]), ppformat(output)))
                success = (not exp_result or (output in exp_result))
                if not success:
                    log("Failed.")
            return { 'padas': transcribe(pdescs, "SLP1", outscr),
                     'generated': xliterate(output, "SLP1", outscr),
                     'expected': transcribe(exp_result, "SLP1", outscr),
                     'mods': transcribe(transforms, "SLP1", outscr),
                     'success': success,
                     }
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
            info("Failed: Caught exception", e)
        return None

def ashtadhyayi_init(rebuild=False):
    if rebuild or not My_config.ashtadhyayi:
        config_init(rebuild)
        try:
            data = urlopen(My_config.sutras_csv_url).read().decode("utf-8")
            open(datapath(My_config.sutras_csv_file), "w", encoding="utf-8").write(data)
        except Exception as e:
            print("error getting ", My_config.sutras_csv_url, repr(e))
            sys.exit(1)
        genjson(datapath(My_config.sutras_csv_file), datapath(My_config.sutras_json_file))
        set_req_context(ReqContext())
        script("SLP1")
        My_config.ashtadhyayi = Ashtadhyayi()
        My_config.ashtadhyayi.load(datapath(My_config.sutras_json_file), rebuild)
        set_req_context(None)
    return My_config.ashtadhyayi

class Paribhasha:
    def __init__(self, sutra_id, defs):
        self.defs = defs
        self.sutra_id = sutra_id
        self.a = a()
        self.sutra = self.a.sutra(sutra_id)

    def applies(self, sutra):
        found = False
        modifications = []
        for m in self.defs:
            cond = m['cond']
            action = m['action']
            if isinstance(m['cond'], dict) and self.a._propmatch(sutra, cond):
                modifications.append({ 'cond' : cond, 'action' : action })
        return modifications

    def matching_sutras(self):
    	return (snum for snum in self.a.sutra_ids \
            if len(self.applies(self.a.sutra(snum))) > 0)

if __name__ == "__main__":
    if (len(sys.argv) < 2):
        print("Usage: " + sys.argv[0] + " <sutranum>")
        print("    For sutra 1.1.1, give 11001")
        exit(1)

    sutra_id = sys.argv[1];

    a = ashtadhyayi_init()

    sutra = a.sutra(sutra_id)
    stext = json.dumps(sutra, indent=4, ensure_ascii=False, separators=(',', ': '))
    #print type(sutra)
    #print(byteify(sutra))
    print(stext)
