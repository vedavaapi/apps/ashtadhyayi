#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import functools
from .utils import *

from collections.abc import Sequence

class Pada:
    def __init__(self, pada='', split_by=[], attrs={}):
        self.pada = pada
        self.attrs = copy.deepcopy(attrs)

    def from_dict(self, obj):
        for k,v in obj.items():
            self.k = v

    def __getitem__(self, name):
        return self.__getattr__(name)

    @classmethod
    def split(cls, pada, split_by=''):
        pattern = r'[^{schars}][{schars}]*'.format(schars=split_by)
        res = re.findall(pattern, pada)
        #res2 = [res[0]] + list(filter(lambda s: s != 'a', res[1:]))
        return res

    @classmethod
    def nosvaras(cls, pada):
        return re.sub(r'[\\\/^]', '', pada)

    @classmethod
    def find_varna(cls, srchv, vlist, exact=False):
        if exact:
            return [srchv in vlist]
        srchv = re.sub('[~/\\\^]*$', '', srchv)
        for v in vlist:
            v = re.sub('[~/\\\^]*$', '', v)
            if srchv == v:
                return True
        return False

# A sequence of morphemes to be analyzed
class PadaSequence(Sequence):
    def __init__(self, myseq=None, *, srcseq=None,  padas=[], pinds = None, vinds = None):
        if myseq:
            self.parts = myseq['parts']
            self.labels = myseq['labels']
            self.views = myseq['views']
            self.skip_sutras = myseq['skip_sutras']
            self.samjnas = []
            self.prev_transforms = myseq['prev_transforms']
        else:
            self.parts = {'padas': [], 'varnas': []}
            self.labels = {}
            self.views = {'padas' : [], 'varnas': []}
            self.skip_sutras = set()
            self.samjnas = []
            self.prev_transforms = []

        if srcseq:
            self.parts = copy.deepcopy(srcseq.parts)
            self.labels = copy.deepcopy(srcseq.labels)
            self.views = copy.deepcopy(srcseq.views)
            self.skip_sutras = copy.deepcopy(srcseq.skip_sutras)
            self.samjnas = copy.deepcopy(srcseq.samjnas)
            self.prev_transforms = srcseq.prev_transforms

        if padas:
            all_varnas = []
            def import_pada(p):
                pvarnas = Pada.split(p.strip(), '/3\\\^~')
                self.parts['padas'].append({'pada': p,
                                        'pos': (len(all_varnas),
                                                len(pvarnas) + len(all_varnas))})
                all_varnas.extend(pvarnas)

            for p in padas:
                if isinstance(p, dict):
                    import_pada(p['pada'])
                    for k,v in p.items():
                        if k not in ['pada', 'label']:
                            self.add_label(f"padas.{k}", positions=[len(self.parts['padas'])-1], value=v)
                else:
                    import_pada(p)

            self.parts['varnas'] = all_varnas

        # If both pinds and vinds weren't supplied set them to full set
        if pinds == None and vinds == None:
            pinds = list(range(0, len(self.parts['padas'])))
#            vinds = list(range(0, len(self.parts['varnas'])))

        if pinds != None:
            self.views['padas'] = pinds
        elif 'padas' in self.views:
            del self.views['padas']

        if vinds != None:
            self.views['varnas'] = vinds
        elif 'varnas' in self.views:
            del self.views['varnas']

    def subset(self, *, pinds=None, vinds=None):
        newpseq = PadaSequence(srcseq=self, pinds=pinds, vinds=vinds)
        debuglog(f"subset of {self} is {newpseq}")
        return newpseq

    def delpadas(self, pinds):
        for pind in reversed(pinds):
            self.delvarnas(list(range(*self.padas(pind)['pos'])))
            self.views['padas'] = \
                [i-1 if i > pind else i
                       for i in self.views['padas'] if i != pind]
            del self.parts['padas'][pind]
            for l,e in self.labels.items():
                if 'padas' in e:
                    newlinfo = []
                    for linfo in e['padas']:
                        newpos = []
                        for i in linfo['pos']:
                            if i == pind:
                                continue
                            elif i < pind:
                                newpos.append(i)
                            else:
                                newpos.append(i-1)
                        if not newpos:
                            continue
                        linfo['pos'] = newpos
                        newlinfo.append(linfo)
                    if not newlinfo:
                        del e['padas']
                    else:
                        e['padas'] = newlinfo
        debuglog("delpada returned ", self)

    def delvarnas(self, vinds):
        # Compute the new varna inds as a result of deletion
        new_vinds = {}
        ndeleted = 0
        for i in range(len(self.parts['varnas'])):
            new_vinds[i] = i - ndeleted if i in vinds else None
            if i in vinds:
                new_vinds[i] = None
                ndeleted += 1
            else:
                new_vinds[i] = i - ndeleted

        trunc_pos = lambda l: [new_vinds[i] for i in l if new_vinds[i]!=None]
        debuglog(f"delvarnas: new_inds = {new_vinds}", level=1)

        # Adjust varnas list
        for i in sorted(vinds, reverse=True):
            del self.parts['varnas'][i]
        # Adjust varna ranges in padas
        maxi = 0
        for i,p in enumerate(self.parts['padas']):
            (o1, o2) = p['pos']
            if o1 == o2:
                (n1, n2) = (maxi, maxi)
            else:
                (n1, n2) = (new_vinds[o1], new_vinds[o2-1])
                debuglog(f"delvarnas {o1},{o2} -> {n1},{n2}", level=1)
                if n2 != None:
                    n2 += 1
                if n1 == None:
                    n1 = maxi
                if n2 == None:
                    for j in reversed(range(o1,o2)):
                        debuglog(f"{j}->{new_vinds[j]}")
                        if new_vinds[j] != None:
                            n2 = new_vinds[j]+1
                            break
                    if n2 == None:
                        n2 = maxi
            maxi = n2
            debuglog(f"delvarnas {o1},{o2} -> {n1},{n2}", level=1)
            self.parts['padas'][i]['pos'] = (n1, n2)

        # Adjust varna indices in labels
        dellabels = []
        for l,linfo in self.labels.items():
            if 'varnas' in linfo:
                delinds = set()
                for i,e in enumerate(linfo['varnas']):
                    newpos = trunc_pos(e['pos'])
                    if newpos:
                        linfo['varnas'][i]['pos'] = newpos
                    else:
                        delinds.add(i)

                for i in sorted(list(delinds), reverse=True):
                    del linfo['varnas'][i]
                if not linfo['varnas']:
                    dellabels.append(l)
        for l in dellabels:
            del self.labels[l]

        # Adjust varna indices in views
        if 'varnas' in self.views:
            self.views['varnas'] = trunc_pos(self.views['varnas'])
        debuglog("delvarna returned ", self)

    # replace = 0 means insert, 1 means replace purva, 2 means purva & para
    def upsertvarnas(self, vinds, insvarnas, replace=0):
        debuglog(f"upsertvarnas: In {self.parts['varnas']} replace = {replace} old_vinds {vinds}, new varnas {insvarnas}")
        # First delete old varnas
        if replace and not insvarnas:
            deletelen = len(vinds)
            vind = vinds[0]
            for i in range(deletelen):
                self.parts['varnas'][vind] = self.parts['varnas'][vind+i]
            vinds = [i+deletelen for i in vinds]
            debuglog(f"upsertvarnas: {self.parts['varnas']} new vinds = {vinds}")
            self.delvarnas(vinds)
            return

        pos = vinds[0]

        # Compute the new indices of the varnas after insertion
        pada_vinds = [list(range(*p['pos'])) for p in self.parts['padas']]
        debuglog(f"upsertvarnas: old pada_vinds={pada_vinds}")
        oldlen = len(vinds) if replace else 0
        newlen = len(insvarnas)
        oldv_pind = {}
        for pi,p_vinds in enumerate(pada_vinds):
            for i in p_vinds:
                oldv_pind[i] = pi
        new_vinds = {}
        newvarnas = []
        curpind = 0
        pada_newvinds = [[] for p in self.parts['padas']]
        for i,v in enumerate(self.parts['varnas']):
            curpind = oldv_pind[i]
            if i < vinds[0]:
                new_vinds[i] = (i, curpind)
                pada_newvinds[curpind].append(i)
                newvarnas.append(v)
                continue
            if i in vinds:
                if replace:
                    new_vinds[i] = (-1, -1)
                else:
                    new_vinds[i] = (i, curpind)
                    newvarnas.append(v)
                    pada_newvinds[curpind].append(i)
                if i == vinds[0] and insvarnas:
                    newvarnas.extend(insvarnas)
                if i == vinds[-1]:
                    pada_newvinds[curpind].extend(list(range(len(newvarnas)-newlen, len(newvarnas))))
            if i > vinds[-1]:
                new_vinds[i] = (i + newlen - oldlen, curpind)
                pada_newvinds[curpind].append(len(newvarnas))
                newvarnas.append(v)

        debuglog(f"upsertvarnas: new pada_vinds={pada_newvinds}")
        self.parts['varnas'] = newvarnas

        # Adjust varna ranges in padas
        p_vind = 0
        for i,vlist in enumerate(pada_newvinds):
            if not vlist:
                self.parts['padas'][i]['pos'] = (p_vind, p_vind)
            else:
                p_vind = vlist[-1]+1
                self.parts['padas'][i]['pos'] = (vlist[0], p_vind)

        new_pos = lambda l: [new_vinds[i][0] for i in l if new_vinds[i][0] >= 0]

        debuglog(f"upsertvarnas: new varna inds: {new_vinds} ")

        debuglog(f"upsertvarnas: parts = {self.parts} ")

        # Adjust varna indices in labels
        for l,linfo in self.labels.items():
            if 'varnas' in linfo:
                for i,e in enumerate(linfo['varnas']):
                    linfo['varnas'][i]['pos'] = new_pos(e['pos'])

        # Adjust varna indices in views
        if 'varnas' in self.views:
            self.views['varnas'] = new_pos(self.views['varnas'])
            self.views['varnas'][pos:pos] = [i+pos for i in range(len(insvarnas))]

        # Finally, install the new varnas
        debuglog(f"pseq.upsertvarnas: parts={ppformat(self.parts)}, "
                 f"views={ppformat(self.views)}, labels={ppformat(self.labels)}")

    # Incorporate pada sequence pseq into self by
    # either replacing or inserting at given pos
    def modpada(self, pseq, pos, replace=None):
        pvarnas = pseq.varnas()
        vind = self.parts['padas'][pos]['pos'][1]
        pind = pos+1
        # adjust varna pointers everywhere to reflect the insertion
        for i in range(pind, len(self.parts['padas'])):
            pvrange = self.parts['padas'][i]['pos']
            self.parts['padas'][i]['pos'] = \
                (pvrange[0] + len(pvarnas), pvrange[1] + len(pvarnas))
        self.parts['padas'].insert(pind, ({'pada': pseq.padas(0)['pada'],
                                           'pos': (vind,
                                                   vind + len(pvarnas))}))
        self.parts['varnas'][vind:vind] = pvarnas
        debuglog("pseq.modify: parts {}".format(ppformat(self.parts)))

        self.views['padas'] = [i+1 if i >= pind else i for i in self.views['padas']]
        self.views['padas'].insert(pind, pind)
        if 'varnas' in self.views:
            self.views['varnas'] = [i+len(pvarnas) if i >= vind else i for i in self.views['varnas']]
            self.views['varnas'] = sorted(self.views['varnas'] + list(range(vind, vind + len(pvarnas))))
            debuglog("pseq.modify: views {}".format(ppformat(self.views)))

        for l,e in self.labels.items():
            #print(f"pseq.modify before: {l}: {e}")
            if 'padas' in e:
                for linfo in e['padas']:
                    linfo['pos'] = [i+1 if i >= pind else i for i in linfo['pos']]
            #print(f"pseq.modify after: {l}: {e}")
            if 'varnas' in e:
                for linfo in e['varnas']:
                    linfo['pos'] = [i+len(pvarnas) if i >= vind else i for i in linfo['pos']]

        # Transfer labels from pseq to self
        for l,e in pseq.labels.items():
            if 'padas' in e:
                for linfo in e['padas']:
                    pos = None
                    if 'pos' in linfo:
                        pos = [i+pind for i in linfo['pos']]
                    self.add_label(f"padas.{l}", positions=pos,
                                   sutra_id=linfo['sutra_id'] if 'sutra_id' in linfo else None,
                                value=linfo['val'] if 'val' in linfo else None)
            if 'varnas' in e:
                for linfo in e['varnas']:
                    pos = None
                    if 'pos' in linfo:
                        pos = [i + vind for i in linfo['pos']]
                    self.add_label(f"varnas.{l}", positions=pos,
                                   sutra_id=linfo['sutra_id'] if 'sutra_id' in linfo else None,
                                   value=linfo['val'] if 'val' in linfo else None)

        for hist in pseq.prev_transforms:
            self.prev_transforms.append(hist)
        return pind

    def varnas(self, ind=-1):
        return self.getParts('varnas') if ind < 0 else self.getPart('varnas', ind)

    def padas(self, ind=-1):
        return self.getParts('padas') if ind < 0 else self.getPart('padas', ind)

    # Derive pada view from varna view and vice versa
    def _derive_view(self, part):
        if part in self.views:
            return self.views[part]

        if part == 'padas':
            if 'varnas' not in self.views:
                return []
            vinds = self.views['varnas']
            pinds = [i for i, p in enumerate(self.parts['padas']) if set(vinds) & set(range(*p['pos']))]
            return pinds
        if part == 'varnas':
            if 'padas' not in self.views:
                return []
            pinds = self.views['padas']
            vinds = [] if not pinds else functools.reduce(lambda x, y: x + y,
                                     [list(range(*self.parts['padas'][i]['pos'])) for i in pinds])
            return vinds
        return []

    def getPart(self, part, ind):
        if ind >= 0:
            return self.parts[part][ind]
        view = self._derive_view(part)
        if not view:
            debuglog("getPart error:", ppformat(self.views))

    def getParts(self, part):
        view = self._derive_view(part)
        return [self.parts[part][i] for i in view]

    def __getitem__(self, ind=0):
        return self.varnas(ind)
    def __len__(self):
        return len(self.parts['varnas'])

    def push(self, sutra_id, mods):
        mods = dict((k, mods[k]) for k in ['inds', 'part', 'replace'])
        pseq_old = copy.deepcopy(vars(self))
        pseq_old['prev_transforms'] = []
        prev_transform = {'sutra_id': sutra_id, 'pseq': pseq_old, 'mods': mods }
        debuglog(f"push: sutra_id {sutra_id} mods {mods}")
        self.prev_transforms.append(prev_transform)

    def pop(self):
        if not self.prev_transforms:
            return None
        clone_xforms = copy.deepcopy(self.prev_transforms)
        newp = PadaSequence(clone_xforms[-1]['pseq'])
        newp.prev_transforms = clone_xforms
        newp.prev_transforms.pop()
        return newp

    def add_label(self, label, positions=None, *, sutra_id=None, value=None):
        label_rec = {}
        if positions:
            label_rec['pos'] = positions
        if sutra_id:
            label_rec['sutra_id'] = sutra_id
        if value:
            label_rec['val'] = value

        debuglog("Add label {} to {}: {}".format(label, str(self),
                                                 ppformat(label_rec)))
        c = label.split('.')
        if c[1] not in self.labels:
            self.labels[c[1]] = {}
        if c[0] not in self.labels[c[1]]:
            self.labels[c[1]][c[0]] = []
        self.labels[c[1]][c[0]].append(label_rec)
        #debuglog("added {}: {}".format(label, label_rec), level=0)
        return self

    def lookup_label(self, label):
#        linfo = None
#        pseq = self
        if label in self.labels:
            return [self]
        else:
            return [PadaSequence(t['pseq'])
                      for t in reversed(self.prev_transforms) if label in t['pseq']['labels']]

    # Return a copy of self with only those padas labelled with given label
    def find_label(self, label):
        debuglog("find_label: ", label, self)
        retlist = []
        views = {}
        matchseqs = self.lookup_label(label)
        if not matchseqs:
            return None
        pseq = matchseqs[0]
        linfo = pseq.labels[label]
#        pseq, linfo = self.lookup_label(label)
#        if not linfo:
#            return None

        debuglog("find_label: ", label, pseq, linfo)
        pinds = None
        vinds = None
        vseqs = []
        if 'val' in linfo:
            if linfo['type'] == 'varnas':
                vinds = linfo['val']['val']
            elif linfo['type'] == 'padas':
                pinds = linfo['val']['val']
        if 'padas' in linfo:
            view_pinds = set(self._derive_view('padas'))
            for p in linfo['padas']:
                if 'pos' not in p:
                    return None
                if not (view_pinds & set(p['pos'])):
                    continue
                if 'val' in p and isinstance(p['val'], dict):
                    debuglog(f"find_label: label={label} {p}")
                    if p['val']['type'] == 'varnas':
                        vinds = p['val']['val']
                        vseqs.append(vinds)
                    elif p['val']['type'] == 'padas':
                        pinds = p['val']['val']
            if not (vinds or pinds):
#                pinds = sorted(pinds & set([p['pos'][0] for p in linfo['padas']]))
                pinds = reduce(lambda x,y: x | y,
                                         [set(p['pos']) for p in linfo['padas']])
                debuglog(f"find_label: pinds={pinds} in {pseq}")
                if self == pseq:
                    pinds &= set(self._derive_view('padas'))
                pinds = sorted(pinds)
                debuglog(f"find_label: pinds={pinds}")
        if 'varnas' in linfo:
            vinds = set(self._derive_view('varnas'))
            recvarnas = functools.reduce(lambda x, y: set(x) | set(y),
                                [rec['pos'] for rec in linfo['varnas']])
            debuglog(f"find_label view={vinds} label={recvarnas}")
            vinds = sorted(vinds & set(recvarnas))
        if pinds == [] and not vinds:
            return None
        if vinds == [] and not pinds:
            return None
        if vinds or pinds or vseqs:
            log("find_label: found {}: {} {} {}".format(
                label,
                "padas=" + str(ppformat([pseq.padas(i)['pada'] for i in pinds])) if pinds else "",
                "vseqs=" + str(ppformat(vseqs)) if vseqs else "",
                "varnas=" + str(ppformat([pseq.varnas(i) for i in vinds])) if vinds else ""))
        r = pseq.subset(pinds=pinds, vinds=vinds)
        if vseqs:
            r.views['vseqs'] = vseqs
        return r

    def matching_pinds(self, srcseq, pinds):
        if srcseq != self:
            newpinds = set()
            for i in pinds:
                for j in self._derive_view('padas'):
                    if srcseq.padas(i)['pada'] == self.padas(j)['pada']:
                        newpinds.add(j)
            pinds = sorted(list(newpinds))
        return pinds

    def intersect(self, pseq):
        if not pseq:
            return None
        common_pinds = None
        common_vinds = None
        for i in self._derive_view('padas'):
            p1 = self.padas(i)
            for p2 in pseq.padas():
                if p1['pada'] != p2['pada']:
                    continue
                if 'padas' in self.views:
                    if not common_pinds:
                        common_pinds = []
                    common_pinds.append(i)
                if 'varnas' in self.views:
                    vpada1 = "".join([self.varnas(i) for i in range(*p1['pos'])])
                    vpada2 = "".join([pseq.varnas(i) for i in range(*p2['pos'])])
                    if vpada1 not in vpada2:
                        continue
                    vinds1 = sorted(set(range(*p1['pos'])) & set(self.views['varnas']))
                    vinds1 = [i-p1['pos'][0] for i in vinds1]
                    if not common_vinds:
                        common_vinds = []
                    vinds2 = sorted(set(range(*p2['pos'])) & set(pseq._derive_view('varnas')))
                    vinds2 = [i-p2['pos'][0] for i in vinds2]
                    vinds = sorted(set(vinds1) & set(vinds2))
                    if vinds:
                        common_vinds.extend([i+p2['pos'][0] for i in vinds])
                break
        if not (common_pinds or common_vinds):
            return None
        debuglog(f"_intersect: {self} AND {pseq} pinds={common_pinds}, vinds={common_vinds}")
        return self.subset(pinds=common_pinds, vinds=common_vinds)

        for part in ['padas', 'varnas']:
            common_inds[part] = None
            if part not in self.views:
                continue
            debuglog(f"intersect {part} {ppformat(self.views)}, {ppformat(pseq.views)}")
            common_inds[part] = sorted(set(self.views[part]) & set(pseq._derive_view(part)))
            if not common_inds[part]:
                return None
        #debuglog(f"intersect {self.views} {pseq.views} {common_inds}")
        return self.subset(pinds=common_inds['padas'], vinds=common_inds['varnas'])

    def __repr__(self):
        x = vars(self)
        xforms = x['prev_transforms']
        x['prev_transforms'] = []
        ret = "Pseq-> {}".format(ppformat(x))
        x['prev_transforms'] = xforms
        return ret

    def toJSON(self, hist=True):
        x = vars(self)
        ret = "Pseq-> {}".format(ppformat(x))
        return ret

    def __str__(self):
        vpadas = []
        varnas = self.parts['varnas']
        for p in self.padas():
            vpada = []
            for j in range(0, len(varnas)):
                if j in range(*p['pos']):
                    vpada.append(self.varnas(j))
            vpadas.append("".join(vpada))

        return "{} - {} - {}".format(
            " ".join([p['pada'] for p in self.padas()]),
            " ".join(vpadas),
            " ".join(self.varnas()))

    def add_samjna(self, samjna):
        if 'vseqs' in self.views:
            vseqs = []
            for seq in self.views['vseqs']:
                vseqs.append("".join([self.varnas(i) for i in seq]))
            val = vseqs
        elif 'varnas' in self.views:
            val = self.varnas()
        elif 'padas' in self.views:
            val = [p['pada'] for p in self.padas()]

#        print(f"samjna: {samjna} - {val}")
        sutras = set()
        if samjna in self.labels:
#            print(f"samjna labels: {pformat(self.labels[samjna])}")
            for k,vals in self.labels[samjna].items():
                sutras.update([l['sutra_id'] for l in vals if 'sutra_id' in l])
        samjnas = {'samjna': samjna, 'val': val}
        samjnastr = "saMjYA {} - {}".format(samjnas['samjna'], ", ".join(samjnas['val']))
        if sutras:
            samjnas['sutras'] = sorted(sutras)
            samjnastr += " (sUtram - " + ", ".join(samjnas['sutras']) + ")"
        self.samjnas.append(samjnastr)
        return self