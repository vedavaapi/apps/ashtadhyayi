import locale
import sys
import json
import re
import os.path
from .utils import *
from .config import *
from pprint import pprint

class Subanta:
    vibhakti_suffixes = []
    pada_cache = {}

    @classmethod
    def load(self, rebuild=False):
        Subanta.vibhakti_suffixes = []
        Subanta.pada_cache = {}
        if not Subanta.vibhakti_suffixes:
            try:
                log("Loading vibhakti suffixes ..")
                with open(datapath("vibhakti_templates_slp1.json")) as f:
                    vibhakti_dict = json.load(f)
                    Subanta.vibhakti_suffixes = vibhakti_dict['suffixes']
            except Exception as e:
                print("Error loading vibhakti_templates_slp1.json: ", e)
                return False
        return True

    @classmethod
    def _insert(clss, pdesc, analysis):
        if pdesc['pada'] not in clss.pada_cache:
            clss.pada_cache[pdesc['pada']] = { }
        if pdesc['vibhakti'] not in clss.pada_cache[pdesc['pada']]:
            clss.pada_cache[pdesc['pada']][pdesc['vibhakti']] = { }
        clss.pada_cache[pdesc['pada']][pdesc['vibhakti']][pdesc['vachana']] = analysis
        return analysis

    @classmethod
    def _lookup(clss, pdesc):
        try:
            srch_pada = pdesc['pada_split'] if 'pada_split' in pdesc else pdesc['pada']
            return clss.pada_cache[srch_pada][pdesc['vibhakti']][pdesc['vachana']]
        except:
            return None

    @classmethod
    def analyze(clss, pada_desc):
        res = clss._lookup(pada_desc)
        if res:
            return res

        if not ('vibhakti' in pada_desc and 'vachana' in pada_desc):
            return []

        pada = sanscript.transliterate(pada_desc['pada'],
                    sanscript.DEVANAGARI, sanscript.SLP1)
        if 'pada_split' in pada_desc:
            pada = sanscript.transliterate(pada_desc['pada_split'],
                                           sanscript.DEVANAGARI, sanscript.SLP1)
        #print("pada (dvng) = ", pada_desc['pada'], " (slp1) = ", pada)
        if pada_desc['vibhakti'] == 0:
            # Avyayam
            pada_desc['vachana'] = 0
            return clss._insert(pada_desc, [{'praatipadikam' : pada, 'stem' : pada, 'anta' : ''}])
        vibhakti = pada_desc['vibhakti']
        vachana = pada_desc['vachana']

        matches = []
        max_len = -1
        #print s_id
        # Find vibhaktis with a maximal match of suffix
        for v_entry in clss.vibhakti_suffixes:
            v = v_entry["vibhaktis"]
            suffix_str = v[vibhakti-1][vachana-1]
            for suffix in suffix_str.split('/'):
                pada = re.sub(r'[mM]$', 'm', pada)
                suffix = re.sub(r'[mM]$', 'm', suffix)
                #pprint(f"{suffix} {pada}")
                regex = re.compile(suffix+'$')
                m = regex.search(pada)
                if m:
                    l = len(m.group(0))
                    if l < max_len:
                        continue
                    elif l > max_len:
                        matches = []
                    max_len = l

                    stem = pada if l <= 0 else pada[:- l]
                    praatipadikam = stem + v_entry['anta']
                    prathamaa_rupam = stem + v[0][0]
                    #pprint(f"{suffix} {pada} {m.group(0)} pratipadikam={praatipadikam}")
                    matches.append({'anta' : v_entry['anta'],
                        'linga' : v_entry['linga'], 
                        'praatipadikam' : praatipadikam, 'stem' : stem})
        if matches:
            clss._insert(pada_desc, matches)
        return matches

    @classmethod
    def praatipadikam(clss, pada_desc):
        if 'praatipadikam' in pada_desc:
            return pada_desc['praatipadikam']
        entry = clss._lookup(pada_desc)
        if not entry:
            entry = clss.analyze(pada_desc)
        if len(entry) == 0:
            return None
        praatipadikam = entry[0]['praatipadikam']
        return praatipadikam if praatipadikam else None
