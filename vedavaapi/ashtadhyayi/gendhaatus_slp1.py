#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import re

def load_dhaatus():
    expand = {
        "a0": "adAdi",
        "ju0": "juhotyAdi",
        "di0": "divAdi",
        "svA0": "svAdi",
        "tu0": "tudAdi",
        "ru0": "ruDAdi",
        "ta0": "tanAdi",
        "kryA0": "kryAdi",
        "cu0": "curAdi",
        "A0": "Atmane",
        "pa0": "parasmE",
        "u0": "uBaya"
    }
    dhatus = []
    with open(("data/dhaatu_patha_slp1.csv")) as f:
        #hdrs = ["dhaatu_mod", "gana", "iT", "padi", "dhatu", "artha"]
        i = 0
        #print(",".join(hdrs))
        for l in f.readlines():
            i = i + 1
            if l.startswith('#'):
                continue
            l = re.sub('\s\s+', ' ', l)
            l_slp1 = l.strip()
            fields = re.split('\s*,\s*', l_slp1)
            if "\s" in fields[0]:
#                print("{}: {}".format(i, fields))
                continue
            if i <= 1:
                hdrs = fields
                print(",".join(hdrs))
                continue
            fields = [(expand[x] if x in expand else x) for x in fields]
            #print("{}: {}".format(i, fields))
#            dhatu, meaning = fields[-1].split(" ", 1)
#            fields.pop()
#            fields.extend([dhatu, meaning])
            print(",".join(fields))
            d = dict(zip(hdrs, fields))
            dhatus.append(d)

    with open("data/dhaatu_patha_slp1.json", "w") as f:
        stext = json.dumps(dhatus, indent=2, ensure_ascii=False, separators=(',', ': '))
        f.write(stext + "\n")

load_dhaatus()
