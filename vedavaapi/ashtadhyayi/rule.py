#!/usr/bin/python
# -*- coding: utf-8 -*-

from .subanta import *
from .shiksha import *
from .padaseq import *

class Rule:
    def __init__(self, ashtadhyayi, rule = None):
        self.a = ashtadhyayi
        self.rule = rule

        self.IF = self._if
        self.PROHIBIT = self._prohibit
        self.PIPE = self._pipe
        self.ANY = self._any
        self.SAMJNA_COMPS = self._samjna_comps
        self.GEN_SAMJNA = self._gen_samjna
        self.GEN_SEQ = self._gen_sequence
        self.LOCATE_FIRST = self._locate_first_upadesha
        self.LOCATE_LAST = self._locate_last_upadesha
        self.ANGA = self._find_anga
        self.BV = self._padaOf

        #global IF

        # Key should be in SLP1 format
        self.predefined_funcs = {
            'tatkAla': lambda x, m: self.retvarnas(x.views['varnas']),
            'tat' : lambda x, m: None,
            'antya' : lambda x, m: self._antya(x),
            'anta' : lambda x, m: self._anta(x),
            r'(.)Adika$': lambda x, m: self.x_adika(x, m),
            r'^anAdi' : lambda x, m: self._anAdi(x),
            'Adi' : lambda x, m: self._adi(x),
            r'(t)apara': lambda x, m: self.x_para(x, m),
            r'(.+?)(d?)it$': lambda x, m: self.x_it(x, m),
            'SabdasaMjYA': lambda x, m: self.retvarnas([0] if self.a.is_samjna(x.padas(0)['pada']) else None),
            'Sabda': lambda x, m: PadaSequence(padas=[x.padas(0)['pada']]).add_label('padas.literal', positions=[0], value=True),
            'anunAsika': lambda x, m: self.retvarnas(self.a.is_anunAsika(x)),
            'viDi': lambda x, m: None,
#            'aNga': lambda x, m: x,
            'savarRa': lambda x, m: self._savarnaOf(x),
            'tulyavarRa': lambda x, m: self.retvarnas(x.views['varnas']),
            'upadeSa': lambda x, m: self.check_upadesa(x),
            'upadizwa': lambda x, m: self._upadishta(x),
            'para' : lambda x, m: self._paraOf(x),
            'pUrva' : lambda x, m: self._purvaOf(x),
            'eka': lambda x, m: self._eka(x),
            'sTAni': lambda x, m: self._sthAni(x),
            '^(praTamA|dvitIyA)vayava': lambda x, m: self._avayava(x, m),
            r'((?:BV|ad|juhoty|div|sv|tud|ruD|tan|kry|cur)Adi)': lambda x, m: self._gana(x, m),
            r'(BUvAdi)': lambda x, m: x.intersect(self.a.is_category(x, m.group(1))),
            'saMhitA': lambda x, m: x,
            # 'pada': lambda x, m: None, #x.subset(pinds=x._derive_view('padas')),
            r'^(.)[/\\\^~]?$': lambda x, m: \
                self.retvarnas([i for i in x._derive_view('varnas') if Pada.nosvaras(x.varnas(i)) in Siksha.variants(m.group(1))]),
            r'^(anudAtta|udAtta|svarita)$': lambda x, m: \
                self._check_svara(x, m.group(1)),
            r'^uBA$': lambda pseq, m: self._uBe(pseq, m),
        }

    def _avayava(self, pseq, m):
        delim_vinds = pseq.views['varnas']
        if not delim_vinds:
            return None

        pinds = pseq._derive_view('padas')
        prange = pseq.padas(pinds[0])['pos']
        delim_vinds = sorted(set(delim_vinds) & set(range(*prange)))
        vind_first = prange[0]
        vind_last = prange[1] if len(delim_vinds) <= 1 else delim_vinds[1]
        if vind_first >= vind_last:
            return None
        ret_vinds = list(range(vind_first, vind_last))
        if m.group(1) == 'praTamA':
            debuglog(f"_avayava: prathama pinds {pinds} ac_vinds {delim_vinds} vinds {ret_vinds}")
            pseq.add_label(".".join(['padas', 'avayava']), positions=[pinds[0]],
                          value={'type': 'varnas', 'val': ret_vinds })
        elif m.group(1) == 'dvitIyA':
            if len(delim_vinds) <= 1:
                return None
            vind_first = delim_vinds[0]+1
            vind_last = prange[1] if len(delim_vinds) <= 2 else delim_vinds[2]
            if vind_first >= vind_last:
                return None
            ret_vinds = list(range(vind_first, vind_last))
            debuglog(f"_avayava: dvitiya pinds {pinds} vinds {ret_vinds}")
            pseq.add_label(".".join(['padas', 'avayava']), positions=[pinds[0]],
                           value={'type': 'varnas', 'val': ret_vinds })
        else:
            return None
        return pseq.subset(pinds=[pinds[0]], vinds=ret_vinds)

    def _gana(self, pseq, m):
        srchgana = m.group(1)
        mseq = pseq.find_label('gaRa')
        if not mseq:
            return None
        mygana = mseq.labels['gaRa']['padas'][0]['val']
        debuglog(f"_gana({pseq}: mygana {mygana} srchgana {srchgana}")
        if srchgana != mygana:
            return None
        pinds = pseq.matching_pinds(mseq, mseq.views['padas'])
        return self.retpadas(pinds) if pinds else None

    def _savarnaOf(self, pseq):
        if 'varnas' not in pseq.views:
            return None
        vinds = pseq._derive_view('varnas')
        savarna_inds = [i+1 for i in vinds
                        if i < len(pseq.parts['varnas'])-1 and
                            Siksha.max_savarna(pseq.varnas(i), [pseq.varnas(i+1)], fuzzy=False)]
        if not savarna_inds:
            return None
        return self.retvarnas(savarna_inds)

    def _sthAni(self, pseq):
        pinds = pseq._derive_view('padas')
        if len(pinds) == 1 and pseq.padas(pinds[0])['pada'] == 'gaRa':
            newpseq = PadaSequence(padas=['gaRa'])
            return newpseq
        return pseq

    # Return the state of padaseq after for itaH lopaH transform.
    def _upadishta(self, pseq):
        if not pseq.prev_transforms or len(pseq.prev_transforms) == 1:
            return pseq
        if len(pseq.prev_transforms) > 1:
            oldseq = PadaSequence(pseq.prev_transforms[1]['pseq'])
            return oldseq

    def _check_svara(self, x, svara):
        vinds = [i for i in x._derive_view('varnas')
                   if Siksha.v[svara] in x.varnas(i)]
        if not vinds:
            return None
        return self.retvarnas(vinds)

    def _eka(self, pseq):
        vinds = pseq.views['varnas'] if 'varnas' in pseq.views else None
        if not vinds:
            return None

        new_vinds = []
        for i,p in enumerate(pseq.parts['padas']):
            v_in_pada = set(vinds) & set(range(*p['pos']))
            if len(v_in_pada) == 1:
                new_vinds.extend(v_in_pada)
        return pseq.subset(vinds=new_vinds) if new_vinds else None

    def check_upadesa(self, pseq):
        res = self.a.is_upadesha(pseq)
        if not res:
            return None

        pinds = []
        for i in res.views['padas']:
            p = res.padas(i)
            if p['pada'] != "".join([res.varnas(j) for j in range(*p['pos'])]):
                pinds.append(i)
        if not pinds:
            return pseq.intersect(res)

        oldseqs = [PadaSequence(t['pseq'])
                for t in reversed(pseq.prev_transforms) if 'upadeSa' in t['pseq']['labels']]
        if oldseqs:
            return None
        return res

    def retvarnas(self, pos):
        return { 'type': 'varnas', 'pos': pos, 'val': None } if pos else None

    def retpadas(self, pos):
        return { 'type': 'padas', 'pos': pos, 'val': None } if pos else None

    # taparaH ityaadi
    def x_para(self, pseq, m):
        match = []
        for i in self._antya(pseq)['pos']:
#            print(i)
#            print(ppformat(pseq))
#            print(pseq.parts['varnas'][i])
#            print(m.group(1))
            if m.group(1) == pseq.parts['varnas'][i]:
                if i-1 in [pseq.padas(j)['pos'][0] for j in pseq._derive_view('padas')]:
                    match.append(i-1)
#        self.debuglog("{}-para = {}".format(m.group(1), match))
        return self.retvarnas(match)

    # Return padas in pseq whose 'it' varnas match those in m.group(1)
    def x_it(self, pseq, m):
        matchseqs = pseq.lookup_label('it')
        if matchseqs:
            for mseq in matchseqs:
                it_vinds = {}
                for linfo in mseq.labels['it']['varnas']:
                    for i in linfo['pos']:
                        if m.group(1) in set(('a', 'i', 'u')) and not Pada.nosvaras(mseq.varnas(i)).endswith('~'):
                            continue
                        varna = re.sub(r'[\\\^~]', '', mseq.varnas(i))
                        if varna not in m.group(1):
                            continue
                        if varna not in it_vinds:
                            it_vinds[varna] = set()
                        it_vinds[varna].add(i)

                for v,locations in it_vinds.items():
                    pinds = [i for i, p in enumerate(mseq.parts['padas'])
                             if locations & set(range(*p['pos']))]
                    pinds = pseq.matching_pinds(mseq, pinds)
                    if not pinds:
                        continue
                    pseq.add_label(f"padas.{v + 'it'}", positions=pinds)

        pinds = set()
        for v in list(m.group(1)):
            chksamjna = v + 'it'
            if self.a.is_samjna(chksamjna):
                res = self.a.label_samjna(pseq, chksamjna)
            else:
                res = pseq.find_label(chksamjna)
            if res:
                pinds |= set(res.views['padas'])

        debuglog("{}-it = padas {}".format(m.group(1), pinds))

        if not pinds:
            return None

        retseq = pseq.subset(pinds=sorted(list(pinds)))
        if 'varnas' in pseq.views:
            retseq = pseq.intersect(retseq)
            if retseq.views['varnas']:
                return self.retvarnas(retseq.views['varnas'])

        return self.retpadas(retseq.views['padas'])

    # Locate the end varna positions of all padas in pseq
    def _antya(self, pseq):
        if 'padas' in pseq.views:
            pos = [p['pos'][1]-1 for p in pseq.padas()]
        else:
            pos = pseq.views['varnas'][-1:]
        self.debuglog(f"_antya of {pseq.views} is {pos}")
        return self.retvarnas(pos)

    # Locate last occurence of given varna in each pada of pseq
    def _anta(self, pseq):
        #self.debuglog(ppformat(pseq), level=0)
        if 'varnas' in pseq.views:
            # vlist indicates positions where given varna occurs
            vlist = pseq.views['varnas']
            result = []
            for p in pseq.padas():
                if p['pos'][0] == p['pos'][1]:
                    continue
                #antya_vind = sorted(set(range(*p['pos'])))[-1]
                # Look up the location of last occurence of given varna
                antya_vind = sorted(set(vlist) & set(range(*p['pos'])))[-1]
                if antya_vind in vlist:
                    result.append(antya_vind)
            self.debuglog("_anta(varnas): {} - {}".format(pseq, result))
            return self.retvarnas(result) if result else None
        elif 'padas' in pseq.views:
            pinds = [i-1 for i in pseq.views['padas'] if i > 0]
            vinds = set()
            for pind in [pinds[0], pinds[0]+1]:
                vinds |= set(list(range(*pseq.padas(pind)['pos'])))
            res = {'type': 'padas', 'pos': [pinds[0]+1],
                   'val': {'type': 'varnas', 'val': vinds}} if pinds else None
            self.debuglog("_anta(padas): {} - {}".format(pseq, res))
            return res

    def _paraOf(self, pseq):
        if 'padas' in pseq.parts:
            pos = [i+1 for i in range(len(pseq.parts['padas'])-1)]
            return pseq.subset(pinds=pos) if pos else None
        return None

    def _purvaOf(self, pseq):
        if 'varnas' in pseq.views:
            pos = [i-1 for i in pseq.views['varnas'] if i >= 1]
            return pseq.subset(vinds=pos) if pos else None
        return None

    def _anAdi(self, pseq):
        adi_vinds = [p['pos'][0] for p in pseq.padas()]
        if 'varnas' in pseq.views:
            vinds = [i for i in pseq.views['varnas'] if i not in adi_vinds]
            return self.retvarnas(vinds)
        return None

    def _adi(self, pseq):
        if 'varnas' in pseq.views:
            vinds = [[i for i in pseq.views['varnas']][0]]
        else:
            vinds = [p['pos'][0] for p in pseq.padas()]
        return self.retvarnas(vinds)

    def _padaOf(self, pseq):
        pinds = pseq._derive_view('padas')
        return pseq.subset(pinds=pinds)

    def x_adika(self, pseq, m):
        pinds = [i for i in pseq._derive_view('padas')
                  if pseq.varnas(pseq.padas(i)['pos'][0]) == m.group(1)]
        return self.retpadas(pinds)

    def _uBe(self, pseq, m):
        debuglog(f"{ppformat(pseq)}")
        pinds = pseq._derive_view('padas')
        vinds = pseq.views['varnas']
        debuglog(f"_uBe: pinds={pinds}, vinds={vinds}")
        pada_vinds = list(range(*pseq.padas(pinds[0])['pos']))
        last_vind = vinds[-1]+len(vinds)
        debuglog(f"_uBe: pada_vinds={pada_vinds} last_vind={last_vind}")
        if last_vind not in pada_vinds:
            return None
        ret_vinds = list(range(vinds[0], last_vind+1))
        debuglog(f"_uBe: ret_vinds={ret_vinds}")

        return pseq.subset(pinds=pinds, vinds=ret_vinds)

    def debuglog(self, *msg, level=2):
        self.a.debuglog(*msg, level=level)

    @logged_func
    def _apply_func(self, pseq, func_desc):
        self.debuglog("apply_func ", ppformat(func_desc))
        if not func_desc:
            return pseq
        if 'vseqs' in pseq.views and pseq.views['vseqs']:
            for vinds in pseq.views['vseqs']:
                rseq = pseq.subset(vinds=vinds)
                del rseq.views['vseqs']
                r = self._apply_func(rseq, func_desc)
                if r:
                    return r
            return None
        if isinstance(func_desc, list):
            f = self._apply_func(pseq, func_desc[0])
            return f(pseq, func_desc[1:]) if f else None
        elif isinstance(func_desc, dict):
            return self._interpret_pada(pseq, func_desc)
        elif isinstance(func_desc, str):
            self.debuglog("apply_func: ", func_desc)
            f = eval("self."+func_desc)
            return f
        else:
            return None

    def find_literal_match(self, pseq, f_pada):
        # Search for literal match in padas
        self.debuglog("literal pada: {}".format(f_pada))
        pinds = [i for i in pseq._derive_view('padas') \
                 if Pada.nosvaras(pseq.padas(i)['pada']) == f_pada]
        if pinds:
            pseq.add_label("{}.{}".format('padas', f_pada), positions=pinds)
            return pseq.find_label(f_pada)

        # Search for literal match in varnas
        vinds = pseq._derive_view('varnas')
        if not vinds:
            return None

        pada = "".join(pseq.parts['varnas'][vinds[0]:])
        pada = Pada.nosvaras(pada)
        self.debuglog("literal varna: {} in {}".format(f_pada, pada))
        f_vlist = Pada.split(f_pada.strip(), '\\\^~')
        i = pada.find(f_pada)
        if i < 0 or i > 0:
            return None
        plist = Pada.split(pada[:i], "\\\^~")
        vinds = [vinds[0]+j for j in range(i, i+len(f_vlist))]
        self.debuglog("vlist = {}, vinds={}".format(f_vlist, vinds))
        pseq.add_label("{}.{}".format('varnas', f_pada), positions=vinds)
        newpseq = pseq.subset(vinds=vinds)
        return newpseq.find_label(f_pada)

    # f_pada input must be in praatipadika form
    def _interpret_singlepada(self, pseq: PadaSequence, f_desc, c=None):
        f_pada = c if c else Subanta.praatipadikam(f_desc)
        if 'tags' in f_desc and f_pada in f_desc['tags']:
            ptag = f_desc['tags'][f_pada]
            f_pada = ptag['val']
        else:
            ptag = f_desc

        if 'reject' in ptag:
            debuglog(f"_interpret_singlepada: forcing fail {f_pada}")
            return None
        if 'skip' in ptag:
            debuglog(f"_interpret_singlepada: Ignoring {f_pada}")
            return pseq
        if 'literal' in ptag and ptag['literal']:
            return self.find_literal_match(pseq, f_pada)

        if 'tags' in f_desc and f_pada in f_desc['tags']:
            debuglog(f"_interpret_pada (tag): f_desc = {f_desc}")
            new_fdesc = copy.deepcopy(f_desc)
            del new_fdesc['tags']
            new_fdesc['pada'] = f_pada
            new_fdesc['pada_split'] = f_pada
            new_fdesc['praatipadikam'] = f_pada
            debuglog(f"_interpret_pada (tag): new_fdesc = {new_fdesc}")
            if c:
                new_fdesc['vibhakti'] = 0
            return self._interpret_pada(pseq, new_fdesc)

        self.debuglog("Interpreting single pada ", ppformat(f_pada))

        if f_pada == 'tat':
            return pseq
        if f_pada == 'viDi':
            return None
        for f in self.predefined_funcs:
            m = re.match(f, f_pada)
            if m:
                self.debuglog("matched {} against predef {}".format(pseq, f))
                res = self.predefined_funcs[f](pseq, m)
                self.debuglog("{} returned {}".format(f, res), level=2)
                if not res:
                    return None
                if isinstance(res, dict):
                    if f_pada not in pseq.labels:
                        pseq.add_label("{}.{}".format(res['type'], f_pada),
                                       positions=res['pos'], value=res['val'])
                elif isinstance(res, PadaSequence):
                    self.debuglog(f"_interpret_singlepada predef {ppformat(res.views)}")
                    return res
                break

        # f_pada already evaluated on pada_desc before?
        labelval = pseq.find_label(f_pada)
        if labelval:
            if f_pada not in pseq.labels:
                debuglog(f"_interpret_singlepada: found {f_pada} not in ")
                res2 = self.a.label_samjna(pseq, f_pada)
                if res2:
                    debuglog("_interpret_singlepada: label_samjna {} returned ".format(f_pada), ppformat(res2))
                    return res2
            if f_pada in ['aNga', 'dvA']:
                return labelval.add_samjna(f_pada)
            res = labelval.intersect(pseq)
            if res:
                res = res.add_samjna(f_pada)
            return res

        if f_pada in r().get_skip_words():
            self.debuglog("interpret_pada: Skipping ", f_pada)
            return None
        # If f_pada is encountered first time, evaluate and remember it
        if not self.a.eval_samjna(f_pada):
            return None
        res = self.a.label_samjna(pseq, f_pada)
        if res:
            self.debuglog("label_samjna {} returned ".format(f_pada), res)
            return res.add_samjna(f_pada)
        return None

    # All samasta-padas other than bahupada dvandvam
    def _interpret_conjunct(self, pseq: PadaSequence, f_desc, fpada):
        debuglog(f"_interpret_conjunct: {fpada}")
        if fpada.find('&') < 0:
            return self._interpret_singlepada(pseq, f_desc, fpada)

        comps = fpada.split('&')
        pre_comps = []
        new_comps = []
        for c in comps:
            c1, count = re.subn(r'^\(.*?\)', '', c)
            if count:
                pre_comps.append(c1)
                debuglog(f"_interpret_conjunct: comp {c} -> {c1} {pre_comps}")
            else:
                new_comps.append(c)
        new_comps = pre_comps + new_comps
        debuglog(f"_interpret_conjunct: {comps} -> {new_comps}")

        r = pseq
        negate = False
        for c in new_comps:
            if c == 'a' or c == 'an':
                negate = True
                continue
            m = re.match(r'<(.*)>', c)
            if m:
                adapt_func = eval("self." + m.group(1))
                r = adapt_func(r)
            else:
                r = self._interpret_singlepada(r, f_desc, c)
            if not r:
                break

        if negate:
            debuglog("conjunct: negating {}".format(c))
            if r:
                return None
            return pseq
        else:
            return r if r else None

    # Return output whose format is same as returned by find_label()
    # Process a single or compound sutra pada
    def _interpret_pada(self, pseq: PadaSequence, f_desc):
        fpada = Subanta.praatipadikam(f_desc)
        if not fpada:
            return None
        self.debuglog("_interpret_pada({}): {}".format(fpada, pseq))
        if fpada.find('-') < 0:
            r = self._interpret_conjunct(pseq, f_desc, fpada)
            self.debuglog("interpret_pada {}({}) = {}".format(fpada, pseq, r))
            return r if r else None

        pinds = None
        vinds = None
        matched = False
        negate = False
        r = None
        results = []
        # Do a UNION/OR of all the components separated by '-'
        for c in fpada.split('-'):
            if c == 'a' or c == 'an':
                negate = True
                continue
            if c == 'anta' or c == 'Adi':
                r, prevc = results[-1]
                if not r:
                    continue
                pinds = None
                vinds = None
                matched = False
                r = self._interpret_singlepada(r, f_desc, c)
            else:
                r = self._interpret_conjunct(pseq, f_desc, c)

            self.debuglog("interpret_pada {} = {}".format(c, r))
            results.append((r, c))

            if negate:
                self.debuglog(f"interpret_pada: negating {c} {r}")
                if r:
                    if 'padas' in r.views:
                        pinds_residue = set(pseq.views['padas']) - set(r.views['padas'])
                        if not pinds_residue:
                            return None
                        pinds = pinds_residue
                        self.debuglog(f"{pinds} {pseq.views['padas']} {r.views['padas']}")
                        r = pseq.subset(pinds=pinds_residue)
                        self.debuglog(f"interpret_pada: residue {c} {r}".format(c, r))
                    else:
                        return None
                matched = True
            else:
                if r:
                    debuglog(f"r = {ppformat(r)}")
                    if c in r.labels:
                        debuglog(f"{r.labels[c]}")
                        if 'varnas' in r.labels[c]:
                            inds = reduce(lambda x, y: x + y, [m['pos'] for m in r.labels[c]['varnas']])
                            inds = sorted(set(inds) & set(r.views['varnas']))
                            if not vinds:
                                vinds = []
                            vinds.extend(inds)
                        if 'padas' in r.labels[c]:
                            inds = reduce(lambda x, y: x + y, [m['pos'] for m in r.labels[c]['padas']])
                            if 'padas' in r.views:
                                inds = sorted(set(inds) & set(r.views['padas']))
                            if not pinds:
                                pinds = []
                            pinds.extend(inds)
                    else:
                        if 'padas' in r.views:
                            if not pinds:
                                pinds = []
                            pinds.extend(r.views['padas'])
                        if 'varnas' in r.views:
                            if not vinds:
                                vinds = []
                            vinds.extend(r.views['varnas'])
                    matched = True

        r = pseq
        if pinds or vinds:
            if vinds and 'padas' in f_desc:
                pinds = [i for i, p in enumerate(r.parts['padas']) \
                         if set(vinds) & set(range(*p['pos']))]
                vinds = None
                debuglog(f"_interpret_pada: returning pind {pinds} for vinds {vinds}")
            self.debuglog(f"pinds={pinds} vinds={vinds}")
            r = pseq.subset(pinds=pinds, vinds=vinds)
        return r if matched else None

    # Return pada_desc if cond applies to it, None otherwise
    def _if(self, pseq, rule):
        cond = rule[0]
        if isinstance(cond, list):
            res = functools.reduce((lambda x, y: x and y),
                (self._interpret_pada(pseq, c) for c in cond))
        else:
            res = self._interpret_pada(pseq, cond)

        self.debuglog("_if = ", res)
        return self._apply_func(pseq, rule[1:]) if res else None

    def _prohibit(self, pseq, rule):
        samjna = rule[0]
        cond = rule[1]
#        if isinstance(cond, list):
#            res = functools.reduce((lambda x, y: x and y),
#                                   (self._interpret_pada(pseq, c) for c in cond))
#        else:
#            res = self._interpret_pada(pseq, cond)

        res = self._apply_func(pseq, cond)
        if not res:
            return None

        # If samjna is preexisting in pseq
        #   remove matching occurences from existing samjna positions
        matchseqs = pseq.lookup_label(samjna)
        if matchseqs:
            for mseq in matchseqs:
                vinds = []
                linfo = {}
                for part, positions in mseq.labels[samjna].items():
                    if part not in res.views:
                        continue
                    remove_inds = res.views[part]
                    newpos = []
                    print(f"_prohibit: {positions}")
                    for p in positions:
                        residue = set(p['pos']) - set(remove_inds)
                        if not residue:
                            continue
                        newp = copy.deepcopy(p)
                        newp['pos'] = sorted(list(residue))
                        newpos.append(newp)
                    if newpos:
                        linfo[part] = newpos
                if linfo:
                    mseq.labels[samjna] = linfo
                else:
                    del mseq.labels[samjna]
            debuglog(f"_prohibit: removed {samjna} {pformat(pseq.labels)}")
#            return pseq.find_label(samjna)
        else:
            debuglog(f"_prohibit: empty {samjna}")
        return pseq.subset(pinds=[])

    def _find_anga(self, pseq: PadaSequence, pratyaya_desc):
        res = self._interpret_pada(pseq, pratyaya_desc[0])
        if not res:
            return res
        pratyaya_pinds = res.views['padas']
#        adi_pind = pratyaya_pinds[0]-1
        adi_pind = 0

        # if pada is akArAnta, and got modified
        p = res.padas(adi_pind)
        adi_pvarnas = [res.varnas(i) for i in range(*p['pos'])]
        adi_pada = p['pada']
        if adi_pada[-1] in ['a', 'i', 'u'] and len(adi_pada) > 1:
            categories = self.a.upadesha_category(adi_pada)
            if 'BUvAdi' in categories:
                if adi_pada != "".join(adi_pvarnas):
                    res.add_label(".".join(['padas', 'aNga']), positions=[pratyaya_pinds[0]],
                                  value={'type': 'padas', 'val': [adi_pind]})
                    debuglog(f"_find_anga: skipping {categories} {adi_pada} {adi_pvarnas}")
                    pratyaya_pinds = pratyaya_pinds[1:]

        pratyaya_vidhi_i = res.padas(adi_pind)['pos'][0]
        pinds = []
        debuglog(f"_find_anga: {pratyaya_vidhi_i} res = {ppformat(res)}")
        for i in pratyaya_pinds:
            pratyaya_i = res.padas(i)['pos'][0]
            pos = list(range(pratyaya_vidhi_i, pratyaya_i))
            res.add_label(".".join(['padas', 'aNga']), positions=[i],
                          value={'type': 'varnas', 'val': pos })
            pinds.append(i)
        debuglog(f"_find_anga: {res.labels['aNga']}")
        return res.subset(pinds=pinds)

    # pseq should contain only one pada
    def _gen_sequence(self, pseq: PadaSequence, seq_desc):
        if len(pseq.varnas()) > 3:
            return None
        self.debuglog("start gen_sequence")
        first = self._apply_func(pseq, seq_desc[0])
        self.debuglog("gen_sequence: first = ", ppformat(first))
        if not first:
            return None
        elif isinstance(first, list):
            first = first[0]
        last = self._apply_func(pseq, seq_desc[1])
        self.debuglog("gen_sequence: last = ", ppformat(first))
        if not last:
            return None
        elif isinstance(last, list):
            last = last[0]
        self.debuglog("genseq: {},{}".format(first, last))
        seq = self.a.gen_sequence(first, last)
        self.debuglog("gen_sequence({}, {}) = {}".format(first, last, seq))
        if not seq:
            return None

        pada = pseq.padas()[0]['pada']
        pseq.add_label(".".join(['padas', pada]), positions=[0],
                       value= { 'type' : 'varnas', 'val' : seq})
        return pseq

    def _locate_first_upadesha(self, pseq, rules):
        return self._locate_upadesha(pseq, rules, False)

    def _locate_last_upadesha(self, pseq, rules):
        return self._locate_upadesha(pseq, rules, True)

    # Apply rules on a pada sequence pseq
    # pseq should contain only one pada
    def _locate_upadesha(self, pseq: PadaSequence, rules, reverse=False):
        res = self._apply_func(pseq, rules[0])
        if not res:
            return None

        varnas = [c for c in res.varnas()]
        self.debuglog("locate_upadesha", varnas)
        matched_upadeshas = self.a.locate_in_upadesha(varnas[0])
        if not matched_upadeshas:
            return None

        mymatches = matched_upadeshas
        if reverse:
            mymatches = reversed(mymatches)
        next_matches = []
        for m in mymatches:
            u_desc = self.a.get_upadesha(m)
            #self.a.upadesha[m['key']][m['idx']].subset(vinds=m['pos'])
            self.debuglog("Checking match of upadesha ...", u_desc)
            matched = True
            for r in rules[1:]:
                if not self._apply_func(u_desc, r):
                    matched = False
                    break
            if matched:
                next_matches.append(m)
                break
        if not next_matches:
            return None
        matched_upadeshas = next_matches
        self.debuglog("locate result: ", matched_upadeshas)
        return matched_upadeshas

    # Check given pada against a generated samjna rule
    def _gen_samjna(self, pseq, func_descs):
        self.debuglog(f"In gen_samjna {pseq}")
        fdesc = func_descs[0]
        self.debuglog(ppformat(fdesc['samjni']), level=2)
        res = self._apply_func(pseq, fdesc['samjna'])
        self.debuglog("gen_samjna returned samjna = ", res)
        if res and fdesc['samjni']:
            res = self._apply_func(res, fdesc['samjni'])
            if res:
                pada = pseq.padas(0)['pada']
                if 'literal' in res.labels:
                    litval = res.labels['literal']['padas'][0]['val']
                    res_pada = res.padas(0)['pada']
                    m = re.fullmatch(r'(.)a', res_pada)
                    if m:
                        pseq.add_label(".".join(['varnas', res_pada]), positions=[0],
                                       value={'type': 'varnas',
                                              'val': [m.group(1)]})
                    else:
                        pseq.add_label(".".join(['padas', pada]), positions=[0],
                                   value = { 'type': 'padas',
                                             'literal': litval,
                                             'val': res_pada})
                else:
                    vlist = [v for v in res.varnas()]
                    pseq.add_label(".".join(['padas', pada]), positions=[0],
                                   value= { 'type' : 'varnas',
                                            'val' : vlist})
            return pseq
        return res

    def _pipe(self, pseq, func_descs):
        res = pseq
        for f in func_descs:
            res = self._apply_func(res, f)
#            self.debuglog("pipe component result", res, level=2)
            if not res:
                return None
#        self.debuglog("Pipe result: ", res, level=2)
        return res

    def _any(self, pseq, func_descs):
        for f in func_descs:
            res = self._apply_func(pseq, f)
            debuglog("any result", res, level=2)
            if res:
                return res
        return pseq

    # A conjunct of Samjna components
    def _samjna_comps(self, samjna_pseq, samjna_conds):
        res = samjna_pseq
        for c in samjna_conds:
            res = self._apply_func(res, c)
            self.debuglog(f"samjna component {c} result {res}")
            if not res:
                return None
        self.debuglog("Samjna_comps result: ", res)
        return res

    # Split given property list into those matching or not matching the cond. 
    def bifurcate(self, pseq: PadaSequence, cond, split_p6 = False):
        matches = []
        rest = []
        # Attach Shashthi vibhakti padas to nearest non-shashthi pada list
        v6_padas = []
        last_match = None
        for p in pseq:
            if self.a._propmatch(p, cond):
                if v6_padas:
                    matches[0:0] = v6_padas
                    v6_padas = []
                matches.append(p)
                last_match = matches
            else:
                if split_p6 and self.a._propmatch(p, {'vibhakti': 6}):
                    v6_padas.append(p)
                    continue
                if v6_padas:
                    rest[0:0] = v6_padas
                    v6_padas = []
                rest.append(p)
                last_match = rest
        if v6_padas and last_match:
            last_match[0:0] = v6_padas
        return (matches, rest)

    def _compile_samjna(self, samjna, plist):
        func_descs = []
        yasmaat, rest = self.bifurcate(plist, {'pada': 'yasmAt'})
        if yasmaat:
            pratyaya, rest = self.bifurcate(rest, {'praatipadikam': 'pratyaya'})
            if pratyaya:
                return ["ANGA"] + pratyaya
            return func_descs

        p7, rest = self.bifurcate(plist, {'vibhakti' : 7})
        if p7:
            func_descs.extend(["IF", p7])

        # Handle generated Samjna names
        match, rest = self.bifurcate(rest, {'praatipadikam' : 'rUpa'}, False)
        if match:
            #print "Entered sva"
            match, rest = self.bifurcate(rest, {'praatipadikam' : 'sva'}, False)
            if match:
                #print "Entered rUpa"
                p6, rest = self.bifurcate(rest, {'vibhakti' : 6})
                samjna = self._compile_samjna(samjna, rest) if rest else None
                if p6:
                    func_descs.extend(["GEN_SAMJNA",
                        { 'samjni' : ["PIPE"] + p6, 'samjna' : samjna }])
                else:
                    func_descs.extend(["GEN_SAMJNA",
                        { 'samjni' : None, 'samjna' : samjna }])

                return func_descs

    #        p6, rest = self.bifurcate(rest, {'vibhakti' : 6})
    #        if p6:
    #            func_descs.append(["PIPE"] + p6)

        match, rest = self.bifurcate(rest, {'pada' : 'na', 'vibhakti' : 0})
        if match:
            if rest:
                subrule = self._compile_samjna(samjna, rest)
                func_descs.extend(["PROHIBIT", samjna, subrule])
                return func_descs

        p1, rest = self.bifurcate(rest, {'vibhakti' : 1})
        match, rest = self.bifurcate(rest, {'pada' : 'saha', 'vibhakti' : 0})
        if match:
            p3, rest = self.bifurcate(rest, {'vibhakti' : 3})
            return ["GEN_SEQ", ["LOCATE_FIRST"] + p1, ["LOCATE_LAST"] + p3]

        if rest:
            p6, rest = self.bifurcate(rest, {'vibhakti': 6})
            if p6:
                p1 = p6 + p1
                #func_descs.extend(["IF", p6])
            else:
                # Handles alo.antyAt pUrva
                p5, rest = self.bifurcate(rest, {'vibhakti': 5})
                if p5:
                    purva_para, rest = self.bifurcate(p1, {'praatipadikam': ['pUrva', 'para']})
                    if not purva_para:
                        purva_para = [ {'pada': 'paraH', 'type': 'subanta', 'vachana': 1, 'vibhakti': 1, 'praatipadikam': 'para'}]
                    p1 = p5 + purva_para + rest

        func_descs.extend(["SAMJNA_COMPS"] + p1)
        return func_descs

    def compile_samjna(self, samjna, plist):
        plist = [self.a.pada_override(fdesc) for fdesc in plist]
        plist = sorted(plist, key=lambda x: int(x['seq']) if 'seq' in x else 0)
        self.rule = self._compile_samjna(samjna, plist)
        return self.rule

    def __repr__(self):
        return self.rule

    def apply(self, pseq):
        res = self._apply_func(pseq, self.rule)
        return res

class Vidhi_rule(Rule):
    def __init__(self, ashtadhyayi, sutra_id, rule = None):
        super(Vidhi_rule, self).__init__(ashtadhyayi, rule)
        self.sutra_id = sutra_id
        self.MODIFY = self._modify
        self.TAG = self._tag

        # Key should be in SLP1 format
        self.predefined_vidhi_funcs = {
            r'^(luk|Slu|lup)$' : lambda pseq, mods, m: self._lukSlulup(pseq, mods, m),
            'lopa' : lambda pseq, mods, m: self._lopa(pseq, mods, m),
            #'adarSana' : lambda pseq, loc, ctx, m: self._antya(x),
            'pratyaya': lambda pseq, mods, m: (pseq, -1), # Ignore
            r'para$': lambda pseq, mods, m: (pseq, -1), # Ignore
            r'(.*)savarRa': lambda pseq, mods, m: self._savarna(pseq, mods, m),
            r'dIrGa': lambda pseq, mods, m: self._dIrGa(pseq, mods, m),
            r'guRa': lambda pseq, mods, m: self._guRa(pseq, mods, m),
            r'dvA': lambda pseq, mods, m: self._dvA(pseq, mods, m),
        }

    def _dvA(self, pseq, mods, m):
        debuglog(f"_dvA: mods {pformat(mods)}")
        vinds = mods['purva']['pos']['inds']
        newvarnas = [pseq.varnas(i) for i in vinds]
        pseq.upsertvarnas(mods['inds'], newvarnas)
        prevseq = PadaSequence(pseq.prev_transforms[-1]['pseq'])
        prevseq.add_label("padas.dvA", positions=mods['purva']['result']['padas'],
                       value={'type': 'varnas', 'val': vinds})
        return (pseq, 1)

    def _dIrGa(self, pseq, mods, m):
        debuglog(f"_dIrGa: {pseq} {ppformat(mods)}")
        modified = False
        for i in mods['inds']:
            if mods['part'] == 'varnas':
                purva_varna = Pada.nosvaras(pseq.varnas(i))
                newvarnas = [Siksha.dIrGa(purva_varna)]
                debuglog(f"purva={purva_varna} dIrGa={newvarnas}")
                vinds = [i]
                modified = True
                if mods['replace'] == 2:
                    vinds.append(i+1)
                elif mods['replace'] == 1 and purva_varna == newvarnas[0]:
                    modified = False
                if modified:
                    pseq.upsertvarnas(vinds, newvarnas, mods['replace'])
        return (pseq, 1) if modified else (None, 0)

    def _guRa(self, pseq, mods, m):
        debuglog(f"_guRa: {pseq} {ppformat(mods)}")
        modified = False
        for i in mods['inds']:
            if mods['part'] == 'varnas':
                target_varna = Pada.nosvaras(pseq.varnas(i))
                if mods['replace'] == 2:
                    # if purvaparayoh, inds has purva_varna, need para_varna to look up the map below
                    target_varna = Pada.nosvaras(pseq.varnas(i+1))

                guRa = { 'i': 'e', 'I': 'e',
                         'u': 'o', 'U': 'o',
                         'f': 'ar', 'F': 'ar',
                         'x': 'al', 'X': 'al' }
                newvarnas = [repl for c,repl in guRa.items() if c == target_varna]
                debuglog(f"para={target_varna} guRa={newvarnas}")
                vinds = [i]
                if mods['replace'] == 2:
                    vinds.append(i+1)
                    pseq.upsertvarnas(vinds, newvarnas, mods['replace'])
                    modified = True
                elif mods['replace'] == 1 and target_varna != newvarnas[0]:
                    pseq.upsertvarnas(vinds, newvarnas, mods['replace'])
                    modified = True
        return (pseq, 1) if modified else (None, 0)

    def _savarna(self, pseq, mods, m):
        debuglog(f"_savarna: {pseq} {ppformat(mods)}")
        modified = False
        for i in mods['inds']:
            if mods['part'] == 'varnas':
                purva_varna = pseq.varnas(i)
                para_varna = pseq.varnas(i+1)
                debuglog(f"purva={purva_varna} para={para_varna}")
                newvarnas = Siksha.savarRa(purva_varna, para_varna)
                pseq.upsertvarnas([i], newvarnas, mods['replace'])
                modified = True
        return (pseq, 1) if modified else (None, 0)

    def _lopa(self, pseq, mods, m):
        lopa_positions = mods['inds']
        debuglog(f"_lopa{mods['part']}: {lopa_positions}")
        if mods['part'] == 'padas':
            pseq.delpadas(mods['inds'])
        else:
            pseq.delvarnas(lopa_positions)
        #vinds = [i for i in pseq.views['varnas'] if i not in lopa_positions]
        #self.debuglog("_lopa: vinds = ", vinds, level=2)
        #return pseq.subset(vinds=vinds)
        return pseq, 1

    def _lukSlulup(self, pseq, mods, m):
        lopa_positions = mods['inds']
        debuglog(f"{m.group(1)} {mods['part']}: {lopa_positions}")
        if mods['part'] == 'padas':
            pseq.parts['padas'][lopa_positions[0]]['pada'] = m.group(1)
            vinds_deleted = list(range(*pseq.parts['padas'][lopa_positions[0]]['pos']))
            pseq.delvarnas(vinds_deleted)
            debuglog(f"After {m.group(1)} {pformat(pseq)}")
        else:
            pseq.delvarnas(lopa_positions)
        #vinds = [i for i in pseq.views['varnas'] if i not in lopa_positions]
        #self.debuglog("_lopa: vinds = ", vinds, level=2)
        #return pseq.subset(vinds=vinds)
        return pseq, 1

    def _pararUpa(self, pseq, mods, m):
        lopa_positions = mods['inds']
        self.debuglog("_lopa: lopa_positions = ", lopa_positions, level=0)
        pseq.delvarnas(lopa_positions)
        #vinds = [i for i in pseq.views['varnas'] if i not in lopa_positions]
        #self.debuglog("_lopa: vinds = ", vinds, level=2)
        #return pseq.subset(vinds=vinds)
        return pseq, 1

    def _para(self, pseq, func_descs):
        if 'padas' in pseq.views:
            matched_pos = []
            for pos in pseq.views['padas']:
                if pos >= len(pseq.parts['padas'])-1:
                    continue
                res = pseq.subset(pinds=[pos + 1])
                for f in func_descs:
                    res = super(Vidhi_rule, self)._apply_func(res, f)
                    if not res:
                        break
                if res:
                    matched_pos.append(pos+1)
            if matched_pos:
                res = pseq.subset(pinds=matched_pos)
                res.add_label("padas.para", matched_pos)
                self.debuglog(f"_para: padas {ppformat(res)}")
                return res

        if 'varnas' in pseq.views:
            matched_pos = []
            for pos in pseq.views['varnas']:
                if pos >= len(pseq.parts['varnas'])-1:
                    continue
                res = pseq.subset(vinds=[pos+1])
                for f in func_descs:
                    res = super(Vidhi_rule, self)._apply_func(res, f)
                    if not res:
                        break
                if res:
                    matched_pos.append(pos+1)
            if matched_pos:
                res = pseq.subset(vinds=matched_pos)
                res.add_label("varnas.para", matched_pos)
                self.debuglog(f"_para: varnas {ppformat(res)}")
                return res
        return None

    def _purva(self, pseq, func_descs):
        if 'padas' in pseq.views:
            matched_pos = []
            for pos in pseq.views['padas']:
                if pos == 0:
                    continue
                res = pseq.subset(pinds=[pos - 1])
                for f in func_descs:
                    res = super(Vidhi_rule, self)._apply_func(res, f)
                    if not res:
                        break
                if res:
                    matched_pos.append(pos-1)
            if matched_pos:
                res = pseq.subset(vinds=matched_pos)
                res.add_label("padas.purva", matched_pos)
                return res

        if 'varnas' in pseq.views:
            matched_pos = []
            for pos in pseq.views['varnas']:
                if pos == 0:
                    continue
                res = pseq.subset(vinds=[pos-1])
                for f in func_descs:
                    res = super(Vidhi_rule, self)._apply_func(res, f)
                    if not res:
                        break
                if res:
                    matched_pos.append(pos-1)
            if matched_pos:
                res = pseq.subset(vinds=matched_pos)
                res.add_label("varnas.purva", matched_pos)
                return res
        return None

    def _conjunct(self, func_descs):
        for f in func_descs:
            res = super(Vidhi_rule, self)._apply_func(res, f)
            if not res:
                return res

    def _locate(self, pseq, loc, func_descs):
        if not func_descs:
            return pseq
        alt_lists = {}
        for f in func_descs:
            altval = int(f['alt']) if 'alt' in f else 0
            if altval not in alt_lists:
                alt_lists[altval] = []
            alt_lists[altval].append(f)

        new_descs = ["PIPE"] + alt_lists[0]
        alt_descs = [["PIPE"] + alt_lists[v] for v in sorted(alt_lists.keys()) if v != 0]
        if alt_descs:
            new_descs.append(["ANY"] + alt_descs)

        res = super(Vidhi_rule, self)._apply_func(pseq, new_descs)

        fnames = []
        for f in func_descs:
            fnames.append(Subanta.praatipadikam(f))

        if res:
            loc['fnames'] = fnames
            for part in ['padas', 'varnas']:
                if part in res.views:
                    loc['pos'] = {
                        'part': part,
                        'inds': res.views[part]
                    }
                    if part == 'varnas':
                        pinds = res._derive_view('padas')
                        vinds = res.views[part]
                        newvinds = reduce(lambda x,y: x + y,
                                       [list(range(*pseq.padas(i)['pos']))
                                        for i in pinds])
                        if not (set(newvinds) & set(vinds)):
                            return None
                    res.add_label(f"{part}.sthaana", positions=res.views[part])
                    loc['result'] = res.views
        return res

    def _modify(self, pseq, func_desc):
        repl_parms = func_desc[0]
        repl_funcs = func_desc[1]
        debuglog("_modify: ", ppformat(func_desc), level=2)
        debuglog("_modify: ", pseq, level=2)
        debuglog("_modify: ", ppformat(repl_parms), level=2)

        if ('purva' in repl_parms) and ('para' not in repl_parms) and \
            ('sthaanas' not in repl_parms):
            skip = True
            for f in repl_funcs:
                f_pada = Subanta.praatipadikam(f)
                if 'Agama' in self.a.upadesha_category(f_pada):
                    log(f"_modify: Skipping due to aagama {f_pada}")
                    skip = False
                if f_pada in self.a.samjna_defs['sanAdi']['members']['val']:
                    log(f"_modify: Skipping due to sanAdi {f_pada}")
                    skip = False
            if skip:
                return None, 0, None

        mytrace = []
        loc = {}
        for location in ['sthaanas', 'para', 'purva']:
            if location in repl_parms:
                loc[location] = {}
                res = self._locate(pseq, loc[location], repl_parms[location])
                if not res:
                    self.debuglog(f"_locate {location}: {ppformat(repl_parms)} {pseq}")
                    del loc[location]
                    return None, 0, None
                mytrace.extend(res.samjnas)
        
        self.debuglog(f"_modify: {ppformat(loc)}", level=1)
        if not loc:
            return None, 0, None
#        if 'para' in loc and loc['para']['pos']['part'] == 'padas':
#            newinds = set()
#            for i in reversed(loc['para']['pos']['inds']):
#                if i > 0:
#                    ppos = pseq.padas(i)['pos']
#                    if ppos[0] == ppos[1]:
#                        newinds.add(i-1)
#            loc['para']['pos']['inds'] = sorted(list(newinds))

        mods = {}
        if 'sthaanas' in loc:
            sthaana_loc = loc['sthaanas']
            if 'para' in loc:
                para_loc = loc['para']
                if sthaana_loc['pos']['part'] == para_loc['pos']['part']:
                    inds1 = sthaana_loc['pos']['inds']
                    inds2 = [i-1 for i in para_loc['pos']['inds'] if i > 0]
                    mods['part'] = sthaana_loc['pos']['part']
                    inds = set(inds1) & set(inds2)
                    mods['para'] = para_loc
                elif sthaana_loc['pos']['part'] == 'padas':
                    inds1 = [pseq.padas(i)['pos'][1]-1 for i in sthaana_loc['pos']['inds']]
                    inds2 = [i-1 for i in para_loc['pos']['inds'] if i > 0]
                    mods['part'] = 'varnas'
                    inds = set(inds1) & set(inds2)
                    mods['para'] = para_loc
                else:
                    vinds1 = sthaana_loc['pos']['inds'] # varna
                    vinds2 = [pseq.padas(i)['pos'][0] for i in para_loc['pos']['inds']]
                    pinds1 = [i for i, p in enumerate(pseq.parts['padas'])
                                if (set(vinds1) & set(range(*p['pos']))) and
                                    (p['pos'][1] in vinds2)]
                    debuglog(f"_modify: vinds_sthana {vinds1} pinds_para {para_loc['pos']['inds']} vinds_para {vinds2} pinds_sthana {pinds1}")
                    if not pinds1:
                        return None, 0, None
                    inds = reduce(lambda x,y: x | y, [set(range(*pseq.parts['padas'][i]['pos'])) for i in pinds1])
                    inds = inds & set(sthaana_loc['pos']['inds'])
                    if not inds:
                        return None, 0, None
                    mods['part'] = sthaana_loc['pos']['part']
                    inds = sorted(list(inds))[-1:]   
            else:
                inds = sthaana_loc['pos']['inds']
                if 'purva' in loc:
                    purva_loc = loc['purva']
                    if sthaana_loc['pos']['part'] == purva_loc['pos']['part']:
                        inds1 = sthaana_loc['pos']['inds']
                        inds2 = [i + 1 for i in purva_loc['pos']['inds'] if i < len(pseq.parts[purva_loc['pos']['part']])-1]
                        inds = set(inds1) & set(inds2)
                    elif sthaana_loc['pos']['part'] == 'padas':
                        # convert the operation into padas   
                        ppos = next((i for i in range(len(pseq.padas())) if purva_loc['pos']['inds'][0] < pseq.padas(i)['pos'][1]), None)
                        inds2 = [ppos + 1]
                        inds = set(inds) & set(inds2)
                    #TODO: sthaana_loc = 'varnas' purva_loc is 'padas'
                mods['part'] = sthaana_loc['pos']['part']

            mods['replace'] = 2 if 'both' in repl_parms else 1
            mods['purva'] = sthaana_loc
            mods['inds'] = sorted(list(inds))
        elif 'para' in loc:
            para_loc = loc['para']
            if 'purva' not in loc:
                loc['purva'] = copy.deepcopy(loc['para'])
                if loc['purva']['pos']['part'] == 'padas':
                    loc['purva']['pos']['inds'] = pseq.views[loc['purva']['pos']['part']]
            if 'purva' in loc:
                purva_loc = loc['purva']
                if purva_loc['pos']['part'] == para_loc['pos']['part']:
                    vinds1 = purva_loc['pos']['inds']
                    vinds2 = [i-1 for i in para_loc['pos']['inds'] if i > 0]
                    mods['part'] = purva_loc['pos']['part']
                elif purva_loc['pos']['part'] == 'padas':
                    vinds1 = [pseq.padas(i)['pos'][1]-1 for i in purva_loc['pos']['inds']]
                    vinds2 = [i-1 for i in para_loc['pos']['inds'] if i > 0]
                    mods['part'] = 'varnas'
                else:
                    vinds1 = purva_loc['pos']['inds'] # varna
                    vinds2 = reduce(lambda x,y: x + y, \
                                [list(range(*pseq.padas(i-1)['pos'])) \
                                   for i in para_loc['pos']['inds'] \
                                    if i > 0])
                    mods['part'] = 'varnas'

                vinds = set(vinds1) & set(vinds2)
                mods['replace'] = 2 if 'both' in repl_parms else 0
                mods['purva'] = purva_loc['result']
                mods['para'] = para_loc['result']
                mods['inds'] = sorted(list(vinds))
        else: # only purva condition
            purva_loc = loc['purva']
            mods['replace'] = 0
            mods['purva'] = purva_loc['result']
            mods['part'] = purva_loc['pos']['part']
            mods['inds'] = purva_loc['pos']['inds']

        self.debuglog(f"_modify: {ppformat(mods)}", level=2)

        if not mods['inds']:
            return None, 0, None

        # Save current plist state in transform history
        pseq.push(self.sutra_id, mods)

        self.debuglog("_modify: ", ppformat(repl_funcs), level=2)
        res = pseq
        modified = False
        for f in repl_funcs:
            if 'skip' in f and f['skip']:
                continue
            res, modified, trace = self._transform(res, mods, f)
            if not res or modified == 0:
                break
            mytrace.extend(trace)
        if res and modified > 0:
            res.labels = {}
            if 'labels' in mods:
                for linfo in mods['labels']:
                    res.add_label(linfo['label'], positions=linfo['pos'], value=linfo['val'])
        return res, modified, mytrace

    # Return output whose format is same as returned by find_label()
    def _transform(self, pseq: PadaSequence, mods, func_desc):
        self.debuglog("_transform({}): {} @ location {}".format(ppformat(func_desc),
                                                                pseq, ppformat(mods)))
        r, modified, trace = self._transform_one(pseq, mods, func_desc)
        if modified:
            log(f"transform_one {func_desc['pada']}({pseq}) = modified={modified} res={r}")
        return r, modified, trace

    def _yatha_sankhyam(self, pseq, vinds, f_desc, mods):
        self.debuglog(f"yatha_sankhyam pseq={pseq}")
        srch_pada = "".join([pseq.varnas(i) for i in vinds])
        if mods['part'] == 'padas':
           #get the srch_pada from pada level info
           pind = mods['inds'][0]
           srch_pada = pseq.parts['padas'][pind]['pada']
        purva_fpada = mods['purva']['fnames'][-1]
        self.debuglog(f"yatha_sankhyam: purva_pada={purva_fpada} mod_f={f_desc}")

        def expand_fpada(fpada):
            mylist = []
            for c in fpada.split('-'):
                if c in self.a.samjna_defs:
                    if 'members' in self.a.samjna_defs[c]:
#                        print(f"expand_fpada: members of {c}: {self.a.samjna_defs[c]}")
                        val = self.a.samjna_defs[c]['members']['val']
                        if isinstance(val, list):
                            mylist.extend(self.a.samjna_defs[c]['members']['val'])
                        else:
                            mylist.append(val)
                else:
                    mylist.append(c)
            debuglog(f"expand_fpada: {fpada} {mylist}")
            return mylist

        purva_list = expand_fpada(purva_fpada)
        if '3' in srch_pada:
            purva_list = [v for v in purva_list if v in Siksha.v['pluta']]
        else:
            purva_list = [v for v in purva_list if v not in Siksha.v['pluta']]
        pos = [i for i,c in enumerate(purva_list) if c == Pada.nosvaras(srch_pada)]
        repl_list = expand_fpada(f_desc['praatipadikam'])
        self.debuglog(f"yatha_sankhyam: {srch_pada}{pos} {purva_list} -> {repl_list}")

        return repl_list[pos[0]] if pos[0] in range(len(repl_list)) else None

    # f_pada input must be in praatipadika form
    def _transform_one(self, pseq, mods, f_desc):
        if not mods or not mods['inds']:
            return None, 0, None
        f_pada = Subanta.praatipadikam(f_desc)
        f_pada_orig = f_pada
        mytrace = []

        debuglog("_transform_one({}): {}".format(ppformat(f_pada), ppformat(mods)))
        if re.match(r'^na$', f_pada) and f_desc['vibhakti'] == 0:
            return pseq, 0, []
        for f in self.predefined_vidhi_funcs:
            m = re.match(f, f_pada)
            if m:
                log("matched {} against predef {}".format(pseq, f))
                res, modified = self.predefined_vidhi_funcs[f](pseq, mods, m)
                return res, modified, []

        add_label = None
        f_pada_eval = False
        m = re.fullmatch(r'([^S])[at]', f_pada)
        if m:
            self.debuglog(f"_transform_one: {f_pada} is akaaranta")
            f_pada = m.group(1)
            f_desc = PadaSequence(padas=[f_pada])
        elif f_pada in self.a.upadesha_idx and (f_desc['type'] == 'subanta' and f_pada not in ['ku', 'cu', 'wu', 'tu', 'pu']):
            self.debuglog(f"_transform_one: upadesha {f_pada}: checking 'it'")
            if 'Agama' in self.a.upadesha_category(f_pada):
                if 'para' not in mods:
                    if pseq.find_label(f_pada):
                        return None, 0, None
                    add_label = f"padas.{f_pada}"
                mods['replace'] = 0
                if f_pada.endswith('w'):
                    mods['inds'] = [i-1 for i in mods['inds']]
                pseq.prev_transforms[-1]['mods'] = dict((k, mods[k]) for k in ['inds', 'part', 'replace'])
            u_desc = PadaSequence(padas=[f_pada])
            self.debuglog(f"_transform_one: {f_pada}: Applying sutra 13009 tasya lopaH")
            res, xform = self.a.apply_vidhi_rule(u_desc, sutra_id='13009')  # tasya (itaH) lopaH
            if res:
                mytrace.append(xform)
                f_desc = res
                f_pada = "".join(f_desc.varnas())
            else:
                f_desc = u_desc
        else:
            f_pada_eval = True

        self.debuglog("_transform_one not predef ({}): {}".format(f_desc, ppformat(mods)))
        modified = False
        if mods['part'] == 'padas':
            for i in mods['inds']:
                self.debuglog(f"_transform_one: at pada {i} replace={mods['replace']} pada {f_pada}")
                if not mods['replace']:
#                    f_res = self.a.label_samjna(f_desc, 'sanAdi')
                    f_res = None
                    if f_res:
                        vinds = [pseq.padas(i)['pos'][1]-1]
                        pseq.upsertvarnas(vinds, f_desc.varnas())
                        pseq.add_label("padas.upadeSa",
                                       positions=mods['inds'], value='DAtu')
                    else:
                        pseq.modpada(f_desc, i)
                    modified = True
                elif 'pada_split' in f_desc:
                    vinds = list(range(*pseq.padas(i)['pos']))
                    newvarnas = self._yatha_sankhyam(pseq, vinds, f_desc, mods)
                    pseq.upsertvarnas(vinds, newvarnas, mods['replace'])
                    modified = True
        else:
            for i in sorted(mods['inds'], reverse=True):
                vinds = [i]
                if mods['replace'] == 2:
                    vinds.append(i+1)

                newvarnas = None
                if f_pada == 'pararUpa':
                    vinds.pop()
                    newvarnas = None
                elif 'pada_split' in f_desc and 'eval' not in f_desc:
                    newvarnas = self._yatha_sankhyam(pseq, vinds, f_desc, mods)
                elif f_pada_eval:
                    if 'pada_split' not in f_desc and self.a.eval_samjna(f_pada):
                        defn = self.a.samjna_defs[f_pada]
                        self.debuglog(f"_transform_one({f_pada}: defn = {ppformat(defn)}")
                        if 'members' in defn:
                            val = defn['members']['val']
                            if isinstance(val, str):
                                newvarnas = [val[0]]
                            elif isinstance(val, list):
                                newvarnas = Siksha.max_savarna(pseq.varnas(i), val)
                                if not newvarnas:
                                    return None, 0, None
                                self.debuglog(f"{newvarnas} {pseq.varnas(i)} vs {val}")
                    if not newvarnas:
                        newvarnas = []
                        pluta = False
                        for vi in vinds:
                            v = pseq.varnas(vi)
                            pluta = pluta or ('3' in v)
                            savarnas = Siksha.savarRa(v)
                            self.debuglog(f"savarnas({v}) = {savarnas}")
                            if not newvarnas:
                                newvarnas = set(savarnas)
                            newvarnas = set(newvarnas) & set(savarnas)
                            newseq = PadaSequence(padas=newvarnas)
                            newseq = Rule(self.a)._interpret_pada(newseq, f_desc)
                            if not newseq:
                                return None, 0, None
                            newvarnas = newseq.varnas()
                            self.debuglog(f"newvarnas = {newvarnas}")

                        # Look for common varnas with same kaala and most restrictive sthaana as v1,v2
                        if pluta:
                            # remove dIrgha varnas
                            rmvarnas = [v for v in newvarnas if v in Siksha.v['dIrGa']]
                        else:
                            # remove pluta varnas
                            rmvarnas = [v for v in newvarnas if v in Siksha.v['pluta']]
                        findvarnas = [Pada.nosvaras(pseq.varnas(i)) for i in vinds]
                        newvarnas = Siksha.max_savarna(findvarnas, set(newvarnas) - set(rmvarnas))
                        debuglog(f"_transform_one({v}): max_savarna = {newvarnas}")
                        if not newvarnas:
                            return None, 0, None
                else:
                    newvarnas = Pada.split(f_pada, '3\\\^~')
                self.debuglog("_transform_one: ", f_pada, vinds, newvarnas, pseq)
                if add_label and mods['replace'] == 0:
                    sthaana_varnas = mods['purva']['pos']['inds'] if 'pos' in mods['purva'] else mods['purva']['varnas']
                    sthaana_pada = [j for j, p in enumerate(pseq.parts['padas'])
                                    if set(sthaana_varnas) & set(range(*p['pos']))]
                    debuglog(f"_transform_one: sthaana_pada = {sthaana_pada} varna = {sthaana_varnas}")
                if mods['replace'] == 1 and pseq.varnas(vinds[0]) == newvarnas[0]:
                    return None, 0, None
                pseq.upsertvarnas(vinds, newvarnas, mods['replace'])
                if f_pada_orig == 'Sna':
                    vind_start = vinds[0]
                    if mods['replace'] == 0:
                        vind_start += 1
                    pinds = [i for i, p in enumerate(pseq.parts['padas'])
                             if vind_start in range(*p['pos'])]
                    mods['labels'] = [
                        {'label': f"padas.{f_pada_orig}",
                         'pos': pinds,
                         'val': { 'type': 'varnas',
                                  'val': list(range(vind_start, vind_start+len(newvarnas))) }
                         } ]
                if add_label:
                    if mods['replace'] == 0:
                        # if inserting an Agama in between two padas,
                        # Make it logically part of the next pada, not previous.
                        nextind = vinds[0]+len(newvarnas)+1
                        debuglog(f"_transform_one: Agama {mods}")
                        j = sthaana_pada[0]
                        p = pseq.padas(j)
                        moved = False
                        while p['pos'][0] == nextind:
                            end = p['pos'][1]
                            if end == nextind:
                                end -= len(newvarnas)
                            p['pos'] = (p['pos'][0] - len(newvarnas), end)
                            moved = True
                            j = j - 1
                            if j < 0:
                                break
                            p = pseq.padas(j)
                        if moved and j >= 0:
                            p['pos'] = (p['pos'][0], p['pos'][1] - len(newvarnas))
                    debuglog(f"Adding {add_label} as label")
                    PadaSequence(pseq.prev_transforms[-1]['pseq']).add_label(add_label, positions=[0], value=True)
                modified = True

        return (pseq, 1, mytrace) if modified else (None, 0, None)

    def _tag(self, pseq: PadaSequence, func_descs):
        res = pseq
        tags = [Subanta.praatipadikam(f) for f in func_descs[1]]
        matched = False
        tags_found = [t for t in tags if t in pseq.labels]
        log(f"_tag: looking for {tags} - preexisting tags: {tags_found}")
        if tags_found:
            return None, 0, None

        for f in func_descs[0]:
            res = super(Vidhi_rule, self)._apply_func(res, f)
            if not res:
                continue
            samjna = Subanta.praatipadikam(f)
            newlabel = copy.deepcopy(res.labels[samjna])
            for t in tags:
                pseq.labels[t] = newlabel
                for part,matches in pseq.labels[t].items():
                    for m in matches:
                        m['sutra_id'] = self.sutra_id
            matched = True

        log(f"_tag: matched = {matched} pseq = {pseq}")
        if matched:
            tags = [Subanta.praatipadikam(f) for f in func_descs[1]]
            tags_found = [t for t in tags if t in pseq.labels]
            log(f"_tag: returning {tags} - found tags: {tags_found}")
            if not tags_found:
                raise RuntimeError(f"_tag: didn't work")
                return None, 0, None
        return (pseq, 1, []) if matched else (None, 0, None)

    def compile(self, plist):
        plist = [self.a.pada_override(f_desc) for f_desc in plist]
        plist = sorted(plist, key=lambda x: int(x['seq']) if 'seq' in x else 0)
        func_descs = []

        # To handle kartari krt
        p1, rest1 = self.bifurcate(plist, {'vibhakti': 1})
        if p1:
            p7, rest1 = self.bifurcate(rest1, {'vibhakti': 7})
            if p7:
                artha_saptami = True
                for p in p1:
                    if not self.a.is_samjna(Subanta.praatipadikam(p)):
                        artha_saptami = False
                        break
                if artha_saptami and not rest1:
                    info(f"found samjna {p['pada']} and artha-saptami")
                    self.rule = ["TAG", p1, p7]
                    return self.rule

        repl_parms = {}
        p6, rest = self.bifurcate(plist, {'vibhakti': 6})
        if p6:
            purvapara, rest6 = self.bifurcate(p6, {'praatipadikam' : 'pUrva-para'})
            if purvapara:
                eka, rest = self.bifurcate(rest,
                                           {'praatipadikam' : 'eka',
                                            'vibhakti': 1})
                repl_parms['both'] = True if eka else False
            if rest6:
                repl_parms['sthaanas'] = rest6

        p7, rest = self.bifurcate(rest, {'vibhakti': 7})
        if p7:
            repl_parms['para'] = p7

        p5, rest = self.bifurcate(rest, {'vibhakti': 5})
        if p5:
            repl_parms['purva'] = p5

        p3, rest = self.bifurcate(rest, {'vibhakti': 3})
        if p3:
            if 'purva' not in repl_parms:
                repl_parms['purva'] = []
            repl_parms['purva'].extend(p3)
            if 'para' not in repl_parms:
                repl_parms['para'] = []
            repl_parms['para'].extend(p3)

        p1, rest = self.bifurcate(rest, {'vibhakti': 1})
        if p1:
            match, rest = self.bifurcate(plist, {'pada': 'na', 'vibhakti': 0})
            if match:
                p1 = match + p1
            func_descs = ["MODIFY", repl_parms, p1]

        self.rule = func_descs
        return self.rule

    def apply(self, pseq: PadaSequence):
        return self._apply_func(pseq, self.rule)
