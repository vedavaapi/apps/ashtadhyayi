from pymongo import MongoClient
from pymongo.database import Database
from pprint import pprint
import re
import sys
import csv
import os

from bson.objectid import ObjectId
import json
from operator import itemgetter, attrgetter, methodcaller

import urllib2

from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
            
def table2json(t):
    return [dict(zip(t['fields'], valrow)) for valrow in t['values']]

class MyCollection:
    def __init__(self, name, mydb, cache=False):
        self.name = name
        self.mydb = mydb
        self.collection = mydb.db[self.name]
        self.cache = cache
        self.local = None
        if cache:
            self.slurp()

    def slurp(self):
        self.local = {}
        for o in self.collection.find():
            o['_id'] = str(o['_id'])
            self.local[o['_id']] = o

        return self.local

    def all(self):
        if not self.local:
            self.slurp()
        return self.local

    def count(self):
        return self.collection.count()

    def toJSON(self):
        return self.slurp()

    def fromJSON(self, data):
        self.collection.drop()
        try:
            for d in data:
                self.insert(d)
        except Exception as e:
            print "Error inserting into " + self.name + ": ", e
        if self.cache:
            self.slurp()

    def fromCSV(self, fname, primarykey = None, ignorecommas=False):
        table = {'name' : self.name}
        with open(fname) as f:
            gothdr = False
            idx = {}
            primaryidx = None
            for row in csv.reader(f):
                if not gothdr:
                    gothdr = True
                    row[0] = row[0].lstrip('#')
                    hdr_split = [e.split(' ', 1) if ' ' in e else [e, ''] for e in row]
                    (cols, coldescs) = zip(*hdr_split)
                    idx = dict(zip(cols, range(len(cols))))
                    table['fields'] = list(cols)
                    '''assigned before << if primarykey: >> block becz table['fields'] should be assigned first before accessing it, like conditional appending of '_id' field.
                    wrapped in list() call, as cols is tuple, and immutable but we may need to append '_id' field to table['fields'] if primarykey is given.
                    '''
                    table['descs'] = list(coldescs)
                    if primarykey:
                        table['fields'].append('_id')
                        primaryidx = idx[primarykey]
                    table['values'] = []
                    continue
                if row[0].startswith('#'):
                    continue
                #print [re.split(r'\s*,\s*', r) for r in row if ',' in r]
                if not ignorecommas:
                    row = map(lambda x: re.split(r',\s*', x) if ',' in x else x, row)
                values = row
                #print row
                if primarykey:
                    values.append(values[primaryidx])
                table['values'].append(values)
            self.fromJSON(table2json(table))

    def fromGSheetWithOAuth(self, gsheet_url, gsheet_name, fields):
        '''Arguments
        ---------------
        gsheet_url: url of google sheet
        gsheet_name: name of sheet in workbook. like 'Concept-sum', etc.
        fields: a sequence of required fields in spread sheet. if spreadsheet contains other fields, and corresponding values, they will be filtered out. in code, final_table variable is for required filtered out table.

        this function uses google's "google-api-python-client" module for python-api to sheets.
        it needs OAuth credintials to access sheets.
        we should generate first client-secret.json , as mentioned in https://developers.google.com/sheets/api/quickstart/python , and keep it in working directory.
        and then we can even read or write private/public sheets by adding needed scopes.

        if we need to only 'read' 'public sheets', then there is no need of OAuth authentication, or no need for google-api-python-client module. we can just use
        a registered Api-key, and use Http protocols for getting jsonified data.
        next fromGSheet() method does this, with out OAuth on publicly shared sheets.
        '''
        gsheet_id = re.search(r'/spreadsheets/d/([a-zA-Z0-9-_]+)', gsheet_url).group(1)

        SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
        store = file.Storage('credentials.json')
        creds = store.get()
        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets('client_secret.json', SCOPES)
            creds = tools.run_flow(flow, store)
        service = build('sheets', 'v4', http=creds.authorize(Http()))

        response_table = service.spreadsheets().values().get(spreadsheetId=gsheet_id,
                                                 range=gsheet_name).execute()


        table = {'name': gsheet_name}
        gothdr = False
        idx = {}
        gs_json_values_key = 'values'

        for row in response_table[gs_json_values_key]:
            if not gothdr:
                gothdr = True
                row[0] = row[0].lstrip('#') #it is like in fromCSV() method.
                hdr_split = [e.split(' ', 1) if ' ' in e else [e, ''] for e in row]
                (cols, coldescs) = zip(*hdr_split)
                idx = dict(zip(cols, range(len(cols))))
                table['fields'] = list(cols)
                table['descs'] = list(coldescs)
                table['values'] = []
                continue

            if row[0].startswith('#'):
                '''this if block is added as in fromCSV() method. in csv, lines starting with #
                seems considered as comments in fromCSV() method. that may not be case with sheets.
                so we may just 'pass' instead of 'continue' for normal behaviour.
                '''
                continue

            if reduce((lambda x, y: x and y), [e=='-' for e in row]):
                '''checks if all fields are fillers. i.e all field values equal to '-'.
                they are used as empty fillers in sheet. so we can filter them out with this block.
                '''
                continue

            values = row
            if len(values) < len(table['fields']):
                #this will append empty string values, to match number of values in each row to number of header fields.
                values+= [""]*(len(table['fields']) - len(values))
            table['values'].append(values)

        reqd_field_indices = [idx[field] for field in fields] # this is to filter in only required fields, which are passed in as parameter.
        final_table= {'name': gsheet_name}
        final_table['fields'] = [table['fields'][i] for i in reqd_field_indices]
        final_table['descs'] = [table['descs'][i] for i in reqd_field_indices]
        final_table['values']= [[table['values'][i][j] for j in reqd_field_indices] for i in range(len(table['values']))]
        #print final_table
        self.fromJSON(table2json(final_table))


    def fromGSheet(self, gsheet_url, gsheet_name, fields = None):
        '''
        Arguments
        ---------------
        gsheet_url: url of google sheet
        gsheet_name: name of sheet in workbook. like 'Concept-sum', etc.
        fields: a sequence of required fields in spread sheet. if spreadsheet contains other fields, and corresponding values, they will be filtered out. in code, final_table variable is for required filtered out table.

        NOTE: to access google sheets programmetically, through google sheets api v4, 
        we need an Oauth creadintials object. with that we can use "google-api-python-client" module, and access sheet.
        but sheets which are shared publicly, can be accessed with out OAuth credintials. but they need 
        an API-key to access them, which we can generate for a project after registering at google console.
        in this method, my Api-Key= AIzaSyCWbhC_Wl0qFPFV293as-cmw7gUl7K4Gk4 is used, which is public, with no restrictions on usage..
        for a big project, it is better to generate Oauth credintials, and use google's "google-api-python-client" module, along with Oauth's client-secret file..
         '''

        gs_api_url_pref = "https://sheets.googleapis.com/v4/spreadsheets/"
        api_key = "AIzaSyCWbhC_Wl0qFPFV293as-cmw7gUl7K4Gk4"
        gsheet_id = re.search(r'/spreadsheets/d/([a-zA-Z0-9-_]+)', gsheet_url).group(1)

        query_url = gs_api_url_pref + gsheet_id + '/values/' + gsheet_name + '?key=' + api_key

        json_response_string = urllib2.urlopen(query_url).read()
        response_table = json.loads(json_response_string)

        table = {'name': gsheet_name}
        gothdr = False
        idx = {}
        gs_json_values_key = 'values' #in returned json, values are mapped to this key

        for row in response_table[gs_json_values_key]:
            if not gothdr:
                gothdr = True
                row[0] = row[0].lstrip('#') #it is like in fromCSV() method.
                hdr_split = [e.split(' ', 1) if ' ' in e else [e, ''] for e in row]
                (cols, coldescs) = zip(*hdr_split)
                idx = dict(zip(cols, range(len(cols))))
                table['fields'] = list(cols)
                table['descs'] = list(coldescs)
                table['values'] = []
                continue

            if row[0].startswith('#'):
                '''this if block is added as in fromCSV() method.
                in csv, lines starting with # seems considered as comments in fromCSV() method. that may not be case with sheets.
                so we may just 'pass' instead of 'continue' for normal behaviour.
                '''
                continue

            if reduce((lambda x, y: x and y), [e=='-' for e in row]):
                '''checks if all fields are fillers. i.e all field values equal to '-'.
                they are used as empty fillers in sheet. so we can filter them out with this block.
                '''
                continue

            values = row
            if len(values) < len(table['fields']):
                #this will append empty string values, to match number of values in each row to number of header fields. some times trailer empty cells will be trimmed out by google api. it will fix this.
                values+= [""]*(len(table['fields']) - len(values))
            table['values'].append(values)

        if not fields:
            fields = table['fields']
        reqd_field_indices = [idx[field] for field in fields] # this is to filter in only required fields, which are passed in as parameter.
        final_table= {'name': gsheet_name}
        final_table['fields'] = [table['fields'][i] for i in reqd_field_indices]
        final_table['descs'] = [table['descs'][i] for i in reqd_field_indices]
        final_table['values']= [[table['values'][i][j] for j in reqd_field_indices] for i in range(len(table['values']))]
        res = json.dumps(final_table, indent=2, ensure_ascii=False).encode('utf8')
        print res
        self.fromJSON(table2json(final_table))


    def __repr__(self):
        return json.dumps(self.toJSON())

    def oid(self, item_id):
        try:
            return {'_id' : ObjectId(item_id)}
        except Exception as e:
            print "Error: invalid object id ", e
            return None

    def get(self, item_id):
        res = self.local[item_id] if self.cache else None
        if not res:
            query = self.oid(item_id)
            if not query:
                return None
            res = self.collection.find_one(query)
            if res:
                res['_id'] = str(res['_id'])
        return res

    def find_one(self, query):
        res = self.collection.find_one(query)
        if res:
            res['_id'] = str(res['_id'])
        return res

    def insert(self, item):
        #print "Inserting: ",item
        try:
            result = self.collection.insert_one(item)
        except Exception as e:
            print "Error inserting into " + self.name + ": ", e
            return None
        return str(result.inserted_id)

    def update(self, item_id, fields):
        query = self.oid(item_id)
        if not query:
            return False
        result = self.collection.update(query, {"$set" : fields})
        isSuccess = (result['n'] > 0)
        return isSuccess

    def delete(self, item_id):
        query = self.oid(item_id)
        if not query:
            return False
        res = self.collection.delete_one(query)
        if res:
            return res.deleted_count > 0
        else:
            return False

    def reset(self):
        return self.collection.drop()

    def find(self, query = {}, fields = []):
        if len(fields) > 0:
            f = dict((k, 1) for k in fields)
            return self.collection.find(query, f)
        else:
            return self.collection.find(query)

    def __exit__(self, type, value, traceback):
        return True

class MyDB:
    def __init__(self, dbname):
        self.dbname = dbname
        self.db = None
        self.c = {}
        self.initialize()
#        if not database.write_concern.acknowledged:
#            raise ConfigurationError('database must use '
#                                     'acknowledged write_concern')

    def __getattr__(self, name):
        if name not in self.c:
            self.add(name)
        return self.c[name]

    def __getitem__(self, name):
        if name not in self.c:
            self.add(name)
        return self.c[name]

    def add(self, cname, cache=False):
        collection = MyCollection(cname, self, cache)
        self.c[collection.name] = collection

    def list(self):
        return self.c.keys()

    def initialize(self):
        try:
            self.client = MongoClient()
            self.db = self.client[self.dbname]
            if not isinstance(self.db, Database):
                raise TypeError("database must be an instance of 'Database'")
        except Exception as e:
            print("Error initializing MongoDB database; aborting.", e)
            sys.exit(1)

    def reset(self):
        print "Clearing database", self.dbname
        self.client.drop_database(self.dbname)
        self.initialize()

if __name__ == "__main__":
    (cmddir, cmdname) = os.path.split(__file__)

    gsheet_url = 'https://docs.google.com/spreadsheets/d/1ngOkSDJjqgohO8FrAiiQRQYCuol6qZ2lUPbzduzsfv8/edit?usp=sharing'
    db = MyDB("testdb")
    db.add("gsheet")
    db.gsheet.fromGSheet(gsheet_url, 'Concept-sum')
