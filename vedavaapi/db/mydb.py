from pymongo import MongoClient
from pymongo.database import Database
from pprint import pprint
import re
import sys
import csv

from bson.objectid import ObjectId
import json
from operator import itemgetter, attrgetter, methodcaller
            
def table2json(t):
    return [dict(zip(t['fields'], valrow)) for valrow in t['values']]

class MyCollection:
    def __init__(self, name, mydb, cache=False):
        self.name = name
        self.mydb = mydb
        self.collection = mydb.db[self.name]
        self.cache = cache
        self.local = None
        if cache:
            self.slurp()

    def slurp(self):
        self.local = {}
        for o in self.collection.find():
            o['_id'] = str(o['_id'])
            self.local[o['_id']] = o

        return self.local

    def all(self):
        if not self.local:
            self.slurp()
        return self.local

    def count(self):
        return self.collection.count()

    def toJSON(self):
        return self.slurp()

    def fromJSON(self, data):
        self.collection.drop()
        try:
            for d in data:
                self.insert(d)
        except Exception as e:
            print "Error inserting into " + self.name + ": ", e
        if self.cache:
            self.slurp()

    def fromCSV(self, fname, primarykey = None, ignorecommas=False):
        table = {'name' : self.name}
        with open(fname) as f:
            gothdr = False
            idx = {}
            primaryidx = None
            for row in csv.reader(f):
                if not gothdr:
                    gothdr = True
                    row[0] = row[0].lstrip('#')
                    hdr_split = [e.split(' ', 1) if ' ' in e else [e, ''] for e in row]
                    (cols, coldescs) = zip(*hdr_split)
                    idx = dict(zip(cols, range(len(cols))))
                    if primarykey:
                        table['fields'].append('_id')
                        primaryidx = idx[primarykey]
                    table['fields'] = cols
                    table['descs'] = coldescs
                    table['values'] = []
                    continue
                if row[0].startswith('#'):
                    continue
                #print [re.split(r'\s*,\s*', r) for r in row if ',' in r]
                if not ignorecommas:
                    row = map(lambda x: re.split(r',\s*', x) if ',' in x else x, row)
                values = row
                #print row
                if primarykey:
                    values.append(values[primaryidx])
                table['values'].append(values)
            self.fromJSON(table2json(table))

    def __repr__(self):
        return json.dumps(self.toJSON())

    def oid(self, item_id):
        try:
            return {'_id' : ObjectId(item_id)}
        except Exception as e:
            print "Error: invalid object id ", e
            return None

    def get(self, item_id):
        res = self.local[item_id] if self.cache else None
        if not res:
            query = self.oid(item_id)
            if not query:
                return None
            res = self.collection.find_one(query)
            if res:
                res['_id'] = str(res['_id'])
        return res

    def find_one(self, query):
        res = self.collection.find_one(query)
        if res:
            res['_id'] = str(res['_id'])
        return res

    def insert(self, item):
        #print "Inserting: ",item
        try:
            result = self.collection.insert_one(item)
        except Exception as e:
            print "Error inserting into " + self.name + ": ", e
            return None
        return str(result.inserted_id)

    def update(self, item_id, fields):
        query = self.oid(item_id)
        if not query:
            return False
        result = self.collection.update(query, {"$set" : fields})
        isSuccess = (result['n'] > 0)
        return isSuccess

    def delete(self, item_id):
        query = self.oid(item_id)
        if not query:
            return False
        res = self.collection.delete_one(query)
        if res:
            return res.deleted_count > 0
        else:
            return False

    def reset(self):
        return self.collection.drop()

    def find(self, query = {}, fields = []):
        if len(fields) > 0:
            f = dict((k, 1) for k in fields)
            return self.collection.find(query, f)
        else:
            return self.collection.find(query)

    def __exit__(self, type, value, traceback):
        return True

class MyDB:
    def __init__(self, dbname):
        self.dbname = dbname
        self.db = None
        self.c = {}
        self.initialize()
#        if not database.write_concern.acknowledged:
#            raise ConfigurationError('database must use '
#                                     'acknowledged write_concern')

    def __getattr__(self, name):
        if name not in self.c:
            self.add(name)
        return self.c[name]

    def __getitem__(self, name):
        if name not in self.c:
            self.add(name)
        return self.c[name]

    def add(self, cname, cache=False):
        collection = MyCollection(cname, self, cache)
        self.c[collection.name] = collection

    def list(self):
        return self.c.keys()

    def initialize(self):
        try:
            self.client = MongoClient()
            self.db = self.client[self.dbname]
            if not isinstance(self.db, Database):
                raise TypeError("database must be an instance of 'Database'")
        except Exception as e:
            print("Error initializing MongoDB database; aborting.", e)
            sys.exit(1)

    def reset(self):
        print "Clearing database", self.dbname
        self.client.drop_database(self.dbname)
        self.initialize()
