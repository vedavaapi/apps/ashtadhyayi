#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append("..")

import re
from vedavaapi.ashtadhyayi import *
from pprint import pprint

rebuild = True
ashtadhyayi_init(rebuild)
set_req_context(ReqContext())

dhatu = "Baja~"
pratyaya = "GaY"
gana = "BvAdi"

pdescs = [{"pada": dhatu, "label": "dhatu.pada", "gaRa": gana}, {"pada": pratyaya, "label": "pratyaya"}]

# pdescs = [{"pada": "rAma","label": "pratipadika"}, {"pada": "su~", "label": "pratyaya"}]

# loglevel(1)
result = a().interpret(pdescs)
pprint(result)
