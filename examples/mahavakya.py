#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys

(file_path, fname) = os.path.split(__file__)
app_path = file_path
if app_path:
  os.chdir(app_path)
sys.path.insert (0, os.path.join(app_path, ".."))

import sys
from vedavaapi.ashtadhyayi import *

if (len(sys.argv) < 2):
    print("Usage: " + sys.argv[0] + " <sutranum>")
    print("    For sutra 1.1.1, give 11001")
    exit(1)

rebuild = False
if sys.argv[1] == '-r':
    rebuild = True
    sys.argv.pop(1)

ashtadhyayi_init(rebuild)
print("Mahavakya of " + sys.argv[1])
x = a().mahavakya(sys.argv[1])
print_dict(x)
exit(0)
