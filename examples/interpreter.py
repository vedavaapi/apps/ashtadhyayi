import sys, traceback
sys.path.append("..")
from vedavaapi.ashtadhyayi import *
from pprint import pprint, pformat
from examples.rupa_siddhi_inputs import *
import inspect
import csv

def analyze(pada, inscr="SLP1", outscr="SLP1"):
    script(outscr)

    # Find the sutra sequence to execute for given pada sequence
    for alg in rupa_siddhi_inputs:
        if pada == alg['result']:
            return ppformat(alg, outscr=outscr)

    return None

def parse_loglevel(vstr):
    loglevels = {}
    for vopt in vstr.split("-"):
        if ":" in vopt:
            level, sutras = vopt.split(":")
            for snum in sutras.split(","):
                loglevels[snum] = level
        else:
            loglevels[0] = vopt
    print("loglevels = ", loglevels)
    return loglevels

if __name__ == '__main__':
    pgmname = sys.argv.pop(0)

    def usage():
        print("Usage: " + pgmname + " [-v <loglevel>] [-r] <pada_1>[,label=dhatu,gaRa=BuvAdi] [<pada2> ...]")
        print("    -r   Rebuild Ashtadhyayi database")
        print("    -v <loglevel>[-level:sutranum1,sutranum2,..]*"
              "         Log messages at <loglevel> verbosity.")
        print("    -l <SCRIPT>"
              "         Produce output in <SCRIPT>."
              "         <SCRIPT> can be SLP1, DEVANAGARI, IAST, TELUGU etc."
              "         Default script is SLP1.")
        print("    -s <sutra_id>[,<sutra_id>]"
              "         Apply given sutras in sequence."
              "         Default: Auto-select sutras.")
        print("    -e <expected_result>"
              "         Expect given result.")
        exit(1)

    rebuild = False
    mock = False
    exp_result = None
    infile = None
    inpadas = []
    sutras = []
    inscript = "SLP1"
    outscript = "SLP1"
    verbose = 0
    loglevels = {}
    set_req_context(ReqContext())

    while sys.argv:
        if sys.argv[0] == '-v':
            sys.argv.pop(0)
            if not sys.argv:
                usage()
            r().loglevels = parse_loglevel(sys.argv[0])
            loglevels = r().loglevels
            verbose = r().loglevels[0]
            loglevel(verbose)
        elif sys.argv[0] == '-r':
            rebuild = True
        elif sys.argv[0] == '-h':
            usage()
        elif sys.argv[0] == '-n':
            mock = True
        elif sys.argv[0] == '-e':
            sys.argv.pop(0)
            if not sys.argv:
                usage()
            exp_result = sys.argv[0]
        elif sys.argv[0] == '-l':
            sys.argv.pop(0)
            if not sys.argv:
                usage()
            inscript = outscript = sys.argv[0]
        elif sys.argv[0] == '-f':
            sys.argv.pop(0)
            if not sys.argv:
                usage()
            infile = sys.argv[0]
        elif sys.argv[0] == '-s':
            sys.argv.pop(0)
            if not sys.argv:
                usage()
            sutras = sys.argv[0].split(',')
            print(sutras)
        else:
            inpadas = sys.argv
            break
        sys.argv.pop(0)

    try:
        ashtadhyayi_init(rebuild)
        failed_results = []
        all_results = []
        if infile:
            stats = {'ntotal': 0, 'nsuccess': 0, 'nfailed': 0, 'ncrash': 0, 'nskipped': 0}

            for row in read_csvfile(infile):
                set_req_context(ReqContext())
#                dhatu = xliterate(row['dhatu'].strip())
#                gana = xliterate(row['gaNa'].strip())
#                pratyaya = xliterate(row['pratyaya'].strip())
#                if dhatu in a().upadesha_idx:
#                    for p in a().upadesha_idx[dhatu]:
#                        if gana == p.labels['gaNa']['padas'][0]['val']:
#                            dhatu = p.padas(0)['pada']
#                            break
                entry = {}
                for k,v in row.items():
                    v = xliterate(v.strip(), row['inscript'], "SLP1")
                    comps = k.split('.')
                    if len(comps) > 1:
                        if comps[0] not in entry:
                            entry[comps[0]] = { 'label': k }
                        entry[comps[0]][".".join(comps[1:])] = v
                    else:
                        entry[comps[0]] = { 'pada': v, 'label': k }
                padas = [v for k,v in entry.items() \
                         if k in ['upasarga', 'dhatu', 'pratyaya'] and v['pada']]
                padastr = [p['pada'] for p in padas]
                stats['ntotal'] += 1
                if int(row['loglevel']) < 0 or ('exp_result' in row and row['exp_result'] == '-'):
                    stats['nskipped'] += 1
                    log(f"skipping {padastr}")
                    continue
                pprint(padas)
                loglevel(row['loglevel'])
                resout = {
                    'serialno': stats['ntotal'] - stats['nskipped'],
                    'padas': padastr,
                    'gana': entry['dhatu']['gaRa'],
                    'upasarga': entry['upasarga']['pada'] if 'upasarga' in entry else '',
                    'dhatu': entry['dhatu']['pada'],
                    'pratyaya': entry['pratyaya']['pada'],
                    'expected': row['exp_result'] if 'exp_result' in row else '',
                    'generated': '',
                    'success': True,
                    'crashed': False}
                sutras = re.split(r'\s*,\s*', row['sutras'].replace('\s', ''))
                result = a().interpret(padas, inscr=row['inscript'],
                          outscr=row['outscript'], sutras=[],
                          result=row['exp_result'].split('/'))
                if result:
#                    result['mods'] = None
                    info(f"{pformat(result)}")
                    resout['expected'] = "/".join(result['expected'])
                    resout['generated'] = result['generated']
                    if result['success']:
                        stats['nsuccess'] += 1
                    else:
                        stats['nfailed'] += 1
                        resout['success'] = False
                        failed_results.append(result)
                else:
                    resout['crashed'] = True
                    resout['success'] = False
                    stats['ncrash'] += 1
                    failed_results.append(resout)

                all_results.append(resout)
                info(stats)
                with open(f"output-{stats['ntotal']}.json", "w", encoding="utf-8") as outf:
                    outf.write(print_dict(r()) + "\n")

                if not result or not result['success']:
                    pass
#                  break
            for r in failed_results:
                info(f"{r['padas']} = {r['generated']} (expected {r['expected']})")
            colnames = ['serialno', 'gana', 'dhatu', 'pratyaya', 'generated', 'expected',
                            'status']
            print(",".join(colnames))
            for r in all_results:
                if r['success']:
                    r['status'] = 'success'
                elif r['crashed']:
                    r['status'] = 'crashed'
                else:
                    r['status'] = 'failed'
                print(",".join([str(r[c]) for c in colnames]))
        elif inpadas:
            set_req_context(ReqContext())
            r().loglevels = loglevels
            loglevel(verbose)
            print(inpadas)
            pdescs = []
            for p in inpadas:
                comps = p.split(",")
                v = comps.pop(0)
                v = xliterate(v.strip(), inscript, "SLP1")
                entry = { "pada": v, "label": "pada" }
                for c in comps:
                    l = c.split('=')
                    entry[l[0]] = l[1]
                pdescs.append(entry)
            print(pdescs)
            log(f"Analyzing {ppformat(pdescs)} with sutras {sutras} ...")
            result = a().interpret(pdescs, inscr=inscript, sutras=sutras, outscr=outscript)
            if result:
                info(f"{pformat(result)}")
    except Exception as e:
        traceback.print_exc(file=sys.stdout)
        log("Failed: Caught exception", e)

    sys.exit(0)