import sys, traceback
sys.path.append("..")
from vedavaapi.ashtadhyayi import *
from pprint import pprint, pformat
from examples.rupa_siddhi_inputs import *
import inspect
import csv

# Expected transformation

def classify_dhatus():
    categories = [
        {'praatipadikam': 'at&()antya', 'pada': 'adanta', 'type': 'subanta', 'vibhakti': 1, 'vachana': 1},
        {'praatipadikam': 'At&()antya', 'pada': 'Ad_anta', 'type': 'subanta', 'vibhakti': 1, 'vachana': 1},
        {'praatipadikam': 'ik&()antya', 'pada': 'iganta', 'type': 'subanta', 'vibhakti': 1, 'vachana': 1},
        {'praatipadikam': 'ec&()antya', 'pada': 'ejanta', 'type': 'subanta', 'vibhakti': 1, 'vachana': 1},
        {'praatipadikam': 'hal&()antya', 'pada': 'halanta', 'type': 'subanta', 'vibhakti': 1, 'vachana': 1},
        {'praatipadikam': 'at&()upaDA', 'pada': 'adupaDA', 'type': 'subanta', 'vibhakti': 1, 'vachana': 1},
        {'praatipadikam': 'ik&()upaDA', 'pada': 'igupaDA', 'type': 'subanta', 'vibhakti': 1, 'vachana': 1},
        {'praatipadikam': 'hal&()upaDA', 'pada': 'Seza', 'type': 'subanta', 'vibhakti': 1, 'vachana': 1}
    ]

    with open("dhatu_patha_new_slp1.csv", "w") as f:
        hdrs = "dhatu_mod,gaNa,iT,padi,dhatu,artha,x_anta,x_upadhA".split(',')
        f.write(",".join(hdrs) + "\n")
        lineno = 0
        for p in a().upadesha['DAtu']:
#            loglevel(0)
            lineno = lineno + 1
            res, xform = a().apply_vidhi_rule(p, sutra_id="13009")
            if not res:
                res = p

            print(lineno)
            found = False
            for c in categories[0:5]:
                res2 = Rule(a())._interpret_pada(res, c)
                if res2:
                    p.add_label("padas.x_anta", positions=[0], value=c['pada'])
                    found = True
                    break
            if not found:
                p.add_label("padas.x_anta", positions=[0], value=categories[4]['pada'])

            found = False
            for c in categories[5:8]:
                res2 = Rule(a())._interpret_pada(res, c)
                if res2:
                    p.add_label("padas.x_upadhA", positions=[0], value=c['pada'])
                    found = True
                    break
            if not found:
                p.add_label("padas.x_upadhA", positions=[0], value=categories[7]['pada'])

            str = ",".join([p.lookup_label(l)[0].labels[l]['padas'][0]['val'] for l in hdrs])
            f.write(str + "\n")

if __name__ == '__main__':
    pgmname = sys.argv.pop(0)

    try:
        set_req_context(ReqContext())
        ashtadhyayi_init(True)

        classify_dhatus()
    except Exception as e:
        traceback.print_exc(file=sys.stdout)
        log("Failed: Caught exception", e)

    sys.exit(0)